//-------------------------------------------------------------------------
// Design Name : satd_8x8_8pairs_operative
// File Name   : satd_8x8_8pairs_operative.v
// Function    : SATD of 8x8 blocks with 8 pixel pairs as input
// Coder       : Ismael Seidel
//-------------------------------------------------------------------------
module satd_8x8_8pairs_operative(
	clock,
	reset,
	enable_inputs,
	reset_transpose_buffer,
	enable_transpose_buffer,
	change_transpose_buffer_direction,
	reset_psatd,
	enable_psatd,
	reset_satd,
	enable_satd,
	original_0,
	original_1,
	original_2,
	original_3,
	original_4,
	original_5,
	original_6,
	original_7,
	candidate_0,
	candidate_1,
	candidate_2,
	candidate_3,
	candidate_4,
	candidate_5,
	candidate_6,
	candidate_7,
	satd
);

parameter DATA_WIDTH = 8;

//------------------------------- Input Ports -----------------------------------

input clock, reset, enable_inputs, reset_transpose_buffer, enable_transpose_buffer, change_transpose_buffer_direction, reset_psatd, enable_psatd, reset_satd, enable_satd;
input [DATA_WIDTH-1:0] original_0, original_1, original_2, original_3, original_4, original_5, original_6, original_7, candidate_0, candidate_1, candidate_2, candidate_3, candidate_4, candidate_5, candidate_6, candidate_7;

//------------------------------- Output Ports -----------------------------------


output reg [DATA_WIDTH+11:0] satd;

//-------------------------------- Internal Wires ----------------------------

wire transpose_buffer_direction;
wire [DATA_WIDTH-1:0] original_reg_0, original_reg_1, original_reg_2, original_reg_3, original_reg_4, original_reg_5, original_reg_6, original_reg_7, candidate_reg_0, candidate_reg_1, candidate_reg_2, candidate_reg_3, candidate_reg_4, candidate_reg_5, candidate_reg_6, candidate_reg_7;
wire signed [DATA_WIDTH:0] difference_0, difference_1, difference_2, difference_3, difference_4, difference_5, difference_6, difference_7;
wire signed [DATA_WIDTH+3:0] out_of_1st_1d_transform_0, out_of_1st_1d_transform_1, out_of_1st_1d_transform_2, out_of_1st_1d_transform_3, out_of_1st_1d_transform_4, out_of_1st_1d_transform_5, out_of_1st_1d_transform_6, out_of_1st_1d_transform_7;
wire signed [DATA_WIDTH+3:0] out_of_transpose_buffer_0, out_of_transpose_buffer_1, out_of_transpose_buffer_2, out_of_transpose_buffer_3, out_of_transpose_buffer_4, out_of_transpose_buffer_5, out_of_transpose_buffer_6, out_of_transpose_buffer_7;
wire signed [DATA_WIDTH+6:0] out_of_2nd_1d_transform_0, out_of_2nd_1d_transform_1, out_of_2nd_1d_transform_2, out_of_2nd_1d_transform_3, out_of_2nd_1d_transform_4, out_of_2nd_1d_transform_5, out_of_2nd_1d_transform_6, out_of_2nd_1d_transform_7;
wire [DATA_WIDTH+5:0] absolute_transformed_difference_0, absolute_transformed_difference_1, absolute_transformed_difference_2, absolute_transformed_difference_3, absolute_transformed_difference_4, absolute_transformed_difference_5, absolute_transformed_difference_6, absolute_transformed_difference_7;
wire [DATA_WIDTH+8:0] sum;
wire [DATA_WIDTH+11:0] psatd;

//--------------------------------- Code Starts Here -----------------------------

tff_async_reset transpose_buffer_direction_holder(change_transpose_buffer_direction, clock, reset, transpose_buffer_direction);
input_buffer_8pairs #(DATA_WIDTH) register_inputs(clock, reset, enable_inputs, original_0, original_1, original_2, original_3, original_4, original_5, original_6, original_7, candidate_0, candidate_1, candidate_2, candidate_3, candidate_4, candidate_5, candidate_6, candidate_7, original_reg_0, original_reg_1, original_reg_2, original_reg_3, original_reg_4, original_reg_5, original_reg_6, original_reg_7, candidate_reg_0, candidate_reg_1, candidate_reg_2, candidate_reg_3, candidate_reg_4, candidate_reg_5, candidate_reg_6, candidate_reg_7);
difference_layer_8pairs #(DATA_WIDTH) difference(original_reg_0, original_reg_1, original_reg_2, original_reg_3, original_reg_4, original_reg_5, original_reg_6, original_reg_7, candidate_reg_0, candidate_reg_1, candidate_reg_2, candidate_reg_3, candidate_reg_4, candidate_reg_5, candidate_reg_6, candidate_reg_7, difference_0, difference_1, difference_2, difference_3, difference_4, difference_5, difference_6, difference_7);
transform_1d_8inputs #(DATA_WIDTH+1) first_1d_transform(difference_0, difference_1, difference_2, difference_3, difference_4, difference_5, difference_6, difference_7, out_of_1st_1d_transform_0, out_of_1st_1d_transform_1, out_of_1st_1d_transform_2, out_of_1st_1d_transform_3, out_of_1st_1d_transform_4, out_of_1st_1d_transform_5, out_of_1st_1d_transform_6, out_of_1st_1d_transform_7);
transpose_buffer_8x8 #(DATA_WIDTH+4) transpose_buffer(clock, reset_transpose_buffer, enable_transpose_buffer, transpose_buffer_direction, out_of_1st_1d_transform_0, out_of_1st_1d_transform_1, out_of_1st_1d_transform_2, out_of_1st_1d_transform_3, out_of_1st_1d_transform_4, out_of_1st_1d_transform_5, out_of_1st_1d_transform_6, out_of_1st_1d_transform_7, out_of_transpose_buffer_0, out_of_transpose_buffer_1, out_of_transpose_buffer_2, out_of_transpose_buffer_3, out_of_transpose_buffer_4, out_of_transpose_buffer_5, out_of_transpose_buffer_6, out_of_transpose_buffer_7);
transform_1d_8inputs #(DATA_WIDTH+4) second_1d_transform(out_of_transpose_buffer_0, out_of_transpose_buffer_1, out_of_transpose_buffer_2, out_of_transpose_buffer_3, out_of_transpose_buffer_4, out_of_transpose_buffer_5, out_of_transpose_buffer_6, out_of_transpose_buffer_7, out_of_2nd_1d_transform_0, out_of_2nd_1d_transform_1, out_of_2nd_1d_transform_2, out_of_2nd_1d_transform_3, out_of_2nd_1d_transform_4, out_of_2nd_1d_transform_5, out_of_2nd_1d_transform_6, out_of_2nd_1d_transform_7);
abs_layer_8inputs #(DATA_WIDTH+7) absolute(out_of_2nd_1d_transform_0, out_of_2nd_1d_transform_1, out_of_2nd_1d_transform_2, out_of_2nd_1d_transform_3, out_of_2nd_1d_transform_4, out_of_2nd_1d_transform_5, out_of_2nd_1d_transform_6, out_of_2nd_1d_transform_7, absolute_transformed_difference_0, absolute_transformed_difference_1, absolute_transformed_difference_2, absolute_transformed_difference_3, absolute_transformed_difference_4, absolute_transformed_difference_5, absolute_transformed_difference_6, absolute_transformed_difference_7);
sum_tree_8inputs #(DATA_WIDTH+6) sum_tree(absolute_transformed_difference_0, absolute_transformed_difference_1, absolute_transformed_difference_2, absolute_transformed_difference_3, absolute_transformed_difference_4, absolute_transformed_difference_5, absolute_transformed_difference_6, absolute_transformed_difference_7, sum);
accumulator #(DATA_WIDTH+9, DATA_WIDTH+12) acc(clock, reset_psatd, enable_psatd, sum, psatd);
always @(posedge clock) begin
	if (reset_satd) begin
		satd <= 0;
	end
	else if (enable_satd) begin
		satd <= (psatd+sum+2)>>2;
	end
end

endmodule

