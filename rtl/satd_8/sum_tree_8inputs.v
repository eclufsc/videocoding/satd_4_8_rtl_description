//-------------------------------------------------------------------------
// Design Name : sum_tree_8inputs
// File Name   : sum_tree_8inputs.v
// Function    : Sums up 8 inputs in a combinational fashion
// Coder       : Ismael Seidel
//-------------------------------------------------------------------------
module sum_tree_8inputs (
	in_0,
	in_1,
	in_2,
	in_3,
	in_4,
	in_5,
	in_6,
	in_7,
	out
);

parameter DATA_WIDTH = 8;


//------------------------------- Input Ports -----------------------------------
input [DATA_WIDTH-1:0] in_0, in_1, in_2, in_3, in_4, in_5, in_6, in_7;

//------------------------------- Output Ports -----------------------------------
output [DATA_WIDTH+2:0] out;


//-------------------------------- Internal Wires ----------------------------
wire [DATA_WIDTH:0] sum_0, sum_1, sum_2, sum_3;
wire [DATA_WIDTH+1:0] sum_4, sum_5;
wire [DATA_WIDTH+2:0] sum_6;


//--------------------------------- Code Starts Here -----------------------------
adder #(DATA_WIDTH) adder_0(in_0, in_1, sum_0);
adder #(DATA_WIDTH) adder_1(in_2, in_3, sum_1);
adder #(DATA_WIDTH) adder_2(in_4, in_5, sum_2);
adder #(DATA_WIDTH) adder_3(in_6, in_7, sum_3);

adder #(DATA_WIDTH+1) adder_4(sum_0, sum_1, sum_4);
adder #(DATA_WIDTH+1) adder_5(sum_2, sum_3, sum_5);

adder #(DATA_WIDTH+2) adder_6(sum_4, sum_5, sum_6);


assign out = sum_6;

endmodule

