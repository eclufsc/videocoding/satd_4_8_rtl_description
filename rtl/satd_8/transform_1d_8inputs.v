module transform_1d_8inputs (
	in_0,
	in_1,
	in_2,
	in_3,
	in_4,
	in_5,
	in_6,
	in_7,
	out_0,
	out_1,
	out_2,
	out_3,
	out_4,
	out_5,
	out_6,
	out_7
);

parameter DATA_WIDTH = 8;

input signed [DATA_WIDTH-1:0] in_0, in_1, in_2, in_3, in_4, in_5, in_6, in_7;
output signed [DATA_WIDTH+2:0] out_0, out_1, out_2, out_3, out_4, out_5, out_6, out_7;
wire [DATA_WIDTH:0] wire_0_0, wire_0_1, wire_0_2, wire_0_3, wire_0_4, wire_0_5, wire_0_6, wire_0_7;
wire [DATA_WIDTH+1:0] wire_1_0, wire_1_1, wire_1_2, wire_1_3, wire_1_4, wire_1_5, wire_1_6, wire_1_7;
//first layer (0)
butterfly2 #(DATA_WIDTH) butterfly2_0_0(in_0, in_1, wire_0_0, wire_0_1);
butterfly2 #(DATA_WIDTH) butterfly2_0_2(in_2, in_3, wire_0_2, wire_0_3);
butterfly2 #(DATA_WIDTH) butterfly2_0_4(in_4, in_5, wire_0_4, wire_0_5);
butterfly2 #(DATA_WIDTH) butterfly2_0_6(in_6, in_7, wire_0_6, wire_0_7);
//layer 1
butterfly2 #(DATA_WIDTH+1) butterfly2_1_0(wire_0_0, wire_0_2, wire_1_0, wire_1_1);
butterfly2 #(DATA_WIDTH+1) butterfly2_1_1(wire_0_1, wire_0_3, wire_1_2, wire_1_3);
butterfly2 #(DATA_WIDTH+1) butterfly2_1_2(wire_0_4, wire_0_6, wire_1_4, wire_1_5);
butterfly2 #(DATA_WIDTH+1) butterfly2_1_3(wire_0_5, wire_0_7, wire_1_6, wire_1_7);
//last layer (2)
butterfly2 #(DATA_WIDTH+2) butterfly2_2_0(wire_1_0, wire_1_4, out_0, out_1);
butterfly2 #(DATA_WIDTH+2) butterfly2_2_1(wire_1_1, wire_1_5, out_2, out_3);
butterfly2 #(DATA_WIDTH+2) butterfly2_2_2(wire_1_2, wire_1_6, out_4, out_5);
butterfly2 #(DATA_WIDTH+2) butterfly2_2_3(wire_1_3, wire_1_7, out_6, out_7);

endmodule

