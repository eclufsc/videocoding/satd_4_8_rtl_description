//-------------------------------------------------------------------------
// Design Name : satd_8x8_8pairs
// File Name   : satd_8x8_8pairs.v
// Function    : SATD of 8x8 blocks with 8 pixel pairs as input
// Coder       : Ismael Seidel
//-------------------------------------------------------------------------
module satd_8x8_8pairs(
	clock,
	reset,
	enable,
	original_0,
	original_1,
	original_2,
	original_3,
	original_4,
	original_5,
	original_6,
	original_7,
	candidate_0,
	candidate_1,
	candidate_2,
	candidate_3,
	candidate_4,
	candidate_5,
	candidate_6,
	candidate_7,
	satd,
	done
);

parameter DATA_WIDTH = 8;

//------------------------------- Input Ports -----------------------------------

input clock, reset, enable;
input [DATA_WIDTH-1:0] original_0, original_1, original_2, original_3, original_4, original_5, original_6, original_7, candidate_0, candidate_1, candidate_2, candidate_3, candidate_4, candidate_5, candidate_6, candidate_7;

//------------------------------- Output Ports -----------------------------------

output [DATA_WIDTH+11:0] satd;
output done;

//-------------------------------- Internal Wires ----------------------------
wire enable_inputs, reset_transpose_buffer, enable_transpose_buffer, change_transpose_buffer_direction, reset_psatd, enable_psatd, reset_satd, enable_satd;

//--------------------------------- Code Starts Here -----------------------------
satd_8x8_8pairs_control control(clock, reset, enable, enable_inputs, reset_transpose_buffer, enable_transpose_buffer, change_transpose_buffer_direction, reset_psatd, enable_psatd, reset_satd, enable_satd, done);
satd_8x8_8pairs_operative operative(clock, reset, enable_inputs, reset_transpose_buffer, enable_transpose_buffer, change_transpose_buffer_direction, reset_psatd, enable_psatd, reset_satd, enable_satd, original_0, original_1, original_2, original_3, original_4, original_5, original_6, original_7, candidate_0, candidate_1, candidate_2, candidate_3, candidate_4, candidate_5, candidate_6, candidate_7, satd);

endmodule

