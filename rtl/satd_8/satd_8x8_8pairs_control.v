//-------------------------------------------------------------------------
// Design Name : satd_8x8_8pairs_control
// File Name   : satd_8x8_8pairs_control.v
// Function    : SATD of 8x8 blocks with 8 pixel pairs as input
// Coder       : Ismael Seidel
//-------------------------------------------------------------------------
module satd_8x8_8pairs_control(
	clock,
	reset,
	enable,
	enable_inputs,
	reset_transpose_buffer,
	enable_transpose_buffer,
	change_transpose_buffer_direction,
	reset_psatd,
	enable_psatd,
	reset_satd,
	enable_satd,
	done
);

parameter DATA_WIDTH = 8;

//------------------------------- Input Ports -----------------------------------
input clock, reset, enable;

//------------------------------- Output Ports -----------------------------------
output reg enable_inputs, reset_transpose_buffer, enable_transpose_buffer, change_transpose_buffer_direction, reset_psatd, enable_psatd, reset_satd, enable_satd, done;

reg [4:0] state;
parameter
IDLE = 0,
INITIALIZE_0 = 1,
INITIALIZE_1 = 2,
INITIALIZE_2 = 3,
INITIALIZE_3 = 4,
INITIALIZE_4 = 5,
INITIALIZE_5 = 6,
INITIALIZE_6 = 7,
INITIALIZE_7 = 8,
PRE_CALCULATE_0 = 9,
PRE_CALCULATE_1 = 10,
CALCULATE_0 = 11,
CALCULATE_1 = 12,
CALCULATE_2 = 13,
CALCULATE_3 = 14,
CALCULATE_4 = 15,
CALCULATE_5 = 16,
CALCULATE_6 = 17,
CALCULATE_7 = 18,
TERMINATE_0 = 19,
TERMINATE_1 = 20,
TERMINATE_2 = 21,
TERMINATE_3 = 22,
TERMINATE_4 = 23,
TERMINATE_5 = 24,
TERMINATE_6 = 25,
TERMINATE_7 = 26,
LAST_0 = 27,
LAST_1 = 28,
PRE_TERMINATE_0 = 29,
PRE_TERMINATE_1 = 30;

always @ (state) begin
	case (state)
		IDLE:begin
			enable_inputs = 1'b0;
			enable_transpose_buffer = 1'b0;
			change_transpose_buffer_direction = 1'b0;
			reset_psatd = 1'b1;
			enable_psatd = 1'b0;
			reset_satd = 1'b1;
			enable_satd = 1'b0;
			reset_transpose_buffer = 1'b1;
			done = 1'b0;
		end
		INITIALIZE_0:begin
			enable_inputs = 1'b1;
			enable_transpose_buffer = 1'b0;
			change_transpose_buffer_direction = 1'b0;
			reset_psatd = 1'b0;
			enable_psatd = 1'b0;
			reset_satd = 1'b0;
			enable_satd = 1'b0;
			reset_transpose_buffer = 1'b0;
			done = 1'b0;
		end
		INITIALIZE_1:begin
			enable_inputs = 1'b1;
			enable_transpose_buffer = 1'b1;
			change_transpose_buffer_direction = 1'b0;
			reset_psatd = 1'b0;
			enable_psatd = 1'b0;
			reset_satd = 1'b0;
			enable_satd = 1'b0;
			reset_transpose_buffer = 1'b0;
			done = 1'b0;
		end
		INITIALIZE_2:begin
			enable_inputs = 1'b1;
			enable_transpose_buffer = 1'b1;
			change_transpose_buffer_direction = 1'b0;
			reset_psatd = 1'b0;
			enable_psatd = 1'b0;
			reset_satd = 1'b0;
			enable_satd = 1'b0;
			reset_transpose_buffer = 1'b0;
			done = 1'b0;
		end
		INITIALIZE_3:begin
			enable_inputs = 1'b1;
			enable_transpose_buffer = 1'b1;
			change_transpose_buffer_direction = 1'b0;
			reset_psatd = 1'b0;
			enable_psatd = 1'b0;
			reset_satd = 1'b0;
			enable_satd = 1'b0;
			reset_transpose_buffer = 1'b0;
			done = 1'b0;
		end
		INITIALIZE_4:begin
			enable_inputs = 1'b1;
			enable_transpose_buffer = 1'b1;
			change_transpose_buffer_direction = 1'b0;
			reset_psatd = 1'b0;
			enable_psatd = 1'b0;
			reset_satd = 1'b0;
			enable_satd = 1'b0;
			reset_transpose_buffer = 1'b0;
			done = 1'b0;
		end
		INITIALIZE_5:begin
			enable_inputs = 1'b1;
			enable_transpose_buffer = 1'b1;
			change_transpose_buffer_direction = 1'b0;
			reset_psatd = 1'b0;
			enable_psatd = 1'b0;
			reset_satd = 1'b0;
			enable_satd = 1'b0;
			reset_transpose_buffer = 1'b0;
			done = 1'b0;
		end
		INITIALIZE_6:begin
			enable_inputs = 1'b1;
			enable_transpose_buffer = 1'b1;
			change_transpose_buffer_direction = 1'b0;
			reset_psatd = 1'b0;
			enable_psatd = 1'b0;
			reset_satd = 1'b0;
			enable_satd = 1'b0;
			reset_transpose_buffer = 1'b0;
			done = 1'b0;
		end
		INITIALIZE_7:begin
			enable_inputs = 1'b1;
			enable_transpose_buffer = 1'b1;
			change_transpose_buffer_direction = 1'b0;
			reset_psatd = 1'b0;
			enable_psatd = 1'b0;
			reset_satd = 1'b0;
			enable_satd = 1'b0;
			reset_transpose_buffer = 1'b0;
			done = 1'b0;
		end
		PRE_CALCULATE_0:begin
			enable_inputs = 1'b1;
			enable_transpose_buffer = 1'b1;
			change_transpose_buffer_direction = 1'b1;
			reset_psatd = 1'b0;
			enable_psatd = 1'b0;
			reset_satd = 1'b0;
			enable_satd = 1'b0;
			reset_transpose_buffer = 1'b0;
			done = 1'b0;
		end
		PRE_CALCULATE_1:begin
			enable_inputs = 1'b1;
			enable_transpose_buffer = 1'b1;
			change_transpose_buffer_direction = 1'b0;
			reset_psatd = 1'b0;
			enable_psatd = 1'b1;
			reset_satd = 1'b0;
			enable_satd = 1'b0;
			reset_transpose_buffer = 1'b0;
			done = 1'b0;
		end
		CALCULATE_0:begin
			enable_inputs = 1'b1;
			enable_transpose_buffer = 1'b1;
			change_transpose_buffer_direction = 1'b1;
			reset_psatd = 1'b1;
			enable_psatd = 1'b0;
			reset_satd = 1'b0;
			enable_satd = 1'b1;
			reset_transpose_buffer = 1'b0;
			done = 1'b0;
		end
		CALCULATE_1:begin
			enable_inputs = 1'b1;
			enable_transpose_buffer = 1'b1;
			change_transpose_buffer_direction = 1'b0;
			reset_psatd = 1'b0;
			enable_psatd = 1'b1;
			reset_satd = 1'b0;
			enable_satd = 1'b0;
			reset_transpose_buffer = 1'b0;
			done = 1'b1;
		end
		CALCULATE_2:begin
			enable_inputs = 1'b1;
			enable_transpose_buffer = 1'b1;
			change_transpose_buffer_direction = 1'b0;
			reset_psatd = 1'b0;
			enable_psatd = 1'b1;
			reset_satd = 1'b0;
			enable_satd = 1'b0;
			reset_transpose_buffer = 1'b0;
			done = 1'b0;
		end
		CALCULATE_3:begin
			enable_inputs = 1'b1;
			enable_transpose_buffer = 1'b1;
			change_transpose_buffer_direction = 1'b0;
			reset_psatd = 1'b0;
			enable_psatd = 1'b1;
			reset_satd = 1'b0;
			enable_satd = 1'b0;
			reset_transpose_buffer = 1'b0;
			done = 1'b0;
		end
		CALCULATE_4:begin
			enable_inputs = 1'b1;
			enable_transpose_buffer = 1'b1;
			change_transpose_buffer_direction = 1'b0;
			reset_psatd = 1'b0;
			enable_psatd = 1'b1;
			reset_satd = 1'b0;
			enable_satd = 1'b0;
			reset_transpose_buffer = 1'b0;
			done = 1'b0;
		end
		CALCULATE_5:begin
			enable_inputs = 1'b1;
			enable_transpose_buffer = 1'b1;
			change_transpose_buffer_direction = 1'b0;
			reset_psatd = 1'b0;
			enable_psatd = 1'b1;
			reset_satd = 1'b0;
			enable_satd = 1'b0;
			reset_transpose_buffer = 1'b0;
			done = 1'b0;
		end
		CALCULATE_6:begin
			enable_inputs = 1'b1;
			enable_transpose_buffer = 1'b1;
			change_transpose_buffer_direction = 1'b0;
			reset_psatd = 1'b0;
			enable_psatd = 1'b1;
			reset_satd = 1'b0;
			enable_satd = 1'b0;
			reset_transpose_buffer = 1'b0;
			done = 1'b0;
		end
		CALCULATE_7:begin
			enable_inputs = 1'b1;
			enable_transpose_buffer = 1'b1;
			change_transpose_buffer_direction = 1'b0;
			reset_psatd = 1'b0;
			enable_psatd = 1'b1;
			reset_satd = 1'b0;
			enable_satd = 1'b0;
			reset_transpose_buffer = 1'b0;
			done = 1'b0;
		end
		TERMINATE_0:begin
			enable_inputs = 1'b0;
			enable_transpose_buffer = 1'b1;
			change_transpose_buffer_direction = 1'b1;
			reset_psatd = 1'b1;
			enable_psatd = 1'b0;
			reset_satd = 1'b0;
			enable_satd = 1'b1;
			reset_transpose_buffer = 1'b0;
			done = 1'b0;
		end
		TERMINATE_1:begin
			enable_inputs = 1'b0;
			enable_transpose_buffer = 1'b1;
			change_transpose_buffer_direction = 1'b0;
			reset_psatd = 1'b0;
			enable_psatd = 1'b1;
			reset_satd = 1'b0;
			enable_satd = 1'b0;
			reset_transpose_buffer = 1'b0;
			done = 1'b1;
		end
		TERMINATE_2:begin
			enable_inputs = 1'b0;
			enable_transpose_buffer = 1'b1;
			change_transpose_buffer_direction = 1'b0;
			reset_psatd = 1'b0;
			enable_psatd = 1'b1;
			reset_satd = 1'b0;
			enable_satd = 1'b0;
			reset_transpose_buffer = 1'b0;
			done = 1'b0;
		end
		TERMINATE_3:begin
			enable_inputs = 1'b0;
			enable_transpose_buffer = 1'b1;
			change_transpose_buffer_direction = 1'b0;
			reset_psatd = 1'b0;
			enable_psatd = 1'b1;
			reset_satd = 1'b0;
			enable_satd = 1'b0;
			reset_transpose_buffer = 1'b0;
			done = 1'b0;
		end
		TERMINATE_4:begin
			enable_inputs = 1'b0;
			enable_transpose_buffer = 1'b1;
			change_transpose_buffer_direction = 1'b0;
			reset_psatd = 1'b0;
			enable_psatd = 1'b1;
			reset_satd = 1'b0;
			enable_satd = 1'b0;
			reset_transpose_buffer = 1'b0;
			done = 1'b0;
		end
		TERMINATE_5:begin
			enable_inputs = 1'b0;
			enable_transpose_buffer = 1'b1;
			change_transpose_buffer_direction = 1'b0;
			reset_psatd = 1'b0;
			enable_psatd = 1'b1;
			reset_satd = 1'b0;
			enable_satd = 1'b0;
			reset_transpose_buffer = 1'b0;
			done = 1'b0;
		end
		TERMINATE_6:begin
			enable_inputs = 1'b0;
			enable_transpose_buffer = 1'b1;
			change_transpose_buffer_direction = 1'b0;
			reset_psatd = 1'b0;
			enable_psatd = 1'b1;
			reset_satd = 1'b0;
			enable_satd = 1'b0;
			reset_transpose_buffer = 1'b0;
			done = 1'b0;
		end
		TERMINATE_7:begin
			enable_inputs = 1'b0;
			enable_transpose_buffer = 1'b1;
			change_transpose_buffer_direction = 1'b0;
			reset_psatd = 1'b0;
			enable_psatd = 1'b1;
			reset_satd = 1'b0;
			enable_satd = 1'b0;
			reset_transpose_buffer = 1'b0;
			done = 1'b0;
		end
		LAST_0:begin
			enable_inputs = 1'b0;
			enable_transpose_buffer = 1'b0;
			change_transpose_buffer_direction = 1'b0;
			reset_psatd = 1'b0;
			enable_psatd = 1'b0;
			reset_satd = 1'b0;
			enable_satd = 1'b1;
			reset_transpose_buffer = 1'b0;
			done = 1'b0;
		end
		LAST_1:begin
			enable_inputs = 1'b0;
			enable_transpose_buffer = 1'b0;
			change_transpose_buffer_direction = 1'b0;
			reset_psatd = 1'b0;
			enable_psatd = 1'b0;
			reset_satd = 1'b0;
			enable_satd = 1'b0;
			reset_transpose_buffer = 1'b0;
			done = 1'b1;
		end
		PRE_TERMINATE_0:begin
			enable_inputs = 1'b0;
			enable_transpose_buffer = 1'b1;
			change_transpose_buffer_direction = 1'b1;
			reset_psatd = 1'b0;
			enable_psatd = 1'b0;
			reset_satd = 1'b0;
			enable_satd = 1'b0;
			reset_transpose_buffer = 1'b0;
			done = 1'b0;
		end
		PRE_TERMINATE_1:begin
			enable_inputs = 1'b0;
			enable_transpose_buffer = 1'b1;
			change_transpose_buffer_direction = 1'b0;
			reset_psatd = 1'b0;
			enable_psatd = 1'b1;
			reset_satd = 1'b0;
			enable_satd = 1'b0;
			reset_transpose_buffer = 1'b0;
			done = 1'b0;
		end
		default:begin
			enable_inputs = 1'b0;
			enable_transpose_buffer = 1'b0;
			change_transpose_buffer_direction = 1'b0;
			reset_psatd = 1'b1;
			enable_psatd = 1'b0;
			reset_satd = 1'b1;
			enable_satd = 1'b0;
			reset_transpose_buffer = 1'b1;
			done = 1'b0;
		end
	endcase
end

always @ (posedge clock or posedge reset) begin
	if (reset)
		state <= IDLE;
	else
		case (state)
			IDLE: if(enable)
				state<= INITIALIZE_0;
			INITIALIZE_0: state<=INITIALIZE_1;
			INITIALIZE_1: state<=INITIALIZE_2;
			INITIALIZE_2: state<=INITIALIZE_3;
			INITIALIZE_3: state<=INITIALIZE_4;
			INITIALIZE_4: state<=INITIALIZE_5;
			INITIALIZE_5: state<=INITIALIZE_6;
			INITIALIZE_6: state<=INITIALIZE_7;
			INITIALIZE_7: if(enable)
				state<=PRE_CALCULATE_0;
			else
				state<=PRE_TERMINATE_0;
			CALCULATE_0: state<=CALCULATE_1;
			CALCULATE_1: state<=CALCULATE_2;
			CALCULATE_2: state<=CALCULATE_3;
			CALCULATE_3: state<=CALCULATE_4;
			CALCULATE_4: state<=CALCULATE_5;
			CALCULATE_5: state<=CALCULATE_6;
			CALCULATE_6: state<=CALCULATE_7;
			CALCULATE_7: if(enable)
				state<=CALCULATE_0;
			else
				state<=TERMINATE_0;
			TERMINATE_0: state<= TERMINATE_1;
			TERMINATE_1: state<= TERMINATE_2;
			TERMINATE_2: state<= TERMINATE_3;
			TERMINATE_3: state<= TERMINATE_4;
			TERMINATE_4: state<= TERMINATE_5;
			TERMINATE_5: state<= TERMINATE_6;
			TERMINATE_6: state<= TERMINATE_7;
			TERMINATE_7: state<=LAST_0;
			PRE_CALCULATE_0: state<= PRE_CALCULATE_1;
			PRE_CALCULATE_1: state<= CALCULATE_2;
			LAST_0: state<= LAST_1;
			LAST_1: state<= IDLE;
			PRE_TERMINATE_0: state<=PRE_TERMINATE_1;
			PRE_TERMINATE_1: state<=TERMINATE_2;
		endcase
end

endmodule

