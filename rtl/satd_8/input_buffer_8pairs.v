//-------------------------------------------------------------------------
// Design Name : input_buffer_8pairs
// File Name   : input_buffer_8pairs.v
// Function    : A simple module to hold the 8 inputs
// Coder       : Ismael Seidel
//-------------------------------------------------------------------------
module input_buffer_8pairs (
	clock,
	reset,
	enable,
	in_0,
	in_1,
	in_2,
	in_3,
	in_4,
	in_5,
	in_6,
	in_7,
	in_8,
	in_9,
	in_10,
	in_11,
	in_12,
	in_13,
	in_14,
	in_15,
	out_0,
	out_1,
	out_2,
	out_3,
	out_4,
	out_5,
	out_6,
	out_7,
	out_8,
	out_9,
	out_10,
	out_11,
	out_12,
	out_13,
	out_14,
	out_15
);

parameter DATA_WIDTH = 8;


//------------------------------- Input Ports -----------------------------------
input clock, reset, enable;
input [DATA_WIDTH-1:0] in_0, in_1, in_2, in_3, in_4, in_5, in_6, in_7, in_8, in_9, in_10, in_11, in_12, in_13, in_14, in_15;

//------------------------------- Output Ports -----------------------------------
output reg [DATA_WIDTH-1:0] out_0, out_1, out_2, out_3, out_4, out_5, out_6, out_7, out_8, out_9, out_10, out_11, out_12, out_13, out_14, out_15;

//--------------------------------- Code Starts Here -----------------------------
always @(posedge clock or posedge reset) begin
	if (reset) begin
		out_0 <= 0;
		out_1 <= 0;
		out_2 <= 0;
		out_3 <= 0;
		out_4 <= 0;
		out_5 <= 0;
		out_6 <= 0;
		out_7 <= 0;
		out_8 <= 0;
		out_9 <= 0;
		out_10 <= 0;
		out_11 <= 0;
		out_12 <= 0;
		out_13 <= 0;
		out_14 <= 0;
		out_15 <= 0;
	end
	else if (enable) begin
		out_0 <= in_0;
		out_1 <= in_1;
		out_2 <= in_2;
		out_3 <= in_3;
		out_4 <= in_4;
		out_5 <= in_5;
		out_6 <= in_6;
		out_7 <= in_7;
		out_8 <= in_8;
		out_9 <= in_9;
		out_10 <= in_10;
		out_11 <= in_11;
		out_12 <= in_12;
		out_13 <= in_13;
		out_14 <= in_14;
		out_15 <= in_15;
	end
end


endmodule

