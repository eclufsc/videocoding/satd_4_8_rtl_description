//-------------------------------------------------------------------------
// Design Name : difference_layer_8pairs
// File Name   : difference_layer_8pairs.v
// Function    : Differences over an array of 8 unsigned input pairs
// Coder       : Ismael Seidel
//-------------------------------------------------------------------------
module difference_layer_8pairs (
	in_A_0,
	in_A_1,
	in_A_2,
	in_A_3,
	in_A_4,
	in_A_5,
	in_A_6,
	in_A_7,
	in_B_0,
	in_B_1,
	in_B_2,
	in_B_3,
	in_B_4,
	in_B_5,
	in_B_6,
	in_B_7,
	out_0,
	out_1,
	out_2,
	out_3,
	out_4,
	out_5,
	out_6,
	out_7
);parameter DATA_WIDTH = 8;


//------------------------------- Input Ports -----------------------------------
input [DATA_WIDTH-1:0] in_A_0, in_A_1, in_A_2, in_A_3, in_A_4, in_A_5, in_A_6, in_A_7, in_B_0, in_B_1, in_B_2, in_B_3, in_B_4, in_B_5, in_B_6, in_B_7;

//------------------------------- Output Ports -----------------------------------
output [DATA_WIDTH:0] out_0, out_1, out_2, out_3, out_4, out_5, out_6, out_7;

//--------------------------------- Code Starts Here -----------------------------
difference_of_unsigned_inputs #(DATA_WIDTH) difference_0(in_A_0, in_B_0, out_0);
difference_of_unsigned_inputs #(DATA_WIDTH) difference_1(in_A_1, in_B_1, out_1);
difference_of_unsigned_inputs #(DATA_WIDTH) difference_2(in_A_2, in_B_2, out_2);
difference_of_unsigned_inputs #(DATA_WIDTH) difference_3(in_A_3, in_B_3, out_3);
difference_of_unsigned_inputs #(DATA_WIDTH) difference_4(in_A_4, in_B_4, out_4);
difference_of_unsigned_inputs #(DATA_WIDTH) difference_5(in_A_5, in_B_5, out_5);
difference_of_unsigned_inputs #(DATA_WIDTH) difference_6(in_A_6, in_B_6, out_6);
difference_of_unsigned_inputs #(DATA_WIDTH) difference_7(in_A_7, in_B_7, out_7);

endmodule

