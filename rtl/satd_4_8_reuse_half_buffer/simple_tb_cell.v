/* 
 *	\file simple_tb_cell.v
 *	\brief Description
 *	\author ismaelseidel
 *	\Date:   2018-07-15 18:38:26
 *  \Last Modified by:   ismaelseidel
 *  \Last Modified time: 2018-07-15 18:38:12
 *
 *  Longer description here
 *	
 *	Developed at Embedded Computing Lab (ECL), 2018.
 *	
 */
 
 // module simple_tb_cell definition
 module simple_tb_cell (
 	//inputs and outputs declaration
 	clock,
 	reset,
 	enable,
 	in,
 	out
 );
 
 //------------------------------------------ Parameters -------------------------------------------
 parameter DATAWIDTH = 8;
 
 
 //------------------------------------------ Input Ports ------------------------------------------
 // input signed[NAME-1:0] in;
 input clock, reset, enable;
 input [DATAWIDTH-1:0] in; 
 
 //----------------------------------------- Output Ports ------------------------------------------
 // output signed[NAME-1:0] out;
 output reg [DATAWIDTH-1:0] out;
 
 //------------------------------------ Module Instantiations --------------------------------------
 // module_name #(PARAMETERS) custom_module_name (ins and outs);
 
 
 //--------------------------------------- Code Starts Here ----------------------------------------
 // always ...
always @(posedge clock or posedge reset) begin
	if (reset) begin
		out <= 0;
	end
	else if (enable) begin
		out <= in;
	end
end

 
 endmodule
 