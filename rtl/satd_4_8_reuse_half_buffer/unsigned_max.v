/*
 *	\file unsigned_max.v
 *	\brief Implementation of an parameterized maximum value.
 *	\author Ismael Seidel
 *	\Date:   2016-04-13 15:29:05
 *  \Last Modified by:   ismaelseidel
 *  \Last Modified time: 2016-04-13 15:34:02
 *
 *	This absolute implementation is parameterized so as to the number of bits in its inputs. 
 *	Notice that the output is one bit smaller, so as remove the carry out of this absolute.
 *	
 *	Developed at Embedded Computing Lab (ECL), 2016.
 *	
 */

//Maximum value module definition.
module unsigned_max (
	in_0,
	in_1,
	out
);

//parameter indicating the number of bits in the input. The output is one bit shorter.
parameter DATA_WIDTH = 8;

//declaration of number of bits for inputs in_0 and in_1.
input [DATA_WIDTH-1:0] in_0;
input [DATA_WIDTH-1:0] in_1;

//declaration of output.
output [DATA_WIDTH-1:0] out;

//the output is assigned to be the larger between in_0 and in_1.
assign out = (in_0>in_1)? in_0 : in_1;

endmodule
