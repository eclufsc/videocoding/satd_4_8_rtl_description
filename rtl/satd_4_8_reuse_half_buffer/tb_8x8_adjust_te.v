/* 
 *	\file tb_8x8_adjust_te.v
 *	\brief Description
 *	\author ismaelseidel
 *	\Date:   2018-07-15 20:22:21
 *  \Last Modified by:   ismaelseidel
 *  \Last Modified time: 2018-07-15 20:22:03
 *
 *  Longer description here
 *	
 *	Developed at Embedded Computing Lab (ECL), 2018.
 *	
 */

//clock, reset, reset_transpose_buffer8x8, enable_transpose_buffer8x8, change_transpose_buffer_direction8x8, reset_psatd8x8, enable_psatd8x8, reset_satd8x8, enable_satd8x8,
 // module tb_8x8_adjust_te definition
 module tb_8x8_adjust_te (
 	//inputs and outputs declaration
 	clock, 
 	reset,
	enable_reg_barrier,
	enable_transpose_buffer8x8,
	reset_transpose_buffer,
	reset_psatd,
	enable_psatd,
	reset_satd,
	enable_satd,
	address_tb,
 	in_0,
 	in_1,
 	in_2,
 	in_3,
 	in_4,
 	in_5,
 	in_6,
 	in_7,
 	satd
 );
 
 //------------------------------------------ Parameters -------------------------------------------
 // parameter NAME = value;
 parameter DATAWIDTH=8;
 
 //------------------------------------------ Input Ports ------------------------------------------
 // input signed[NAME-1:0] in;
 input clock, reset, enable_reg_barrier, enable_transpose_buffer8x8, reset_transpose_buffer, reset_psatd, enable_psatd, reset_satd, enable_satd;
 input [1:0] address_tb;
 input signed [DATAWIDTH-1:0] in_0, in_1, in_2, in_3, in_4, in_5, in_6, in_7;
 
 //----------------------------------------- Output Ports ------------------------------------------
 // output signed[NAME-1:0] out;
 output reg [DATAWIDTH+3:0] satd;
 
 //---------------------------------------- Internal Wires -----------------------------------------
 // wire signed[NAME-1:0] some_wire;
 wire [DATAWIDTH:0] out_adj_0, out_adj_1, out_adj_2, out_adj_3, out_adj_4, out_adj_5, out_adj_6, out_adj_7;
 wire [DATAWIDTH-1:0] out_abs_layer_0, out_abs_layer_1, out_abs_layer_2, out_abs_layer_3, out_abs_layer_4, out_abs_layer_5, out_abs_layer_6, out_abs_layer_7;
 wire [DATAWIDTH-1:0] out_tb_0, out_tb_1, out_tb_2, out_tb_3, out_tb_4, out_tb_5, out_tb_6, out_tb_7;
 wire [DATAWIDTH-1:0] out_max_0, out_max_1, out_max_2, out_max_3, out_max_4, out_max_5, out_max_6, out_max_7;
 wire [DATAWIDTH-1:0] out_reg_0, out_reg_1, out_reg_2, out_reg_3, out_reg_4, out_reg_5, out_reg_6, out_reg_7;
 wire [DATAWIDTH+2:0] sum;
 wire [DATAWIDTH+4:0] psatd;
 
 //------------------------------------ Module Instantiations --------------------------------------
 // module_name #(PARAMETERS) custom_module_name (ins and outs);
 transform_1d_8inputs_mod #(DATAWIDTH) first_1d_transform(in_0, in_1, in_2, in_3, in_4, in_5, in_6, in_7, out_adj_0, out_adj_1, out_adj_2, out_adj_3, out_adj_4, out_adj_5, out_adj_6, out_adj_7);

 abs_layer_8inputs #(DATAWIDTH+1) abs_layer (out_adj_0, out_adj_1, out_adj_2, out_adj_3, out_adj_4, out_adj_5, out_adj_6, out_adj_7, out_abs_layer_0, out_abs_layer_1, out_abs_layer_2, out_abs_layer_3, out_abs_layer_4, out_abs_layer_5, out_abs_layer_6, out_abs_layer_7);

 transpose_buffer_half #(DATAWIDTH) tb_a (clock, reset_transpose_buffer, enable_transpose_buffer8x8, address_tb, out_abs_layer_0, out_abs_layer_1, out_abs_layer_2, out_abs_layer_3, out_tb_0, out_tb_1, out_tb_2, out_tb_3);
 transpose_buffer_half #(DATAWIDTH) tb_b (clock, reset_transpose_buffer, enable_transpose_buffer8x8, address_tb, out_abs_layer_4, out_abs_layer_5, out_abs_layer_6, out_abs_layer_7, out_tb_4, out_tb_5, out_tb_6, out_tb_7);

unsigned_max_layer_8pairs #(DATAWIDTH) max_layer(out_abs_layer_0, out_tb_0, out_abs_layer_1, out_tb_1, out_abs_layer_2, out_tb_2, out_abs_layer_3, out_tb_3, out_abs_layer_4, out_tb_4, out_abs_layer_5, out_tb_5, out_abs_layer_6, out_tb_6, out_abs_layer_7, out_tb_7, out_max_0, out_max_1, out_max_2, out_max_3, out_max_4, out_max_5, out_max_6, out_max_7);

unsigned_register_layer_8inputs #(DATAWIDTH) reg_barrier(clock, reset, enable_reg_barrier, out_max_0, out_max_1, out_max_2, out_max_3, out_max_4, out_max_5, out_max_6, out_max_7, out_reg_0, out_reg_1, out_reg_2, out_reg_3, out_reg_4, out_reg_5, out_reg_6, out_reg_7);

 sum_tree_8inputs #(DATAWIDTH) sum_tree(out_reg_0, out_reg_1, out_reg_2, out_reg_3, out_reg_4, out_reg_5, out_reg_6, out_reg_7, sum);

accumulator #(DATAWIDTH+3, DATAWIDTH+5) acc(clock, reset_psatd, enable_psatd, sum, psatd);

always @(posedge clock) begin
	if (reset_satd) begin
		satd <= 0;
	end
	else if (enable_satd) begin
		satd <= (psatd+sum+1)>>1;
	end
end

 
 //--------------------------------------- Code Starts Here ----------------------------------------
 // always ...
 
 endmodule
 
