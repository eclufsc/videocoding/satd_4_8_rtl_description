/* 
 *	\file transpose_buffer_half.v
 *	\brief Description
 *	\author ismaelseidel
 *	\Date:   2018-07-15 18:49:20
 *  \Last Modified by:   ismaelseidel
 *  \Last Modified time: 2018-07-15 18:48:53
 *
 *  Longer description here
 *	
 *	Developed at Embedded Computing Lab (ECL), 2018.
 *	
 */
 

 // module transpose_buffer_half definition
 module transpose_buffer_half (
 	//inputs and outputs declaration
 	clock,
 	reset,
 	enable,
 	address,
 	in_0,
 	in_1,
 	in_2,
 	in_3,
 	out_0,
 	out_1,
 	out_2,
 	out_3
 );
 
 //------------------------------------------ Parameters -------------------------------------------
 parameter DATAWIDTH = 8;

 
 //------------------------------------------ Input Ports ------------------------------------------
 // input signed[NAME-1:0] in;
 input clock, reset, enable;
 input [DATAWIDTH-1:0] in_0, in_1, in_2, in_3;
 input [1:0] address;
 
 //----------------------------------------- Output Ports ------------------------------------------
 // output signed[NAME-1:0] out;
 output [DATAWIDTH-1:0] out_0, out_1, out_2, out_3;
 
 //---------------------------------------- Internal Wires -----------------------------------------
 // wire signed[NAME-1:0] some_wire;
 wire enable_line0, enable_line1, enable_line2, enable_line3;
 wire [DATAWIDTH-1:0] out_array_0[0:3], out_array_1[0:3], out_array_2[0:3], out_array_3[0:3];
 
 //------------------------------------ Module Instantiations --------------------------------------
 // module_name #(PARAMETERS) custom_module_name (ins and outs);

simple_tb_cell #(DATAWIDTH) cell_0_0 (clock, reset, enable_line0, in_0, out_array_0[0]);
simple_tb_cell #(DATAWIDTH) cell_0_1 (clock, reset, enable_line0, in_1, out_array_1[0]);
simple_tb_cell #(DATAWIDTH) cell_0_2 (clock, reset, enable_line0, in_2, out_array_2[0]);
simple_tb_cell #(DATAWIDTH) cell_0_3 (clock, reset, enable_line0, in_3, out_array_3[0]);
 
simple_tb_cell #(DATAWIDTH) cell_1_0 (clock, reset, enable_line1, in_0, out_array_0[1]);
simple_tb_cell #(DATAWIDTH) cell_1_1 (clock, reset, enable_line1, in_1, out_array_1[1]);
simple_tb_cell #(DATAWIDTH) cell_1_2 (clock, reset, enable_line1, in_2, out_array_2[1]);
simple_tb_cell #(DATAWIDTH) cell_1_3 (clock, reset, enable_line1, in_3, out_array_3[1]);

simple_tb_cell #(DATAWIDTH) cell_2_0 (clock, reset, enable_line2, in_0, out_array_0[2]);
simple_tb_cell #(DATAWIDTH) cell_2_1 (clock, reset, enable_line2, in_1, out_array_1[2]);
simple_tb_cell #(DATAWIDTH) cell_2_2 (clock, reset, enable_line2, in_2, out_array_2[2]);
simple_tb_cell #(DATAWIDTH) cell_2_3 (clock, reset, enable_line2, in_3, out_array_3[2]);

simple_tb_cell #(DATAWIDTH) cell_3_0 (clock, reset, enable_line3, in_0, out_array_0[3]);
simple_tb_cell #(DATAWIDTH) cell_3_1 (clock, reset, enable_line3, in_1, out_array_1[3]);
simple_tb_cell #(DATAWIDTH) cell_3_2 (clock, reset, enable_line3, in_2, out_array_2[3]);
simple_tb_cell #(DATAWIDTH) cell_3_3 (clock, reset, enable_line3, in_3, out_array_3[3]);
 
 //--------------------------------------- Code Starts Here ----------------------------------------

 // always @(posedge clock or posedge reset) begin
 // 	if (reset) begin
 // 		address<=3'd7;
 // 	end
 // 	else if (enable) begin
 // 		address<=address+3'd1;
 // 	end
 // end

assign enable_line0=~(address [1] | address [0]) & enable;
assign enable_line1=~(address [1] | ~address [0]) & enable;
assign enable_line2=(address[1] & ~address[0]) & enable;
assign enable_line3=(address [1]  & address [0]) & enable;


assign out_0 = out_array_0[address];
assign out_1 = out_array_1[address];
assign out_2 = out_array_2[address];
assign out_3 = out_array_3[address];




 endmodule
 
