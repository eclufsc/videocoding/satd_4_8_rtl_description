//-------------------------------------------------------------------------
// Design Name : satd_8x8_4x4_wreuse_reduced_buffer
// File Name   : satd_8x8_4x4_wreuse_reduced_buffer.v
// Function    : 
// Coder       : Ismael Seidel and Marcio Monteiro
//-------------------------------------------------------------------------

module satd_8x8_4x4_wreuse_reduced_buffer(
	clock,
	reset,
	enable,
	original_0,
	original_1,
	original_2,
	original_3,
	original_4,
	original_5,
	original_6,
	original_7,
	candidate_0,
	candidate_1,
	candidate_2,
	candidate_3,
	candidate_4,
	candidate_5,
	candidate_6,
	candidate_7,
	doneAB,
	satd4x4A,
	satd4x4B,
	done,
	satd8x8
);

parameter DATA_WIDTH = 8;

//------------------------------- Input Ports -----------------------------------
input clock, reset, enable;
input [DATA_WIDTH-1:0] original_0, original_1, original_2, original_3, original_4, original_5, original_6, original_7, candidate_0, candidate_1, candidate_2, candidate_3, candidate_4, candidate_5, candidate_6, candidate_7;

//------------------------------- Output Ports -----------------------------------
output [DATA_WIDTH+7:0] satd4x4A, satd4x4B;
output [DATA_WIDTH+8:0] satd8x8;
//output [DATA_WIDTH+4:0] out_HT_0, out_HT_1, out_HT_2, out_HT_3, out_HT_4, out_HT_5, out_HT_6, out_HT_7;
output done, doneAB;

//-------------------------------- Internal Wires ----------------------------
wire enable_inputs4x4, enable_transpose_buffer4x4, change_transpose_buffer_direction4x4, reset_psatd4x4, enable_psatd4x4, reset_satd4x4, enable_satd4x4, enable_transpose_buffer8x8, reset_transpose_buffer4x4, reset_transpose_buffer8x8, change_transpose_buffer_direction8x8, reset_psatd8x8, enable_psatd8x8, reset_satd8x8, enable_satd8x8, enable_reg_barrier;


wire [1:0] address_tb;

//--------------------------------- Code Starts Here -----------------------------
mainControl controlePrincipal( 
	.clock (clock), 
	.reset (reset), 
	.enable (enable), 
	.enable_inputs4x4 (enable_inputs4x4), 
	.reset_transpose_buffer4x4 (reset_transpose_buffer4x4), 
	.enable_transpose_buffer4x4 (enable_transpose_buffer4x4), 
	.change_transpose_buffer_direction4x4 (change_transpose_buffer_direction4x4), 
	.reset_psatd4x4 (reset_psatd4x4), 
	.enable_psatd4x4 (enable_psatd4x4), 
	.reset_satd4x4 (reset_satd4x4), 
	.enable_satd4x4 (enable_satd4x4), 
	.done4x4AB (doneAB), 
	.enable_reg_barrier (enable_reg_barrier), 
	.enable_transpose_buffer8x8 (enable_transpose_buffer8x8), 
	.reset_transpose_buffer8x8 (reset_transpose_buffer8x8), 
	.reset_psatd8x8 (reset_psatd8x8), 
	.enable_psatd8x8 (enable_psatd8x8), 
	.reset_satd8x8 (reset_satd8x8), 
	.enable_satd8x8 (enable_satd8x8), 
	.address_tb (address_tb), 
	.done8x8 (done)
);

mainOperative operativoPrincipal( 
	.clock (clock), 
	.reset (reset), 
	.enable_inputs4x4 (enable_inputs4x4), 
	.enable_transpose_buffer4x4 (enable_transpose_buffer4x4), 
	.enable_reg_barrier (enable_reg_barrier), 
	.reset_psatd4x4 (reset_psatd4x4), 
	.enable_psatd4x4 (enable_psatd4x4), 
	.reset_satd4x4 (reset_satd4x4), 
	.enable_satd4x4 (enable_satd4x4),
	.reset_transpose_buffer8x8 (reset_transpose_buffer8x8),
	.enable_transpose_buffer8x8 (enable_transpose_buffer8x8), 
	.change_transpose_buffer_direction4x4 (change_transpose_buffer_direction4x4), 
	.reset_psatd8x8 (reset_psatd8x8), 
	.enable_psatd8x8 (enable_psatd8x8), 
	.reset_satd8x8 (reset_satd8x8), 
	.enable_satd8x8 (enable_satd8x8), 
	.original_0 (original_0), 
	.original_1 (original_1), 
	.original_2 (original_2), 
	.original_3 (original_3), 
	.original_4 (original_4), 
	.original_5 (original_5), 
	.original_6 (original_6), 
	.original_7 (original_7), 
	.candidate_0 (candidate_0), 
	.candidate_1 (candidate_1), 
	.candidate_2 (candidate_2), 
	.candidate_3 (candidate_3), 
	.candidate_4 (candidate_4), 
	.candidate_5 (candidate_5), 
	.candidate_6 (candidate_6), 
	.candidate_7 (candidate_7), 
	.satd4x4A (satd4x4A), 
	.satd4x4B (satd4x4B), 
	.satd8x8 (satd8x8),
	.address_tb (address_tb)
);

endmodule