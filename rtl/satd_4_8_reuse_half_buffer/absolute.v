/* 
 *	\file absolute.v
 *	\brief Implementation of an parameterized absolute.
 *	\author Ismael Seidel
 *	\Date:   2015-08-13 16:55:45
 *  \Last Modified by:   ismaelseidel
 *  \Last Modified time: 2015-08-05 17:00:35
 *
 *	This absolute implementation is parameterized so as to the number of bits in its inputs. 
 *	Thus, the value DATA_WIDTH is the number of bits of in_0 and also the number of bits of in_1.
 *	Notice that the output is one bit larger, so as to accomodate the carry out of this absolute.
 *	
 *	Developed at Embedded Computing Lab (ECL), 2015.
 *	
 */

//Absolute module definition.
module absolute (
	in,
	out
);

//parameter indicating the number of bits in the input. The output is one bit shorter.
parameter DATA_WIDTH = 8;

//declaration of number of bits for input in.
input signed[DATA_WIDTH-1:0] in;

//declaration of output.
output [DATA_WIDTH-2:0] out;

//the output is assigned to be the inverse of in for the case when in is negative. Otherwise, the output is the input without is first bit (MSB).
assign out = (in[DATA_WIDTH-1]) ? -in[DATA_WIDTH-2:0] : in[DATA_WIDTH-2:0];

endmodule