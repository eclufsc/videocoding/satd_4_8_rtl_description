//-------------------------------------------------------------------------
// Design Name : mainControl
// File Name   : mainControl.v
// Function    : SATD of 4x4 blocks with 4 pixel pairs as input
// Coder       : Ismael Seidel and Marcio Monteiro
//-------------------------------------------------------------------------
module mainControl(
	clock,
	reset,
	enable,
	enable_inputs4x4, // controls the inputs of 4x4
	reset_transpose_buffer4x4,
	enable_transpose_buffer4x4,
	change_transpose_buffer_direction4x4,
	reset_psatd4x4,
	enable_psatd4x4,
	reset_satd4x4,
	enable_satd4x4,
	done4x4AB,
	enable_reg_barrier,
	enable_transpose_buffer8x8,
	reset_transpose_buffer8x8,
	reset_psatd8x8,
	enable_psatd8x8,
	reset_satd8x8,
	enable_satd8x8,
	address_tb,
	done8x8
);

parameter DATA_WIDTH = 8;

//------------------------------- Input Ports -----------------------------------
input clock, reset, enable;


//------------------------------- Output Ports -----------------------------------
output reg enable_inputs4x4, reset_transpose_buffer4x4, enable_transpose_buffer4x4, change_transpose_buffer_direction4x4, reset_psatd4x4, enable_psatd4x4, reset_satd4x4, enable_satd4x4, done4x4AB,  enable_reg_barrier, enable_transpose_buffer8x8, reset_transpose_buffer8x8, reset_psatd8x8, enable_psatd8x8, reset_satd8x8, enable_satd8x8, done8x8;
output reg [1:0] address_tb;

reg [4:0] state;
parameter
IDLE = 0,
INITIALIZE_0 = 1,
INITIALIZE_1 = 2,
INITIALIZE_2 = 3,
INITIALIZE_3 = 4,
INITIALIZE_4 = 5,
INITIALIZE_5 = 6,
INITIALIZE_6 = 7,
INITIALIZE_7 = 8,
CALCULATE_0 = 9,
CALCULATE_1 = 10,
CALCULATE_2 = 11,
CALCULATE_3 = 12,
CALCULATE_4 = 13,
CALCULATE_5 = 14,
CALCULATE_6 = 15,
CALCULATE_7 = 16,
TERMINATE_0 = 17,
TERMINATE_1 = 18,
TERMINATE_2 = 19,
TERMINATE_3 = 20,
TERMINATE_4 = 21,
TERMINATE_5 = 22,
TERMINATE_6 = 23;


  // This code allows you to see state names in simulation
 `ifndef SYNTHESIS
  reg [127:0] statename;
  always @* begin
    case (state)
    	IDLE:
    		statename = "IDLE";
		INITIALIZE_0:
			statename = "IN_0";
		INITIALIZE_1:
			statename = "IN_1";
		INITIALIZE_2:
			statename = "IN_2";
		INITIALIZE_3:
			statename = "IN_3";
		INITIALIZE_4:
			statename = "IN_4";
		INITIALIZE_5:
			statename = "IN_5";
		INITIALIZE_6:
			statename = "IN_6";
		INITIALIZE_7:
			statename = "IN_7";
		CALCULATE_0 :
			statename = "C_0";
		CALCULATE_1 :
			statename = "C_1";
		CALCULATE_2 :
			statename = "C_2";
		CALCULATE_3 :
			statename = "C_3";
		CALCULATE_4 :
			statename = "C_4";
		CALCULATE_5 :
			statename = "C_5";
		CALCULATE_6 :
			statename = "C_6";
		CALCULATE_7 :
			statename = "C_7";
		TERMINATE_0 :
			statename = "T_0";
		TERMINATE_1 :
			statename = "T_1";
		TERMINATE_2 :
			statename = "T_2";
		TERMINATE_3 :
			statename = "T_3";
		TERMINATE_4 :
			statename = "T_4";
		TERMINATE_5 :
			statename = "T_5";
		TERMINATE_6 :
			statename = "T_6";
      	default:
        	statename = "XXXX";
    endcase
  end
 `endif

always @ (state) begin
	case (state)
		IDLE:begin
			enable_inputs4x4 = 1'b0;
			enable_transpose_buffer4x4 = 1'b0;
			change_transpose_buffer_direction4x4 = 1'b0;
			reset_psatd4x4 = 1'b1;
			enable_psatd4x4 = 1'b0;
			reset_satd4x4 = 1'b1;
			enable_satd4x4 = 1'b0;
			reset_transpose_buffer4x4 = 1'b1;
			done4x4AB = 1'b0;
			enable_reg_barrier=1'b0;
			enable_transpose_buffer8x8 = 1'b0;
			reset_psatd8x8 = 1'b1;
			enable_psatd8x8 = 1'b0;
			reset_satd8x8 = 1'b1;
			enable_satd8x8 = 1'b0;
			reset_transpose_buffer8x8 = 1'b1;
			done8x8 = 1'b0;
			address_tb=2'd0;
		end
		INITIALIZE_0:begin
			enable_inputs4x4 = 1'b1;
			enable_transpose_buffer4x4 = 1'b1;
			change_transpose_buffer_direction4x4 = 1'b0;
			reset_psatd4x4 = 1'b0;
			enable_psatd4x4 = 1'b0;
			reset_satd4x4 = 1'b0;
			enable_satd4x4 = 1'b0;
			reset_transpose_buffer4x4 = 1'b0;
			done4x4AB = 1'b0;
			enable_reg_barrier=1'b0;
			enable_transpose_buffer8x8 = 1'b0;
			reset_psatd8x8 = 1'b0;
			enable_psatd8x8 = 1'b0;
			reset_satd8x8 = 1'b0;
			enable_satd8x8 = 1'b0;
			reset_transpose_buffer8x8 = 1'b0;
			done8x8 = 1'b0;
			address_tb=2'd0;
		end
		INITIALIZE_1:begin
			enable_inputs4x4 = 1'b1;
			enable_transpose_buffer4x4 = 1'b1;
			change_transpose_buffer_direction4x4 = 1'b0;
			reset_psatd4x4 = 1'b0;
			enable_psatd4x4 = 1'b0;
			reset_satd4x4 = 1'b0;
			enable_satd4x4 = 1'b0;
			reset_transpose_buffer4x4 = 1'b0;
			done4x4AB = 1'b0;
			enable_reg_barrier=1'b0;
			enable_transpose_buffer8x8 = 1'b0;
			reset_psatd8x8 = 1'b0;
			enable_psatd8x8 = 1'b0;
			reset_satd8x8 = 1'b0;
			enable_satd8x8 = 1'b0;
			reset_transpose_buffer8x8 = 1'b0;
			done8x8 = 1'b0;
			address_tb=2'd0;
		end
		INITIALIZE_2:begin
			enable_inputs4x4 = 1'b1;
			enable_transpose_buffer4x4 = 1'b1;
			change_transpose_buffer_direction4x4 = 1'b0;
			reset_psatd4x4 = 1'b0;
			enable_psatd4x4 = 1'b0;
			reset_satd4x4 = 1'b0;
			enable_satd4x4 = 1'b0;
			reset_transpose_buffer4x4 = 1'b0;
			done4x4AB = 1'b0;
			enable_reg_barrier=1'b0;
			enable_transpose_buffer8x8 = 1'b0;
			reset_psatd8x8 = 1'b0;
			enable_psatd8x8 = 1'b0;
			reset_satd8x8 = 1'b0;
			enable_satd8x8 = 1'b0;
			reset_transpose_buffer8x8 = 1'b0;
			done8x8 = 1'b0;
			address_tb=2'd0;
		end
		INITIALIZE_3:begin
			enable_inputs4x4 = 1'b1;
			enable_transpose_buffer4x4 = 1'b1;
			change_transpose_buffer_direction4x4 = 1'b0;
			reset_psatd4x4 = 1'b0;
			enable_psatd4x4 = 1'b0;
			reset_satd4x4 = 1'b0;
			enable_satd4x4 = 1'b0;
			reset_transpose_buffer4x4 = 1'b0;
			done4x4AB = 1'b0;
			enable_reg_barrier=1'b0;
			enable_transpose_buffer8x8 = 1'b0;
			reset_psatd8x8 = 1'b0;
			enable_psatd8x8 = 1'b0;
			reset_satd8x8 = 1'b0;
			enable_satd8x8 = 1'b0;
			reset_transpose_buffer8x8 = 1'b0;
			done8x8 = 1'b0;
			address_tb=2'd0;
		end
		INITIALIZE_4:begin
			enable_inputs4x4 = 1'b1;
			enable_transpose_buffer4x4 = 1'b1;
			change_transpose_buffer_direction4x4 = 1'b1;
			reset_psatd4x4 = 1'b0;
			enable_psatd4x4 = 1'b0;
			reset_satd4x4 = 1'b0;
			enable_satd4x4 = 1'b0;
			reset_transpose_buffer4x4 = 1'b0;
			done4x4AB = 1'b0;
			enable_reg_barrier=1'b0;
			enable_transpose_buffer8x8 = 1'b0;
			reset_psatd8x8 = 1'b0;
			enable_psatd8x8 = 1'b0;
			reset_satd8x8 = 1'b0;
			enable_satd8x8 = 1'b0;
			reset_transpose_buffer8x8 = 1'b0;
			done8x8 = 1'b0;
			address_tb=2'd0;
		end
		INITIALIZE_5:begin
			enable_inputs4x4 = 1'b1;
			enable_transpose_buffer4x4 = 1'b1;
			change_transpose_buffer_direction4x4 = 1'b0;
			reset_psatd4x4 = 1'b0;
			enable_psatd4x4 = 1'b1;
			reset_satd4x4 = 1'b0;
			enable_satd4x4 = 1'b0;
			reset_transpose_buffer4x4 = 1'b0;
			done4x4AB = 1'b0;
			enable_reg_barrier=1'b0;
			enable_transpose_buffer8x8 = 1'b1;
			reset_psatd8x8 = 1'b0;
			enable_psatd8x8 = 1'b0;
			reset_satd8x8 = 1'b0;
			enable_satd8x8 = 1'b0;
			reset_transpose_buffer8x8 = 1'b0;
			done8x8 = 1'b0;
			address_tb=2'd0;
		end
		INITIALIZE_6:begin
			enable_inputs4x4 = 1'b1;
			enable_transpose_buffer4x4 = 1'b1;
			change_transpose_buffer_direction4x4 = 1'b0;
			reset_psatd4x4 = 1'b0;
			enable_psatd4x4 = 1'b1;
			reset_satd4x4 = 1'b0;
			enable_satd4x4 = 1'b0;
			reset_transpose_buffer4x4 = 1'b0;
			done4x4AB = 1'b0;
			enable_reg_barrier=1'b0;
			enable_transpose_buffer8x8 = 1'b1;
			reset_psatd8x8 = 1'b0;
			enable_psatd8x8 = 1'b0;
			reset_satd8x8 = 1'b0;
			enable_satd8x8 = 1'b0;
			reset_transpose_buffer8x8 = 1'b0;
			done8x8 = 1'b0;
			address_tb=2'd1;
		end
		INITIALIZE_7:begin
			enable_inputs4x4 = 1'b1;
			enable_transpose_buffer4x4 = 1'b1;
			change_transpose_buffer_direction4x4 = 1'b0;
			reset_psatd4x4 = 1'b0;
			enable_psatd4x4 = 1'b1;
			reset_satd4x4 = 1'b0;
			enable_satd4x4 = 1'b0;
			reset_transpose_buffer4x4 = 1'b0;
			done4x4AB = 1'b0;
			enable_reg_barrier=1'b0;
			enable_transpose_buffer8x8 = 1'b1;
			reset_psatd8x8 = 1'b0;
			enable_psatd8x8 = 1'b0;
			reset_satd8x8 = 1'b0;
			enable_satd8x8 = 1'b0;
			reset_transpose_buffer8x8 = 1'b0;
			done8x8 = 1'b0;
			address_tb=2'd2;
		end
		CALCULATE_0:begin
			enable_inputs4x4 = 1'b1;
			enable_transpose_buffer4x4 = 1'b1;
			change_transpose_buffer_direction4x4 = 1'b1;
			reset_psatd4x4 = 1'b1;
			enable_psatd4x4 = 1'b0;
			reset_satd4x4 = 1'b0;
			enable_satd4x4 = 1'b1;
			reset_transpose_buffer4x4 = 1'b0;
			done4x4AB = 1'b0;
			enable_reg_barrier=1'b0;
			enable_transpose_buffer8x8 = 1'b1;
			reset_psatd8x8 = 1'b0;
			enable_psatd8x8 = 1'b0;
			reset_satd8x8 = 1'b0;
			enable_satd8x8 = 1'b0;
			reset_transpose_buffer8x8 = 1'b0;
			done8x8 = 1'b0;
			address_tb=2'd3;
		end
		CALCULATE_1:begin
			enable_inputs4x4 = 1'b1;
			enable_transpose_buffer4x4 = 1'b1;
			change_transpose_buffer_direction4x4 = 1'b0;
			reset_psatd4x4 = 1'b0;
			enable_psatd4x4 = 1'b1;
			reset_satd4x4 = 1'b0;
			enable_satd4x4 = 1'b0;
			reset_transpose_buffer4x4 = 1'b0;
			done4x4AB = 1'b1;
			enable_reg_barrier=1'b1;
			enable_transpose_buffer8x8 = 1'b0;
			reset_psatd8x8 = 1'b0;
			enable_psatd8x8 = 1'b0;
			reset_satd8x8 = 1'b0;
			enable_satd8x8 = 1'b0;
			reset_transpose_buffer8x8 = 1'b0;
			done8x8 = 1'b0;
			address_tb=2'd0;
		end
		CALCULATE_2:begin
			enable_inputs4x4 = 1'b1;
			enable_transpose_buffer4x4 = 1'b1;
			change_transpose_buffer_direction4x4 = 1'b0;
			reset_psatd4x4 = 1'b0;
			enable_psatd4x4 = 1'b1;
			reset_satd4x4 = 1'b0;
			enable_satd4x4 = 1'b0;
			reset_transpose_buffer4x4 = 1'b0;
			done4x4AB = 1'b0;
			enable_reg_barrier=1'b1;
			enable_transpose_buffer8x8 = 1'b0;
			reset_psatd8x8 = 1'b0;
			enable_psatd8x8 = 1'b1;
			reset_satd8x8 = 1'b0;
			enable_satd8x8 = 1'b0;
			reset_transpose_buffer8x8 = 1'b0;
			done8x8 = 1'b0;
			address_tb=2'd1;
		end
		CALCULATE_3:begin
			enable_inputs4x4 = 1'b1;
			enable_transpose_buffer4x4 = 1'b1;
			change_transpose_buffer_direction4x4 = 1'b0;
			reset_psatd4x4 = 1'b0;
			enable_psatd4x4 = 1'b1;
			reset_satd4x4 = 1'b0;
			enable_satd4x4 = 1'b0;
			reset_transpose_buffer4x4 = 1'b0;
			done4x4AB = 1'b0;
			enable_reg_barrier=1'b1;
			enable_transpose_buffer8x8 = 1'b0;
			reset_psatd8x8 = 1'b0;
			enable_psatd8x8 = 1'b1;
			reset_satd8x8 = 1'b0;
			enable_satd8x8 = 1'b0;
			reset_transpose_buffer8x8 = 1'b0;
			done8x8 = 1'b0;
			address_tb=2'd2;
		end
		CALCULATE_4:begin
			enable_inputs4x4 = 1'b1;
			enable_transpose_buffer4x4 = 1'b1;
			change_transpose_buffer_direction4x4 = 1'b1;
			reset_psatd4x4 = 1'b1;
			enable_psatd4x4 = 1'b0;
			reset_satd4x4 = 1'b0;
			enable_satd4x4 = 1'b1;
			reset_transpose_buffer4x4 = 1'b0;
			done4x4AB = 1'b0;
			enable_reg_barrier=1'b1;
			enable_transpose_buffer8x8 = 1'b0;
			reset_psatd8x8 = 1'b0;
			enable_psatd8x8 = 1'b1;
			reset_satd8x8 = 1'b0;
			enable_satd8x8 = 1'b0;
			reset_transpose_buffer8x8 = 1'b0;
			done8x8 = 1'b0;
			address_tb=2'd3;
		end
		CALCULATE_5:begin
			enable_inputs4x4 = 1'b1;
			enable_transpose_buffer4x4 = 1'b1;
			change_transpose_buffer_direction4x4 = 1'b0;
			reset_psatd4x4 = 1'b0;
			enable_psatd4x4 = 1'b1;
			reset_satd4x4 = 1'b0;
			enable_satd4x4 = 1'b0;
			reset_transpose_buffer4x4 = 1'b0;
			done4x4AB = 1'b1;
			enable_reg_barrier=1'b0;
			enable_transpose_buffer8x8 = 1'b1;
			reset_psatd8x8 = 1'b1;
			enable_psatd8x8 = 1'b0;
			reset_satd8x8 = 1'b0;
			enable_satd8x8 = 1'b1;
			reset_transpose_buffer8x8 = 1'b0;
			done8x8 = 1'b0;
			address_tb=2'd0;
		end
		CALCULATE_6:begin
			enable_inputs4x4 = 1'b1;
			enable_transpose_buffer4x4 = 1'b1;
			change_transpose_buffer_direction4x4 = 1'b0;
			reset_psatd4x4 = 1'b0;
			enable_psatd4x4 = 1'b1;
			reset_satd4x4 = 1'b0;
			enable_satd4x4 = 1'b0;
			reset_transpose_buffer4x4 = 1'b0;
			done4x4AB = 1'b0;
			enable_reg_barrier=1'b0;
			enable_transpose_buffer8x8 = 1'b1;
			reset_psatd8x8 = 1'b0;
			enable_psatd8x8 = 1'b0;
			reset_satd8x8 = 1'b0;
			enable_satd8x8 = 1'b0;
			reset_transpose_buffer8x8 = 1'b0;
			done8x8 = 1'b1;
			address_tb=2'd1;
		end
		CALCULATE_7:begin
			enable_inputs4x4 = 1'b1;
			enable_transpose_buffer4x4 = 1'b1;
			change_transpose_buffer_direction4x4 = 1'b0;
			reset_psatd4x4 = 1'b0;
			enable_psatd4x4 = 1'b1;
			reset_satd4x4 = 1'b0;
			enable_satd4x4 = 1'b0;
			reset_transpose_buffer4x4 = 1'b0;
			done4x4AB = 1'b0;
			enable_reg_barrier=1'b0;
			enable_transpose_buffer8x8 = 1'b1;
			reset_psatd8x8 = 1'b0;
			enable_psatd8x8 = 1'b0;
			reset_satd8x8 = 1'b0;
			enable_satd8x8 = 1'b0;
			reset_transpose_buffer8x8 = 1'b0;
			done8x8 = 1'b0;
			address_tb=2'd2;
		end
		TERMINATE_0:begin
			enable_inputs4x4 = 1'b0;
			enable_transpose_buffer4x4 = 1'b1;
			change_transpose_buffer_direction4x4 = 1'b1;
			reset_psatd4x4 = 1'b1;
			enable_psatd4x4 = 1'b0;
			reset_satd4x4 = 1'b0;
			enable_satd4x4 = 1'b1;
			reset_transpose_buffer4x4 = 1'b0;
			done4x4AB = 1'b0;
			enable_reg_barrier=1'b0;
			enable_transpose_buffer8x8 = 1'b1;
			reset_psatd8x8 = 1'b0;
			enable_psatd8x8 = 1'b0;
			reset_satd8x8 = 1'b0;
			enable_satd8x8 = 1'b0;
			reset_transpose_buffer8x8 = 1'b0;
			done8x8 = 1'b0;
			address_tb=2'd3;
		end
		TERMINATE_1:begin
			enable_inputs4x4 = 1'b0;
			enable_transpose_buffer4x4 = 1'b1;
			change_transpose_buffer_direction4x4 = 1'b0;
			reset_psatd4x4 = 1'b0;
			enable_psatd4x4 = 1'b1;
			reset_satd4x4 = 1'b0;
			enable_satd4x4 = 1'b0;
			reset_transpose_buffer4x4 = 1'b0;
			done4x4AB = 1'b1;
			enable_reg_barrier=1'b1;
			enable_transpose_buffer8x8 = 1'b0;
			reset_psatd8x8 = 1'b0;
			enable_psatd8x8 = 1'b0;
			reset_satd8x8 = 1'b0;
			enable_satd8x8 = 1'b0;
			reset_transpose_buffer8x8 = 1'b0;
			done8x8 = 1'b0;
			address_tb=2'd0;
		end
		TERMINATE_2:begin
			enable_inputs4x4 = 1'b0;
			enable_transpose_buffer4x4 = 1'b1;
			change_transpose_buffer_direction4x4 = 1'b0;
			reset_psatd4x4 = 1'b0;
			enable_psatd4x4 = 1'b1;
			reset_satd4x4 = 1'b0;
			enable_satd4x4 = 1'b0;
			reset_transpose_buffer4x4 = 1'b0;
			done4x4AB = 1'b0;
			enable_reg_barrier=1'b1;
			enable_transpose_buffer8x8 = 1'b0;
			reset_psatd8x8 = 1'b0;
			enable_psatd8x8 = 1'b1;
			reset_satd8x8 = 1'b0;
			enable_satd8x8 = 1'b0;
			reset_transpose_buffer8x8 = 1'b0;
			done8x8 = 1'b0;
			address_tb=2'd1;
		end
		TERMINATE_3:begin
			enable_inputs4x4 = 1'b0;
			enable_transpose_buffer4x4 = 1'b1;
			change_transpose_buffer_direction4x4 = 1'b0;
			reset_psatd4x4 = 1'b0;
			enable_psatd4x4 = 1'b1;
			reset_satd4x4 = 1'b0;
			enable_satd4x4 = 1'b0;
			reset_transpose_buffer4x4 = 1'b0;
			done4x4AB = 1'b0;
			enable_reg_barrier=1'b1;
			enable_transpose_buffer8x8 = 1'b0;
			reset_psatd8x8 = 1'b0;
			enable_psatd8x8 = 1'b1;
			reset_satd8x8 = 1'b0;
			enable_satd8x8 = 1'b0;
			reset_transpose_buffer8x8 = 1'b0;
			done8x8 = 1'b0;
			address_tb=2'd2;
		end
		TERMINATE_4:begin
			enable_inputs4x4 = 1'b0;
			enable_transpose_buffer4x4 = 1'b1;
			change_transpose_buffer_direction4x4 = 1'b1;
			reset_psatd4x4 = 1'b1;
			enable_psatd4x4 = 1'b0;
			reset_satd4x4 = 1'b0;
			enable_satd4x4 = 1'b1;
			reset_transpose_buffer4x4 = 1'b0;
			done4x4AB = 1'b0;
			enable_reg_barrier=1'b1;
			enable_transpose_buffer8x8 = 1'b0;
			reset_psatd8x8 = 1'b0;
			enable_psatd8x8 = 1'b1;
			reset_satd8x8 = 1'b0;
			enable_satd8x8 = 1'b0;
			reset_transpose_buffer8x8 = 1'b0;
			done8x8 = 1'b0;
			address_tb=2'd3;
		end
		TERMINATE_5:begin
			enable_inputs4x4 = 1'b0;
			enable_transpose_buffer4x4 = 1'b0;
			change_transpose_buffer_direction4x4 = 1'b0;
			reset_psatd4x4 = 1'b0;
			enable_psatd4x4 = 1'b1;
			reset_satd4x4 = 1'b0;
			enable_satd4x4 = 1'b0;
			reset_transpose_buffer4x4 = 1'b0;
			done4x4AB = 1'b1;
			enable_reg_barrier=1'b0;
			enable_transpose_buffer8x8 = 1'b1;
			reset_psatd8x8 = 1'b1;
			enable_psatd8x8 = 1'b0;
			reset_satd8x8 = 1'b0;
			enable_satd8x8 = 1'b1;
			reset_transpose_buffer8x8 = 1'b0;
			done8x8 = 1'b0;
			address_tb=2'd0;
		end
		TERMINATE_6:begin
			enable_inputs4x4 = 1'b0;
			enable_transpose_buffer4x4 = 1'b0;
			change_transpose_buffer_direction4x4 = 1'b0;
			reset_psatd4x4 = 1'b0;
			enable_psatd4x4 = 1'b1;
			reset_satd4x4 = 1'b0;
			enable_satd4x4 = 1'b0;
			reset_transpose_buffer4x4 = 1'b0;
			done4x4AB = 1'b0;
			enable_reg_barrier=1'b0;
			enable_transpose_buffer8x8 = 1'b1;
			reset_psatd8x8 = 1'b0;
			enable_psatd8x8 = 1'b0;
			reset_satd8x8 = 1'b0;
			enable_satd8x8 = 1'b0;
			reset_transpose_buffer8x8 = 1'b0;
			done8x8 = 1'b1;
			address_tb=2'd1;
		end
		default:begin
			enable_inputs4x4 = 1'b0;
			enable_transpose_buffer4x4 = 1'b0;
			change_transpose_buffer_direction4x4 = 1'b0;
			reset_psatd4x4 = 1'b0;
			enable_psatd4x4 = 1'b0;
			reset_satd4x4 = 1'b0;
			enable_satd4x4 = 1'b0;
			reset_transpose_buffer4x4 = 1'b0;
			done4x4AB = 1'b0;
			enable_reg_barrier=1'b0;
			enable_transpose_buffer8x8 = 1'b0;
			reset_psatd8x8 = 1'b0;
			enable_psatd8x8 = 1'b0;
			reset_satd8x8 = 1'b0;
			enable_satd8x8 = 1'b0;
			reset_transpose_buffer8x8 = 1'b0;
			done8x8 = 1'b0;
			address_tb=2'd0;
		end
	endcase
end

always @ (posedge clock or posedge reset) begin
	if (reset)
		state <= IDLE;
	else
		case (state)
			IDLE: if(enable)
				state<= INITIALIZE_0;
			INITIALIZE_0: state<=INITIALIZE_1;
			INITIALIZE_1: state<=INITIALIZE_2;
			INITIALIZE_2: state<=INITIALIZE_3;
			INITIALIZE_3: state<=INITIALIZE_4;
			INITIALIZE_4: state<=INITIALIZE_5;
			INITIALIZE_5: state<=INITIALIZE_6;
			INITIALIZE_6: state<=INITIALIZE_7;
			INITIALIZE_7: if(enable)
				state<=CALCULATE_0;
			else
				state<=TERMINATE_0;

			CALCULATE_0: state<=CALCULATE_1;
			CALCULATE_1: state<=CALCULATE_2;
			CALCULATE_2: state<=CALCULATE_3;
			CALCULATE_3: state<=CALCULATE_4;
			CALCULATE_4: state<=CALCULATE_5;
			CALCULATE_5: state<=CALCULATE_6;
			CALCULATE_6: state<=CALCULATE_7;
			CALCULATE_7: if(enable)
				state<=CALCULATE_0;
			else
				state<=TERMINATE_0;
			TERMINATE_0: state<= TERMINATE_1;
			TERMINATE_1: state<= TERMINATE_2;
			TERMINATE_2: state<= TERMINATE_3;
			TERMINATE_3: state<= TERMINATE_4;
			TERMINATE_4: state<= TERMINATE_5;
			TERMINATE_5: state<= TERMINATE_6;
			TERMINATE_6: state<= IDLE;
		endcase
	end
endmodule