//-------------------------------------------------------------------------
// Design Name : abs_layer_8inputs
// File Name   : abs_layer_8inputs.v
// Function    : Performs the combinational absolute value extraction over an array of 8 signed values
// Coder       : Ismael Seidel
//-------------------------------------------------------------------------
module abs_layer_8inputs (
	in_0,
	in_1,
	in_2,
	in_3,
	in_4,
	in_5,
	in_6,
	in_7,
	out_0,
	out_1,
	out_2,
	out_3,
	out_4,
	out_5,
	out_6,
	out_7
);

parameter DATA_WIDTH = 8;

//------------------------------- Input Ports -----------------------------------
input signed [DATA_WIDTH-1:0] in_0, in_1, in_2, in_3, in_4, in_5, in_6, in_7;

//------------------------------- Output Ports -----------------------------------
output [DATA_WIDTH-2:0] out_0, out_1, out_2, out_3, out_4, out_5, out_6, out_7;

//--------------------------------- Code Starts Here -----------------------------
absolute #(DATA_WIDTH) abs_0(in_0, out_0);
absolute #(DATA_WIDTH) abs_1(in_1, out_1);
absolute #(DATA_WIDTH) abs_2(in_2, out_2);
absolute #(DATA_WIDTH) abs_3(in_3, out_3);
absolute #(DATA_WIDTH) abs_4(in_4, out_4);
absolute #(DATA_WIDTH) abs_5(in_5, out_5);
absolute #(DATA_WIDTH) abs_6(in_6, out_6);
absolute #(DATA_WIDTH) abs_7(in_7, out_7);

endmodule