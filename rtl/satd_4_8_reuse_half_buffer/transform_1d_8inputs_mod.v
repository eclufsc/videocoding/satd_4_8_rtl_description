module transform_1d_8inputs_mod (
	in_0,
	in_1,
	in_2,
	in_3,
	in_4,
	in_5,
	in_6,
	in_7,
	out_0,
	out_1,
	out_2,
	out_3,
	out_4,
	out_5,
	out_6,
	out_7
);

parameter DATA_WIDTH = 8;

input signed [DATA_WIDTH-1:0] in_0, in_1, in_2, in_3, in_4, in_5, in_6, in_7;
output signed [DATA_WIDTH:0] out_0, out_1, out_2, out_3, out_4, out_5, out_6, out_7;

butterfly2 #(DATA_WIDTH) butterfly2_0_0(in_0, in_4, out_0, out_4);
butterfly2 #(DATA_WIDTH) butterfly2_0_2(in_1, in_5, out_1, out_5);
butterfly2 #(DATA_WIDTH) butterfly2_0_4(in_2, in_6, out_2, out_6);
butterfly2 #(DATA_WIDTH) butterfly2_0_6(in_3, in_7, out_3, out_7);

endmodule