/* 
 *	\file unsigned_max_layer_8pairs.v
 *	\brief Description
 *	\author ismaelseidel
 *	\Date:   2018-07-15 20:41:58
 *  \Last Modified by:   ismaelseidel
 *  \Last Modified time: 2018-07-15 20:41:36
 *
 *  Longer description here
 *	
 *	Developed at Embedded Computing Lab (ECL), 2018.
 *	
 */
 

 // module unsigned_max_layer_8pairs definition
 module unsigned_max_layer_8pairs (
 	//inputs and outputs declaration
 	in_0,
 	in_1,
 	in_2,
 	in_3,
 	in_4,
 	in_5,
 	in_6,
 	in_7,
 	in_8,
 	in_9,
 	in_10,
 	in_11,
 	in_12,
 	in_13,
 	in_14,
 	in_15,
 	out_0,
 	out_1,
 	out_2,
 	out_3,
 	out_4,
 	out_5,
 	out_6,
 	out_7
 );
 
 //------------------------------------------ Parameters -------------------------------------------
 parameter DATAWIDTH=8;
 
 
 //------------------------------------------ Input Ports ------------------------------------------
 // input signed[NAME-1:0] in;
 input [DATAWIDTH-1:0] in_0, in_1, in_2, in_3, in_4, in_5, in_6, in_7, in_8, in_9, in_10, in_11, in_12, in_13, in_14, in_15;

 //----------------------------------------- Output Ports ------------------------------------------
 // output signed[NAME-1:0] out;
output [DATAWIDTH-1:0] out_0, out_1, out_2, out_3, out_4, out_5, out_6, out_7;
 
 //---------------------------------------- Internal Wires -----------------------------------------
 // wire signed[NAME-1:0] some_wire;
 
 
 //------------------------------------ Module Instantiations --------------------------------------
 // module_name #(PARAMETERS) custom_module_name (ins and outs);
 unsigned_max #(DATAWIDTH) max_0 (in_0, in_1, out_0);
 unsigned_max #(DATAWIDTH) max_1 (in_2, in_3, out_1);
 unsigned_max #(DATAWIDTH) max_2 (in_4, in_5, out_2);
 unsigned_max #(DATAWIDTH) max_3 (in_6, in_7, out_3);
 unsigned_max #(DATAWIDTH) max_4 (in_8, in_9, out_4);
 unsigned_max #(DATAWIDTH) max_5 (in_10, in_11, out_5);
 unsigned_max #(DATAWIDTH) max_6 (in_12, in_13, out_6);
 unsigned_max #(DATAWIDTH) max_7 (in_14, in_15, out_7);
 
 
 //--------------------------------------- Code Starts Here ----------------------------------------
 // always ...
 
 endmodule
 