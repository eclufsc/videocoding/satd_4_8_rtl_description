//-------------------------------------------------------------------------
// Design Name : input_buffer_8pairs
// File Name   : input_buffer_8pairs.v
// Function    : A simple module to hold the 8 inputs
// Coder       : Ismael Seidel
//-------------------------------------------------------------------------
module unsigned_register_layer_8inputs (
	clock,
	reset,
	enable,
	in_0,
	in_1,
	in_2,
	in_3,
	in_4,
	in_5,
	in_6,
	in_7,
	out_0,
	out_1,
	out_2,
	out_3,
	out_4,
	out_5,
	out_6,
	out_7
);

parameter DATAWIDTH = 8;


//------------------------------- Input Ports -----------------------------------
input clock, reset, enable;
input [DATAWIDTH-1:0] in_0, in_1, in_2, in_3, in_4, in_5, in_6, in_7;

//------------------------------- Output Ports -----------------------------------
output reg [DATAWIDTH-1:0] out_0, out_1, out_2, out_3, out_4, out_5, out_6, out_7;

//--------------------------------- Code Starts Here -----------------------------
always @(posedge clock or posedge reset) begin
	if (reset) begin
		out_0 <= 0;
		out_1 <= 0;
		out_2 <= 0;
		out_3 <= 0;
		out_4 <= 0;
		out_5 <= 0;
		out_6 <= 0;
		out_7 <= 0;
	end
	else if (enable) begin
		out_0 <= in_0;
		out_1 <= in_1;
		out_2 <= in_2;
		out_3 <= in_3;
		out_4 <= in_4;
		out_5 <= in_5;
		out_6 <= in_6;
		out_7 <= in_7;
	end
end


endmodule

