//-------------------------------------------------------------------------
// Design Name : difference_layer_4pairs
// File Name   : difference_layer_4pairs.v
// Function    : Differences over an array of 4 unsigned input pairs
// Coder       : Ismael Seidel
//-------------------------------------------------------------------------
module difference_layer_4pairs (
	in_A_0,
	in_A_1,
	in_A_2,
	in_A_3,
	in_B_0,
	in_B_1,
	in_B_2,
	in_B_3,
	out_0,
	out_1,
	out_2,
	out_3
);parameter DATA_WIDTH = 8;


//------------------------------- Input Ports -----------------------------------
input [DATA_WIDTH-1:0] in_A_0, in_A_1, in_A_2, in_A_3, in_B_0, in_B_1, in_B_2, in_B_3;

//------------------------------- Output Ports -----------------------------------
output [DATA_WIDTH:0] out_0, out_1, out_2, out_3;

//--------------------------------- Code Starts Here -----------------------------
difference_of_unsigned_inputs #(DATA_WIDTH) difference_0(in_A_0, in_B_0, out_0);
difference_of_unsigned_inputs #(DATA_WIDTH) difference_1(in_A_1, in_B_1, out_1);
difference_of_unsigned_inputs #(DATA_WIDTH) difference_2(in_A_2, in_B_2, out_2);
difference_of_unsigned_inputs #(DATA_WIDTH) difference_3(in_A_3, in_B_3, out_3);

endmodule