//-------------------------------------------------------------------------
// Design Name : satd_8x8_8pairs_operative
// File Name   : satd_8x8_8pairs_operative.v
// Function    : SATD of 8x8 blocks with 8 pixel pairs as input
// Coder       : Ismael Seidel
//-------------------------------------------------------------------------
module satd_8x8_8pairs_operative(
	clock,
	reset,
	reset_transpose_buffer,
	enable_transpose_buffer,
	change_transpose_buffer_direction,
	reset_psatd,
	enable_psatd,
	reset_satd,
	enable_satd,
	out_HT_0,
	out_HT_1,
	out_HT_2,
	out_HT_3,
	out_HT_4,
	out_HT_5,
	out_HT_6,
	out_HT_7,
	satd
);

parameter DATA_WIDTH = 8;

//------------------------------- Input Ports -----------------------------------
input clock, reset, reset_transpose_buffer, enable_transpose_buffer, change_transpose_buffer_direction, reset_psatd, enable_psatd, reset_satd, enable_satd;
input [DATA_WIDTH+4:0] out_HT_0, out_HT_1, out_HT_2, out_HT_3, out_HT_4, out_HT_5, out_HT_6, out_HT_7;

//------------------------------- Output Ports -----------------------------------
output reg [DATA_WIDTH+11:0] satd;

//-------------------------------- Internal Wires ----------------------------
wire transpose_buffer_direction;
wire signed [DATA_WIDTH+5:0] out_of_1st_1d_transform_0, out_of_1st_1d_transform_1, out_of_1st_1d_transform_2, out_of_1st_1d_transform_3, out_of_1st_1d_transform_4, out_of_1st_1d_transform_5, out_of_1st_1d_transform_6, out_of_1st_1d_transform_7;
wire signed [DATA_WIDTH+5:0] out_of_transpose_buffer_0, out_of_transpose_buffer_1, out_of_transpose_buffer_2, out_of_transpose_buffer_3, out_of_transpose_buffer_4, out_of_transpose_buffer_5, out_of_transpose_buffer_6, out_of_transpose_buffer_7;
wire signed [DATA_WIDTH+6:0] out_of_2nd_1d_transform_0, out_of_2nd_1d_transform_1, out_of_2nd_1d_transform_2, out_of_2nd_1d_transform_3, out_of_2nd_1d_transform_4, out_of_2nd_1d_transform_5, out_of_2nd_1d_transform_6, out_of_2nd_1d_transform_7;
wire [DATA_WIDTH+5:0] absolute_transformed_difference_0, absolute_transformed_difference_1, absolute_transformed_difference_2, absolute_transformed_difference_3, absolute_transformed_difference_4, absolute_transformed_difference_5, absolute_transformed_difference_6, absolute_transformed_difference_7;
wire [DATA_WIDTH+8:0] sum;
wire [DATA_WIDTH+12:0] psatd;

//--------------------------------- Code Starts Here -----------------------------
tff_async_reset transpose_buffer_direction_holder(change_transpose_buffer_direction, clock, reset, transpose_buffer_direction);

transform_1d_8inputs_mod #(DATA_WIDTH+5) first_1d_transform(out_HT_0, out_HT_1, out_HT_2, out_HT_3, out_HT_4, out_HT_5, out_HT_6, out_HT_7, out_of_1st_1d_transform_0, out_of_1st_1d_transform_1, out_of_1st_1d_transform_2, out_of_1st_1d_transform_3, out_of_1st_1d_transform_4, out_of_1st_1d_transform_5, out_of_1st_1d_transform_6, out_of_1st_1d_transform_7);

transpose_buffer_8x8 #(DATA_WIDTH+6) transpose_buffer(clock, reset_transpose_buffer, enable_transpose_buffer, transpose_buffer_direction, out_of_1st_1d_transform_0, out_of_1st_1d_transform_1, out_of_1st_1d_transform_2, out_of_1st_1d_transform_3, out_of_1st_1d_transform_4, out_of_1st_1d_transform_5, out_of_1st_1d_transform_6, out_of_1st_1d_transform_7, out_of_transpose_buffer_0, out_of_transpose_buffer_1, out_of_transpose_buffer_2, out_of_transpose_buffer_3, out_of_transpose_buffer_4, out_of_transpose_buffer_5, out_of_transpose_buffer_6, out_of_transpose_buffer_7);

transform_1d_8inputs_mod_sec #(DATA_WIDTH+6) second_1d_transform(out_of_transpose_buffer_0, out_of_transpose_buffer_1, out_of_transpose_buffer_2, out_of_transpose_buffer_3, out_of_transpose_buffer_4, out_of_transpose_buffer_5, out_of_transpose_buffer_6, out_of_transpose_buffer_7, out_of_2nd_1d_transform_0, out_of_2nd_1d_transform_1, out_of_2nd_1d_transform_2, out_of_2nd_1d_transform_3, out_of_2nd_1d_transform_4, out_of_2nd_1d_transform_5, out_of_2nd_1d_transform_6, out_of_2nd_1d_transform_7);

abs_layer_8inputs #(DATA_WIDTH+7) absolute1(out_of_2nd_1d_transform_0, out_of_2nd_1d_transform_1, out_of_2nd_1d_transform_2, out_of_2nd_1d_transform_3, out_of_2nd_1d_transform_4, out_of_2nd_1d_transform_5, out_of_2nd_1d_transform_6, out_of_2nd_1d_transform_7, absolute_transformed_difference_0, absolute_transformed_difference_1, absolute_transformed_difference_2, absolute_transformed_difference_3, absolute_transformed_difference_4, absolute_transformed_difference_5, absolute_transformed_difference_6, absolute_transformed_difference_7);

sum_tree_8inputs #(DATA_WIDTH+6) sum_tree(absolute_transformed_difference_0, absolute_transformed_difference_1, absolute_transformed_difference_2, absolute_transformed_difference_3, absolute_transformed_difference_4, absolute_transformed_difference_5, absolute_transformed_difference_6, absolute_transformed_difference_7, sum);

accumulator #(DATA_WIDTH+9, DATA_WIDTH+13) acc(clock, reset_psatd, enable_psatd, sum, psatd);

always @(posedge clock) begin
	if (reset_satd) begin
		satd <= 0;
	end
	else if (enable_satd) begin
		satd <= (psatd+sum+2)>>2;
	end
end

endmodule