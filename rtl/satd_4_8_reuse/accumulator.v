//-------------------------------------------------------------------------
// Design Name : accumulator
// File Name   : accumulator.v
// Function    : Accumulator to sum up values in a sequential fashion
// Coder       : Ismael Seidel
//-------------------------------------------------------------------------
module accumulator (
	clock,
	reset,
	enable,
	in,
	out
);

parameter IN_DATA_WIDTH = 8;
parameter MAX_DATA_WIDTH = 12;

//------------------------------- Input Ports -----------------------------------
input clock, reset, enable;
input [IN_DATA_WIDTH-1:0] in;


//------------------------------- Output Ports -----------------------------------
output [MAX_DATA_WIDTH-1:0] out;

//-------------------------------- Internal Regiters ----------------------------
reg [MAX_DATA_WIDTH-1:0] out;

//--------------------------------- Code Starts Here -----------------------------
always @(posedge clock) begin
	if (reset) begin
		out <= 0;		
	end
	else if (enable) begin
		out <=out+in;
	end
end

endmodule