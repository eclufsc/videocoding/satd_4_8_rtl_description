module transform_1d_4inputs_second (
	in_0,
	in_1,
	in_2,
	in_3,
	out_0,
	out_1,
	out_2,
	out_3
);

parameter DATA_WIDTH = 8;

input signed [DATA_WIDTH-1:0] in_0, in_1, in_2, in_3;
output signed [DATA_WIDTH+1:0] out_0, out_1, out_2, out_3;
wire [DATA_WIDTH:0] wire_0_0, wire_0_1, wire_0_2, wire_0_3;
//first layer (0)
butterfly2 #(DATA_WIDTH) butterfly2_0_0(in_0, in_1, wire_0_0, wire_0_1);
butterfly2 #(DATA_WIDTH) butterfly2_0_2(in_2, in_3, wire_0_2, wire_0_3);
//last layer (1)
butterfly2 #(DATA_WIDTH+1) butterfly2_1_0(wire_0_0, wire_0_2, out_0, out_1);
butterfly2 #(DATA_WIDTH+1) butterfly2_1_1(wire_0_1, wire_0_3, out_2, out_3);

endmodule