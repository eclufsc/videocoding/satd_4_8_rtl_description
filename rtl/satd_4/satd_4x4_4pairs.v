//-------------------------------------------------------------------------
// Design Name : satd_4x4_4pairs
// File Name   : satd_4x4_4pairs.v
// Function    : SATD of 4x4 blocks with 4 pixel pairs as input
// Coder       : Ismael Seidel
//-------------------------------------------------------------------------
module satd_4x4_4pairs(
	clock,
	reset,
	enable,
	original_0,
	original_1,
	original_2,
	original_3,
	candidate_0,
	candidate_1,
	candidate_2,
	candidate_3,
	satd,
	done
);

parameter DATA_WIDTH = 8;

//------------------------------- Input Ports -----------------------------------

input clock, reset, enable;
input [DATA_WIDTH-1:0] original_0, original_1, original_2, original_3, candidate_0, candidate_1, candidate_2, candidate_3;

//------------------------------- Output Ports -----------------------------------

output [DATA_WIDTH+7:0] satd;
output done;

//-------------------------------- Internal Wires ----------------------------
wire enable_inputs, reset_transpose_buffer, enable_transpose_buffer, change_transpose_buffer_direction, reset_psatd, enable_psatd, reset_satd, enable_satd;

//--------------------------------- Code Starts Here -----------------------------
satd_4x4_4pairs_control control(clock, reset, enable, enable_inputs, reset_transpose_buffer, enable_transpose_buffer, change_transpose_buffer_direction, reset_psatd, enable_psatd, reset_satd, enable_satd, done);
satd_4x4_4pairs_operative operative(clock, reset, enable_inputs, reset_transpose_buffer, enable_transpose_buffer, change_transpose_buffer_direction, reset_psatd, enable_psatd, reset_satd, enable_satd, original_0, original_1, original_2, original_3, candidate_0, candidate_1, candidate_2, candidate_3, satd);

endmodule

