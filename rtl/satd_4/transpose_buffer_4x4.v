module transpose_buffer_4x4 (
	clock, 
	reset,
	enable,
	direction,
	in_0,
	in_1,
	in_2,
	in_3,
	out_0,
	out_1,
	out_2,
	out_3
);

parameter DATA_WIDTH = 8;

input clock, reset, enable, direction;
input signed [DATA_WIDTH-1:0] in_0, in_1, in_2, in_3;
output signed [DATA_WIDTH-1:0] out_0, out_1, out_2, out_3;
wire signed [DATA_WIDTH-1:0] out_of_0_0, out_of_0_1, out_of_0_2, out_of_0_3, out_of_1_0, out_of_1_1, out_of_1_2, out_of_1_3, out_of_2_0, out_of_2_1, out_of_2_2, out_of_2_3, out_of_3_0, out_of_3_1, out_of_3_2, out_of_3_3;

transpose_buffer_cell #(DATA_WIDTH) tb_cell_0_0(clock, reset, enable, direction, in_3, in_0, out_of_0_0);
transpose_buffer_cell #(DATA_WIDTH) tb_cell_0_1(clock, reset, enable, direction, in_2, out_of_0_0, out_of_0_1);
transpose_buffer_cell #(DATA_WIDTH) tb_cell_0_2(clock, reset, enable, direction, in_1, out_of_0_1, out_of_0_2);
transpose_buffer_cell #(DATA_WIDTH) tb_cell_0_3(clock, reset, enable, direction, in_0, out_of_0_2, out_of_0_3);
transpose_buffer_cell #(DATA_WIDTH) tb_cell_1_0(clock, reset, enable, direction, out_of_0_0, in_1, out_of_1_0);
transpose_buffer_cell #(DATA_WIDTH) tb_cell_1_1(clock, reset, enable, direction, out_of_0_1, out_of_1_0, out_of_1_1);
transpose_buffer_cell #(DATA_WIDTH) tb_cell_1_2(clock, reset, enable, direction, out_of_0_2, out_of_1_1, out_of_1_2);
transpose_buffer_cell #(DATA_WIDTH) tb_cell_1_3(clock, reset, enable, direction, out_of_0_3, out_of_1_2, out_of_1_3);
transpose_buffer_cell #(DATA_WIDTH) tb_cell_2_0(clock, reset, enable, direction, out_of_1_0, in_2, out_of_2_0);
transpose_buffer_cell #(DATA_WIDTH) tb_cell_2_1(clock, reset, enable, direction, out_of_1_1, out_of_2_0, out_of_2_1);
transpose_buffer_cell #(DATA_WIDTH) tb_cell_2_2(clock, reset, enable, direction, out_of_1_2, out_of_2_1, out_of_2_2);
transpose_buffer_cell #(DATA_WIDTH) tb_cell_2_3(clock, reset, enable, direction, out_of_1_3, out_of_2_2, out_of_2_3);
transpose_buffer_cell #(DATA_WIDTH) tb_cell_3_0(clock, reset, enable, direction, out_of_2_0, in_3, out_of_3_0);
transpose_buffer_cell #(DATA_WIDTH) tb_cell_3_1(clock, reset, enable, direction, out_of_2_1, out_of_3_0, out_of_3_1);
transpose_buffer_cell #(DATA_WIDTH) tb_cell_3_2(clock, reset, enable, direction, out_of_2_2, out_of_3_1, out_of_3_2);
transpose_buffer_cell #(DATA_WIDTH) tb_cell_3_3(clock, reset, enable, direction, out_of_2_3, out_of_3_2, out_of_3_3);

assign out_0 = (direction) ? out_of_0_3 : out_of_3_3;
assign out_1 = (direction) ? out_of_1_3 : out_of_3_2;
assign out_2 = (direction) ? out_of_2_3 : out_of_3_1;
assign out_3 = (direction) ? out_of_3_3 : out_of_3_0;

endmodule

