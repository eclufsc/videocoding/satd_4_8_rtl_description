module butterfly2 (
	in_0,
	in_1,
	out_0,
	out_1
);

parameter DATA_WIDTH = 8;

input signed [DATA_WIDTH-1:0] in_0, in_1;
output signed [DATA_WIDTH:0] out_0, out_1;

assign out_0 = in_0 + in_1;
assign out_1 = in_0 - in_1;

endmodule