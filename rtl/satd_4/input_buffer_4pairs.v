//-------------------------------------------------------------------------
// Design Name : input_buffer_4pairs
// File Name   : input_buffer_4pairs.v
// Function    : A simple module to hold the 4 inputs
// Coder       : Ismael Seidel
//-------------------------------------------------------------------------
module input_buffer_4pairs (
	clock,
	reset,
	enable,
	in_0,
	in_1,
	in_2,
	in_3,
	in_4,
	in_5,
	in_6,
	in_7,
	out_0,
	out_1,
	out_2,
	out_3,
	out_4,
	out_5,
	out_6,
	out_7
);

parameter DATA_WIDTH = 8;


//------------------------------- Input Ports -----------------------------------
input clock, reset, enable;
input [DATA_WIDTH-1:0] in_0, in_1, in_2, in_3, in_4, in_5, in_6, in_7;

//------------------------------- Output Ports -----------------------------------
output reg [DATA_WIDTH-1:0] out_0, out_1, out_2, out_3, out_4, out_5, out_6, out_7;

//--------------------------------- Code Starts Here -----------------------------
always @(posedge clock or posedge reset) begin
	if (reset) begin
		out_0 <= 0;
		out_1 <= 0;
		out_2 <= 0;
		out_3 <= 0;
		out_4 <= 0;
		out_5 <= 0;
		out_6 <= 0;
		out_7 <= 0;
	end
	else if (enable) begin
		out_0 <= in_0;
		out_1 <= in_1;
		out_2 <= in_2;
		out_3 <= in_3;
		out_4 <= in_4;
		out_5 <= in_5;
		out_6 <= in_6;
		out_7 <= in_7;
	end
end


endmodule

