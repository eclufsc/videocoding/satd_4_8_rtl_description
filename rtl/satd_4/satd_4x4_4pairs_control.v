//-------------------------------------------------------------------------
// Design Name : satd_4x4_4pairs_control
// File Name   : satd_4x4_4pairs_control.v
// Function    : SATD of 4x4 blocks with 4 pixel pairs as input
// Coder       : Ismael Seidel
//-------------------------------------------------------------------------
module satd_4x4_4pairs_control(
	clock,
	reset,
	enable,
	enable_inputs,
	reset_transpose_buffer,
	enable_transpose_buffer,
	change_transpose_buffer_direction,
	reset_psatd,
	enable_psatd,
	reset_satd,
	enable_satd,
	done
);

parameter DATA_WIDTH = 8;

//------------------------------- Input Ports -----------------------------------
input clock, reset, enable;

//------------------------------- Output Ports -----------------------------------
output reg enable_inputs, reset_transpose_buffer, enable_transpose_buffer, change_transpose_buffer_direction, reset_psatd, enable_psatd, reset_satd, enable_satd, done;

reg [4:0] state;
parameter
IDLE = 0,
INITIALIZE_0 = 1,
INITIALIZE_1 = 2,
INITIALIZE_2 = 3,
INITIALIZE_3 = 4,
PRE_CALCULATE_0 = 5,
PRE_CALCULATE_1 = 6,
CALCULATE_0 = 7,
CALCULATE_1 = 8,
CALCULATE_2 = 9,
CALCULATE_3 = 10,
TERMINATE_0 = 11,
TERMINATE_1 = 12,
TERMINATE_2 = 13,
TERMINATE_3 = 14,
LAST_0 = 15,
LAST_1 = 16,
PRE_TERMINATE_0 = 17,
PRE_TERMINATE_1 = 18;

always @ (state) begin
	case (state)
		IDLE:begin
			enable_inputs = 1'b0;
			enable_transpose_buffer = 1'b0;
			change_transpose_buffer_direction = 1'b0;
			reset_psatd = 1'b1;
			enable_psatd = 1'b0;
			reset_satd = 1'b1;
			enable_satd = 1'b0;
			reset_transpose_buffer = 1'b1;
			done = 1'b0;
		end
		INITIALIZE_0:begin
			enable_inputs = 1'b1;
			enable_transpose_buffer = 1'b0;
			change_transpose_buffer_direction = 1'b0;
			reset_psatd = 1'b0;
			enable_psatd = 1'b0;
			reset_satd = 1'b0;
			enable_satd = 1'b0;
			reset_transpose_buffer = 1'b0;
			done = 1'b0;
		end
		INITIALIZE_1:begin
			enable_inputs = 1'b1;
			enable_transpose_buffer = 1'b1;
			change_transpose_buffer_direction = 1'b0;
			reset_psatd = 1'b0;
			enable_psatd = 1'b0;
			reset_satd = 1'b0;
			enable_satd = 1'b0;
			reset_transpose_buffer = 1'b0;
			done = 1'b0;
		end
		INITIALIZE_2:begin
			enable_inputs = 1'b1;
			enable_transpose_buffer = 1'b1;
			change_transpose_buffer_direction = 1'b0;
			reset_psatd = 1'b0;
			enable_psatd = 1'b0;
			reset_satd = 1'b0;
			enable_satd = 1'b0;
			reset_transpose_buffer = 1'b0;
			done = 1'b0;
		end
		INITIALIZE_3:begin
			enable_inputs = 1'b1;
			enable_transpose_buffer = 1'b1;
			change_transpose_buffer_direction = 1'b0;
			reset_psatd = 1'b0;
			enable_psatd = 1'b0;
			reset_satd = 1'b0;
			enable_satd = 1'b0;
			reset_transpose_buffer = 1'b0;
			done = 1'b0;
		end
		PRE_CALCULATE_0:begin
			enable_inputs = 1'b1;
			enable_transpose_buffer = 1'b1;
			change_transpose_buffer_direction = 1'b1;
			reset_psatd = 1'b0;
			enable_psatd = 1'b0;
			reset_satd = 1'b0;
			enable_satd = 1'b0;
			reset_transpose_buffer = 1'b0;
			done = 1'b0;
		end
		PRE_CALCULATE_1:begin
			enable_inputs = 1'b1;
			enable_transpose_buffer = 1'b1;
			change_transpose_buffer_direction = 1'b0;
			reset_psatd = 1'b0;
			enable_psatd = 1'b1;
			reset_satd = 1'b0;
			enable_satd = 1'b0;
			reset_transpose_buffer = 1'b0;
			done = 1'b0;
		end
		CALCULATE_0:begin
			enable_inputs = 1'b1;
			enable_transpose_buffer = 1'b1;
			change_transpose_buffer_direction = 1'b1;
			reset_psatd = 1'b1;
			enable_psatd = 1'b0;
			reset_satd = 1'b0;
			enable_satd = 1'b1;
			reset_transpose_buffer = 1'b0;
			done = 1'b0;
		end
		CALCULATE_1:begin
			enable_inputs = 1'b1;
			enable_transpose_buffer = 1'b1;
			change_transpose_buffer_direction = 1'b0;
			reset_psatd = 1'b0;
			enable_psatd = 1'b1;
			reset_satd = 1'b0;
			enable_satd = 1'b0;
			reset_transpose_buffer = 1'b0;
			done = 1'b1;
		end
		CALCULATE_2:begin
			enable_inputs = 1'b1;
			enable_transpose_buffer = 1'b1;
			change_transpose_buffer_direction = 1'b0;
			reset_psatd = 1'b0;
			enable_psatd = 1'b1;
			reset_satd = 1'b0;
			enable_satd = 1'b0;
			reset_transpose_buffer = 1'b0;
			done = 1'b0;
		end
		CALCULATE_3:begin
			enable_inputs = 1'b1;
			enable_transpose_buffer = 1'b1;
			change_transpose_buffer_direction = 1'b0;
			reset_psatd = 1'b0;
			enable_psatd = 1'b1;
			reset_satd = 1'b0;
			enable_satd = 1'b0;
			reset_transpose_buffer = 1'b0;
			done = 1'b0;
		end
		TERMINATE_0:begin
			enable_inputs = 1'b0;
			enable_transpose_buffer = 1'b1;
			change_transpose_buffer_direction = 1'b1;
			reset_psatd = 1'b1;
			enable_psatd = 1'b0;
			reset_satd = 1'b0;
			enable_satd = 1'b1;
			reset_transpose_buffer = 1'b0;
			done = 1'b0;
		end
		TERMINATE_1:begin
			enable_inputs = 1'b0;
			enable_transpose_buffer = 1'b1;
			change_transpose_buffer_direction = 1'b0;
			reset_psatd = 1'b0;
			enable_psatd = 1'b1;
			reset_satd = 1'b0;
			enable_satd = 1'b0;
			reset_transpose_buffer = 1'b0;
			done = 1'b1;
		end
		TERMINATE_2:begin
			enable_inputs = 1'b0;
			enable_transpose_buffer = 1'b1;
			change_transpose_buffer_direction = 1'b0;
			reset_psatd = 1'b0;
			enable_psatd = 1'b1;
			reset_satd = 1'b0;
			enable_satd = 1'b0;
			reset_transpose_buffer = 1'b0;
			done = 1'b0;
		end
		TERMINATE_3:begin
			enable_inputs = 1'b0;
			enable_transpose_buffer = 1'b1;
			change_transpose_buffer_direction = 1'b0;
			reset_psatd = 1'b0;
			enable_psatd = 1'b1;
			reset_satd = 1'b0;
			enable_satd = 1'b0;
			reset_transpose_buffer = 1'b0;
			done = 1'b0;
		end
		LAST_0:begin
			enable_inputs = 1'b0;
			enable_transpose_buffer = 1'b0;
			change_transpose_buffer_direction = 1'b0;
			reset_psatd = 1'b0;
			enable_psatd = 1'b0;
			reset_satd = 1'b0;
			enable_satd = 1'b1;
			reset_transpose_buffer = 1'b0;
			done = 1'b0;
		end
		LAST_1:begin
			enable_inputs = 1'b0;
			enable_transpose_buffer = 1'b0;
			change_transpose_buffer_direction = 1'b0;
			reset_psatd = 1'b0;
			enable_psatd = 1'b0;
			reset_satd = 1'b0;
			enable_satd = 1'b0;
			reset_transpose_buffer = 1'b0;
			done = 1'b1;
		end
		PRE_TERMINATE_0:begin
			enable_inputs = 1'b0;
			enable_transpose_buffer = 1'b1;
			change_transpose_buffer_direction = 1'b1;
			reset_psatd = 1'b0;
			enable_psatd = 1'b0;
			reset_satd = 1'b0;
			enable_satd = 1'b0;
			reset_transpose_buffer = 1'b0;
			done = 1'b0;
		end
		PRE_TERMINATE_1:begin
			enable_inputs = 1'b0;
			enable_transpose_buffer = 1'b1;
			change_transpose_buffer_direction = 1'b0;
			reset_psatd = 1'b0;
			enable_psatd = 1'b1;
			reset_satd = 1'b0;
			enable_satd = 1'b0;
			reset_transpose_buffer = 1'b0;
			done = 1'b0;
		end
		default:begin
			enable_inputs = 1'b0;
			enable_transpose_buffer = 1'b0;
			change_transpose_buffer_direction = 1'b0;
			reset_psatd = 1'b1;
			enable_psatd = 1'b0;
			reset_satd = 1'b1;
			enable_satd = 1'b0;
			reset_transpose_buffer = 1'b1;
			done = 1'b0;
		end
	endcase
end

always @ (posedge clock or posedge reset) begin
	if (reset)
		state <= IDLE;
	else
		case (state)
			IDLE: if(enable)
				state<= INITIALIZE_0;
			INITIALIZE_0: state<=INITIALIZE_1;
			INITIALIZE_1: state<=INITIALIZE_2;
			INITIALIZE_2: state<=INITIALIZE_3;
			INITIALIZE_3: if(enable)
				state<=PRE_CALCULATE_0;
			else
				state<=PRE_TERMINATE_0;
			CALCULATE_0: state<=CALCULATE_1;
			CALCULATE_1: state<=CALCULATE_2;
			CALCULATE_2: state<=CALCULATE_3;
			CALCULATE_3: if(enable)
				state<=CALCULATE_0;
			else
				state<=TERMINATE_0;
			TERMINATE_0: state<= TERMINATE_1;
			TERMINATE_1: state<= TERMINATE_2;
			TERMINATE_2: state<= TERMINATE_3;
			TERMINATE_3: state<=LAST_0;
			PRE_CALCULATE_0: state<= PRE_CALCULATE_1;
			PRE_CALCULATE_1: state<= CALCULATE_2;
			LAST_0: state<= LAST_1;
			LAST_1: state<= IDLE;
			PRE_TERMINATE_0: state<=PRE_TERMINATE_1;
			PRE_TERMINATE_1: state<=TERMINATE_2;
		endcase
end

endmodule

