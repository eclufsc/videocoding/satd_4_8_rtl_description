//-------------------------------------------------------------------------
// Design Name : abs_layer_4inputs
// File Name   : abs_layer_4inputs.v
// Function    : Performs the combinational absolute value extraction over an array of 4 signed values
// Coder       : Ismael Seidel
//-------------------------------------------------------------------------
module abs_layer_4inputs (
	in_0,
	in_1,
	in_2,
	in_3,
	out_0,
	out_1,
	out_2,
	out_3
);

parameter DATA_WIDTH = 8;


//------------------------------- Input Ports -----------------------------------
input signed [DATA_WIDTH-1:0] in_0, in_1, in_2, in_3;

//------------------------------- Output Ports -----------------------------------
output [DATA_WIDTH-2:0] out_0, out_1, out_2, out_3;

//--------------------------------- Code Starts Here -----------------------------
absolute #(DATA_WIDTH) abs_0(in_0, out_0);
absolute #(DATA_WIDTH) abs_1(in_1, out_1);
absolute #(DATA_WIDTH) abs_2(in_2, out_2);
absolute #(DATA_WIDTH) abs_3(in_3, out_3);

endmodule

