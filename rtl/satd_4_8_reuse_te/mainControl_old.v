//-------------------------------------------------------------------------
// Design Name : mainControl
// File Name   : mainControl.v
// Function    : SATD of 4x4 blocks with 4 pixel pairs as input
// Coder       : Ismael Seidel and Marcio Monteiro
//-------------------------------------------------------------------------
module mainControl(
	clock,
	reset,
	enable,
	enable_inputs4x4, // controls the inputs of 4x4
	reset_transpose_buffer4x4,
	enable_transpose_buffer4x4,
	change_transpose_buffer_direction4x4,
	reset_psatd4x4,
	enable_psatd4x4,
	reset_satd4x4,
	enable_satd4x4,
	done4x4,
	done4x4B,
	reset_transpose_buffer8x8,
	enable_transpose_buffer8x8,
	change_transpose_buffer_direction8x8,
	reset_psatd8x8,
	enable_psatd8x8,
	reset_satd8x8,
	enable_satd8x8,
	done8x8
);

parameter DATA_WIDTH = 8;

//------------------------------- Input Ports -----------------------------------
input clock, reset, enable;

//------------------------------- Output Ports -----------------------------------
output reg enable_inputs4x4, reset_transpose_buffer4x4, enable_transpose_buffer4x4, change_transpose_buffer_direction4x4, reset_psatd4x4, enable_psatd4x4, reset_satd4x4, enable_satd4x4, done4x4, done4x4B, reset_transpose_buffer8x8, enable_transpose_buffer8x8, change_transpose_buffer_direction8x8, reset_psatd8x8, enable_psatd8x8, reset_satd8x8, enable_satd8x8, done8x8;

reg [5:0] state;
parameter
IDLE = 0,
INITIALIZE_0 = 1,
INITIALIZE_1 = 2,
INITIALIZE_2 = 3,
INITIALIZE_3 = 4,
INITIALIZE_4 = 5,
INITIALIZE_5 = 6,
INITIALIZE_6 = 7,
INITIALIZE_7 = 8,

INITIALIZE_8 = 9,
INITIALIZE_9 = 10,
INITIALIZE_10 = 11,
INITIALIZE_11 = 12,

INITIALIZE_12 = 13,
INITIALIZE_13 = 14,
INITIALIZE_14 = 15,
INITIALIZE_15 = 16,
INITIALIZE_16 = 17,
INITIALIZE_17 = 18,
INITIALIZE_18 = 19,
INITIALIZE_19 = 20,

PRE_CALCULATE_0 = 21,
PRE_CALCULATE_1 = 22,
CALCULATE_0 = 23,
CALCULATE_1 = 24,
CALCULATE_2 = 25,
CALCULATE_3 = 26,
CALCULATE_4 = 27,
CALCULATE_5 = 28,
CALCULATE_6 = 29,
CALCULATE_7 = 30,
TERMINATE_0 = 31,
TERMINATE_1 = 32,
TERMINATE_2 = 33,
TERMINATE_3 = 34,
TERMINATE_4 = 35,
TERMINATE_5 = 36,
TERMINATE_6 = 37,
TERMINATE_7 = 38,
LAST_0 = 39,
LAST_1 = 40,
PRE_TERMINATE_0 = 41,
PRE_TERMINATE_1 = 42;


  // This code allows you to see state names in simulation
 `ifndef SYNTHESIS
  reg [127:0] statename;
  always @* begin
    case (state)
    	IDLE:
    		statename = "IDLE";
		INITIALIZE_0:
			statename = "IN_0";
		INITIALIZE_1:
			statename = "IN_1";
		INITIALIZE_2:
			statename = "IN_2";
		INITIALIZE_3:
			statename = "IN_3";
		INITIALIZE_4:
			statename = "IN_4";
		INITIALIZE_5:
			statename = "IN_5";
		INITIALIZE_6:
			statename = "IN_6";
		INITIALIZE_7:
			statename = "IN_7";

		INITIALIZE_8:
			statename = "IN_8";
		INITIALIZE_9 :
			statename = "IN_9";
		INITIALIZE_10 :
			statename = "IN_10";
		INITIALIZE_11 :
			statename = "IN_11";

		INITIALIZE_12 :
			statename = "IN_12";
		INITIALIZE_13 :
			statename = "IN_13";
		INITIALIZE_14 :
			statename = "IN_14";
		INITIALIZE_15 :
			statename = "IN_15";
		INITIALIZE_16 :
			statename = "IN_16";
		INITIALIZE_17 :
			statename = "IN_17";
		INITIALIZE_18 :
			statename = "IN_18";
		INITIALIZE_19 :
			statename = "IN_19";

		PRE_CALCULATE_0 :
			statename = "PC_0";
		PRE_CALCULATE_1 :
			statename = "PC_1";
		CALCULATE_0 :
			statename = "C_0";
		CALCULATE_1 :
			statename = "C_1";
		CALCULATE_2 :
			statename = "C_2";
		CALCULATE_3 :
			statename = "C_3";
		CALCULATE_4 :
			statename = "C_4";
		CALCULATE_5 :
			statename = "C_5";
		CALCULATE_6 :
			statename = "C_6";
		CALCULATE_7 :
			statename = "C_7";
		TERMINATE_0 :
			statename = "T_0";
		TERMINATE_1 :
			statename = "T_1";
		TERMINATE_2 :
			statename = "T_2";
		TERMINATE_3 :
			statename = "T_3";
		TERMINATE_4 :
			statename = "T_4";
		TERMINATE_5 :
			statename = "T_5";
		TERMINATE_6 :
			statename = "T_6";
		TERMINATE_7 :
			statename = "T_7";
		LAST_0 :
			statename = "L_0";
		LAST_1 :
			statename = "L_1";
		PRE_TERMINATE_0 :
			statename = "PT_0";
		PRE_TERMINATE_1 :
			statename = "PT_1";
      	default:
        	statename = "XXXX";
    endcase
  end
 `endif

always @ (state) begin
	case (state)
		IDLE:begin
			enable_inputs4x4 = 1'b0;
			enable_transpose_buffer4x4 = 1'b0;
			change_transpose_buffer_direction4x4 = 1'b0;
			reset_psatd4x4 = 1'b1;
			enable_psatd4x4 = 1'b0;
			reset_satd4x4 = 1'b1;
			enable_satd4x4 = 1'b0;
			reset_transpose_buffer4x4 = 1'b1;
			done4x4 = 1'b0;
			done4x4B = 1'b0;
			enable_transpose_buffer8x8 = 1'b0;
			change_transpose_buffer_direction8x8 = 1'b0;
			reset_psatd8x8 = 1'b1;
			enable_psatd8x8 = 1'b0;
			reset_satd8x8 = 1'b1;
			enable_satd8x8 = 1'b0;
			reset_transpose_buffer8x8 = 1'b1;
			done8x8 = 1'b0;
		end
		INITIALIZE_0:begin
			enable_inputs4x4 = 1'b1;
			enable_transpose_buffer4x4 = 1'b1;
			change_transpose_buffer_direction4x4 = 1'b0;
			reset_psatd4x4 = 1'b0;
			enable_psatd4x4 = 1'b0;
			reset_satd4x4 = 1'b0;
			enable_satd4x4 = 1'b0;
			reset_transpose_buffer4x4 = 1'b0;
			done4x4 = 1'b0;
			done4x4B = 1'b0;
			enable_transpose_buffer8x8 = 1'b0;
			change_transpose_buffer_direction8x8 = 1'b0;
			reset_psatd8x8 = 1'b0;
			enable_psatd8x8 = 1'b0;
			reset_satd8x8 = 1'b0;
			enable_satd8x8 = 1'b0;
			reset_transpose_buffer8x8 = 1'b0;
			done8x8 = 1'b0;
		end
		INITIALIZE_1:begin
			enable_inputs4x4 = 1'b1;
			enable_transpose_buffer4x4 = 1'b1;
			change_transpose_buffer_direction4x4 = 1'b0;
			reset_psatd4x4 = 1'b0;
			enable_psatd4x4 = 1'b0;
			reset_satd4x4 = 1'b0;
			enable_satd4x4 = 1'b0;
			reset_transpose_buffer4x4 = 1'b0;
			done4x4 = 1'b0;
			done4x4B = 1'b0;
			enable_transpose_buffer8x8 = 1'b0;
			change_transpose_buffer_direction8x8 = 1'b0;
			reset_psatd8x8 = 1'b0;
			enable_psatd8x8 = 1'b0;
			reset_satd8x8 = 1'b0;
			enable_satd8x8 = 1'b0;
			reset_transpose_buffer8x8 = 1'b0;
			done8x8 = 1'b0;
		end
		INITIALIZE_2:begin
			enable_inputs4x4 = 1'b1;
			enable_transpose_buffer4x4 = 1'b1;
			change_transpose_buffer_direction4x4 = 1'b0;
			reset_psatd4x4 = 1'b0;
			enable_psatd4x4 = 1'b0;
			reset_satd4x4 = 1'b0;
			enable_satd4x4 = 1'b0;
			reset_transpose_buffer4x4 = 1'b0;
			done4x4 = 1'b0;
			done4x4B = 1'b0;
			enable_transpose_buffer8x8 = 1'b0;
			change_transpose_buffer_direction8x8 = 1'b0;
			reset_psatd8x8 = 1'b0;
			enable_psatd8x8 = 1'b0;
			reset_satd8x8 = 1'b0;
			enable_satd8x8 = 1'b0;
			reset_transpose_buffer8x8 = 1'b0;
			done8x8 = 1'b0;
		end
		INITIALIZE_3:begin
			enable_inputs4x4 = 1'b1;
			enable_transpose_buffer4x4 = 1'b1;
			change_transpose_buffer_direction4x4 = 1'b0;
			reset_psatd4x4 = 1'b0;
			enable_psatd4x4 = 1'b0;
			reset_satd4x4 = 1'b0;
			enable_satd4x4 = 1'b0;
			reset_transpose_buffer4x4 = 1'b0;
			done4x4 = 1'b0;
			done4x4B = 1'b0;
			enable_transpose_buffer8x8 = 1'b0;
			change_transpose_buffer_direction8x8 = 1'b0;
			reset_psatd8x8 = 1'b0;
			enable_psatd8x8 = 1'b0;
			reset_satd8x8 = 1'b0;
			enable_satd8x8 = 1'b0;
			reset_transpose_buffer8x8 = 1'b0;
			done8x8 = 1'b0;
		end
		INITIALIZE_4:begin
			enable_inputs4x4 = 1'b1;
			enable_transpose_buffer4x4 = 1'b1;
			change_transpose_buffer_direction4x4 = 1'b1;
			reset_psatd4x4 = 1'b0;
			enable_psatd4x4 = 1'b0;
			reset_satd4x4 = 1'b0;
			enable_satd4x4 = 1'b0;
			reset_transpose_buffer4x4 = 1'b0;
			done4x4 = 1'b0;
			done4x4B = 1'b0;
			enable_transpose_buffer8x8 = 1'b0;
			change_transpose_buffer_direction8x8 = 1'b0;
			reset_psatd8x8 = 1'b0;
			enable_psatd8x8 = 1'b0;
			reset_satd8x8 = 1'b0;
			enable_satd8x8 = 1'b0;
			reset_transpose_buffer8x8 = 1'b0;
			done8x8 = 1'b0;
		end
		INITIALIZE_5:begin
			enable_inputs4x4 = 1'b1;
			enable_transpose_buffer4x4 = 1'b1;
			change_transpose_buffer_direction4x4 = 1'b0;
			reset_psatd4x4 = 1'b0;
			enable_psatd4x4 = 1'b1;
			reset_satd4x4 = 1'b0;
			enable_satd4x4 = 1'b0;
			reset_transpose_buffer4x4 = 1'b0;
			done4x4 = 1'b0;
			done4x4B = 1'b0;
			enable_transpose_buffer8x8 = 1'b1;
			change_transpose_buffer_direction8x8 = 1'b0;
			reset_psatd8x8 = 1'b0;
			enable_psatd8x8 = 1'b0;
			reset_satd8x8 = 1'b0;
			enable_satd8x8 = 1'b0;
			reset_transpose_buffer8x8 = 1'b0;
			done8x8 = 1'b0;
		end
		INITIALIZE_6:begin
			enable_inputs4x4 = 1'b1;
			enable_transpose_buffer4x4 = 1'b1;
			change_transpose_buffer_direction4x4 = 1'b0;
			reset_psatd4x4 = 1'b0;
			enable_psatd4x4 = 1'b1;
			reset_satd4x4 = 1'b0;
			enable_satd4x4 = 1'b0;
			reset_transpose_buffer4x4 = 1'b0;
			done4x4 = 1'b0;
			done4x4B = 1'b0;
			enable_transpose_buffer8x8 = 1'b1;
			change_transpose_buffer_direction8x8 = 1'b0;
			reset_psatd8x8 = 1'b0;
			enable_psatd8x8 = 1'b0;
			reset_satd8x8 = 1'b0;
			enable_satd8x8 = 1'b0;
			reset_transpose_buffer8x8 = 1'b0;
			done8x8 = 1'b0;
		end
		INITIALIZE_7:begin
			enable_inputs4x4 = 1'b1;
			enable_transpose_buffer4x4 = 1'b1;
			change_transpose_buffer_direction4x4 = 1'b0;
			reset_psatd4x4 = 1'b0;
			enable_psatd4x4 = 1'b1;
			reset_satd4x4 = 1'b0;
			enable_satd4x4 = 1'b0;
			reset_transpose_buffer4x4 = 1'b0;
			done4x4 = 1'b0;
			done4x4B = 1'b0;
			enable_transpose_buffer8x8 = 1'b1;
			change_transpose_buffer_direction8x8 = 1'b0;
			reset_psatd8x8 = 1'b0;
			enable_psatd8x8 = 1'b0;
			reset_satd8x8 = 1'b0;
			enable_satd8x8 = 1'b0;
			reset_transpose_buffer8x8 = 1'b0;
			done8x8 = 1'b0;
		end
		INITIALIZE_8:begin
			enable_inputs4x4 = 1'b1;
			enable_transpose_buffer4x4 = 1'b1;
			change_transpose_buffer_direction4x4 = 1'b1;
			reset_psatd4x4 = 1'b1;
			enable_psatd4x4 = 1'b0;
			reset_satd4x4 = 1'b0;
			enable_satd4x4 = 1'b1;
			reset_transpose_buffer4x4 = 1'b0;
			done4x4 = 1'b0;
			done4x4B = 1'b0;
			enable_transpose_buffer8x8 = 1'b1;
			change_transpose_buffer_direction8x8 = 1'b0;
			reset_psatd8x8 = 1'b1;
			enable_psatd8x8 = 1'b0;
			reset_satd8x8 = 1'b0;
			enable_satd8x8 = 1'b0;
			reset_transpose_buffer8x8 = 1'b0;
			done8x8 = 1'b0;
		end
		INITIALIZE_9:begin
			enable_inputs4x4 = 1'b1;
			enable_transpose_buffer4x4 = 1'b1;
			change_transpose_buffer_direction4x4 = 1'b0;
			reset_psatd4x4 = 1'b0;
			enable_psatd4x4 = 1'b1;
			reset_satd4x4 = 1'b0;
			enable_satd4x4 = 1'b0;
			reset_transpose_buffer4x4 = 1'b0;
			done4x4 = 1'b1;
			done4x4B = 1'b1;
			enable_transpose_buffer8x8 = 1'b1;
			change_transpose_buffer_direction8x8 = 1'b0;
			reset_psatd8x8 = 1'b0;
			enable_psatd8x8 = 1'b1;
			reset_satd8x8 = 1'b0;
			enable_satd8x8 = 1'b0;
			reset_transpose_buffer8x8 = 1'b0;
			done8x8 = 1'b0;
		end
		INITIALIZE_10:begin
			enable_inputs4x4 = 1'b1;
			enable_transpose_buffer4x4 = 1'b1;
			change_transpose_buffer_direction4x4 = 1'b0;
			reset_psatd4x4 = 1'b0;
			enable_psatd4x4 = 1'b1;
			reset_satd4x4 = 1'b0;
			enable_satd4x4 = 1'b0;
			reset_transpose_buffer4x4 = 1'b0;
			done4x4 = 1'b0;
			done4x4B = 1'b0;
			enable_transpose_buffer8x8 = 1'b1;
			change_transpose_buffer_direction8x8 = 1'b0;
			reset_psatd8x8 = 1'b0;
			enable_psatd8x8 = 1'b1;
			reset_satd8x8 = 1'b0;
			enable_satd8x8 = 1'b0;
			reset_transpose_buffer8x8 = 1'b0;
			done8x8 = 1'b0;
		end
		INITIALIZE_11:begin
			enable_inputs4x4 = 1'b1;
			enable_transpose_buffer4x4 = 1'b1;
			change_transpose_buffer_direction4x4 = 1'b0;
			reset_psatd4x4 = 1'b0;
			enable_psatd4x4 = 1'b1;
			reset_satd4x4 = 1'b0;
			enable_satd4x4 = 1'b0;
			reset_transpose_buffer4x4 = 1'b0;
			done4x4 = 1'b0;
			done4x4B = 1'b0;
			enable_transpose_buffer8x8 = 1'b1;
			change_transpose_buffer_direction8x8 = 1'b0;
			reset_psatd8x8 = 1'b0;
			enable_psatd8x8 = 1'b1;
			reset_satd8x8 = 1'b0;
			enable_satd8x8 = 1'b0;
			reset_transpose_buffer8x8 = 1'b0;
			done8x8 = 1'b0;
		end
		INITIALIZE_12:begin
			enable_inputs4x4 = 1'b1;
			enable_transpose_buffer4x4 = 1'b1;
			change_transpose_buffer_direction4x4 = 1'b1;
			reset_psatd4x4 = 1'b1;
			enable_psatd4x4 = 1'b0;
			reset_satd4x4 = 1'b0;
			enable_satd4x4 = 1'b1;
			reset_transpose_buffer4x4 = 1'b0;
			done4x4 = 1'b0;
			done4x4B = 1'b0;
			enable_transpose_buffer8x8 = 1'b1;
			change_transpose_buffer_direction8x8 = 1'b1;
			reset_psatd8x8 = 1'b0;
			enable_psatd8x8 = 1'b1;
			reset_satd8x8 = 1'b0;
			enable_satd8x8 = 1'b0;
			reset_transpose_buffer8x8 = 1'b0;
			done8x8 = 1'b0;
		end
		INITIALIZE_13:begin
			enable_inputs4x4 = 1'b1;
			enable_transpose_buffer4x4 = 1'b1;
			change_transpose_buffer_direction4x4 = 1'b0;
			reset_psatd4x4 = 1'b0;
			enable_psatd4x4 = 1'b1;
			reset_satd4x4 = 1'b0;
			enable_satd4x4 = 1'b0;
			reset_transpose_buffer4x4 = 1'b0;
			done4x4 = 1'b1;
			done4x4B = 1'b1;
			enable_transpose_buffer8x8 = 1'b1;
			change_transpose_buffer_direction8x8 = 1'b0;
			reset_psatd8x8 = 1'b0;
			enable_psatd8x8 = 1'b1;
			reset_satd8x8 = 1'b0;
			enable_satd8x8 = 1'b0;
			reset_transpose_buffer8x8 = 1'b0;
			done8x8 = 1'b0;
		end
		INITIALIZE_14:begin
			enable_inputs4x4 = 1'b1;
			enable_transpose_buffer4x4 = 1'b1;
			change_transpose_buffer_direction4x4 = 1'b0;
			reset_psatd4x4 = 1'b0;
			enable_psatd4x4 = 1'b1;
			reset_satd4x4 = 1'b0;
			enable_satd4x4 = 1'b0;
			reset_transpose_buffer4x4 = 1'b0;
			done4x4 = 1'b0;
			done4x4B = 1'b0;
			enable_transpose_buffer8x8 = 1'b1;
			change_transpose_buffer_direction8x8 = 1'b0;
			reset_psatd8x8 = 1'b0;
			enable_psatd8x8 = 1'b1;
			reset_satd8x8 = 1'b0;
			enable_satd8x8 = 1'b0;
			reset_transpose_buffer8x8 = 1'b0;
			done8x8 = 1'b0;
		end
		INITIALIZE_15:begin
			enable_inputs4x4 = 1'b1;
			enable_transpose_buffer4x4 = 1'b1;
			change_transpose_buffer_direction4x4 = 1'b0;
			reset_psatd4x4 = 1'b0;
			enable_psatd4x4 = 1'b1;
			reset_satd4x4 = 1'b0;
			enable_satd4x4 = 1'b0;
			reset_transpose_buffer4x4 = 1'b0;
			done4x4 = 1'b0;
			done4x4B = 1'b0;
			enable_transpose_buffer8x8 = 1'b1;
			change_transpose_buffer_direction8x8 = 1'b0;
			reset_psatd8x8 = 1'b0;
			enable_psatd8x8 = 1'b1;
			reset_satd8x8 = 1'b0;
			enable_satd8x8 = 1'b0;
			reset_transpose_buffer8x8 = 1'b0;
			done8x8 = 1'b0;
		end
		INITIALIZE_16:begin
			enable_inputs4x4 = 1'b1;
			enable_transpose_buffer4x4 = 1'b1;
			change_transpose_buffer_direction4x4 = 1'b1;
			reset_psatd4x4 = 1'b1;
			enable_psatd4x4 = 1'b0;
			reset_satd4x4 = 1'b0;
			enable_satd4x4 = 1'b1;
			reset_transpose_buffer4x4 = 1'b0;
			done4x4 = 1'b0;
			done4x4B = 1'b0;
			enable_transpose_buffer8x8 = 1'b1;
			change_transpose_buffer_direction8x8 = 1'b0;
			reset_psatd8x8 = 1'b0;
			enable_psatd8x8 = 1'b1;
			reset_satd8x8 = 1'b0;
			enable_satd8x8 = 1'b0;
			reset_transpose_buffer8x8 = 1'b0;
			done8x8 = 1'b0;
		end
		INITIALIZE_17:begin
			enable_inputs4x4 = 1'b1;
			enable_transpose_buffer4x4 = 1'b1;
			change_transpose_buffer_direction4x4 = 1'b0;
			reset_psatd4x4 = 1'b0;
			enable_psatd4x4 = 1'b1;
			reset_satd4x4 = 1'b0;
			enable_satd4x4 = 1'b0;
			reset_transpose_buffer4x4 = 1'b0;
			done4x4 = 1'b1;
			done4x4B = 1'b1;
			enable_transpose_buffer8x8 = 1'b1;
			change_transpose_buffer_direction8x8 = 1'b0;
			reset_psatd8x8 = 1'b0;
			enable_psatd8x8 = 1'b1;
			reset_satd8x8 = 1'b0;
			enable_satd8x8 = 1'b0;
			reset_transpose_buffer8x8 = 1'b0;
			done8x8 = 1'b0;
		end
		INITIALIZE_18:begin
			enable_inputs4x4 = 1'b1;
			enable_transpose_buffer4x4 = 1'b1;
			change_transpose_buffer_direction4x4 = 1'b0;
			reset_psatd4x4 = 1'b0;
			enable_psatd4x4 = 1'b1;
			reset_satd4x4 = 1'b0;
			enable_satd4x4 = 1'b0;
			reset_transpose_buffer4x4 = 1'b0;
			done4x4 = 1'b0;
			done4x4B = 1'b0;
			enable_transpose_buffer8x8 = 1'b1;
			change_transpose_buffer_direction8x8 = 1'b0;
			reset_psatd8x8 = 1'b0;
			enable_psatd8x8 = 1'b1;
			reset_satd8x8 = 1'b0;
			enable_satd8x8 = 1'b0;
			reset_transpose_buffer8x8 = 1'b0;
			done8x8 = 1'b0;
		end
		INITIALIZE_19:begin
			enable_inputs4x4 = 1'b1;
			enable_transpose_buffer4x4 = 1'b1;
			change_transpose_buffer_direction4x4 = 1'b0;
			reset_psatd4x4 = 1'b0;
			enable_psatd4x4 = 1'b1;
			reset_satd4x4 = 1'b0;
			enable_satd4x4 = 1'b0;
			reset_transpose_buffer4x4 = 1'b0;
			done4x4 = 1'b0;
			done4x4B = 1'b0;
			enable_transpose_buffer8x8 = 1'b1;
			change_transpose_buffer_direction8x8 = 1'b0;
			reset_psatd8x8 = 1'b0;
			enable_psatd8x8 = 1'b1;
			reset_satd8x8 = 1'b0;
			enable_satd8x8 = 1'b0;
			reset_transpose_buffer8x8 = 1'b0;
			done8x8 = 1'b0;
		end
		PRE_CALCULATE_0:begin
			enable_inputs4x4 = 1'b1;
			enable_transpose_buffer4x4 = 1'b1;
			change_transpose_buffer_direction4x4 = 1'b1;
			reset_psatd4x4 = 1'b1;
			enable_psatd4x4 = 1'b0;
			reset_satd4x4 = 1'b0;
			enable_satd4x4 = 1'b1;
			reset_transpose_buffer4x4 = 1'b0;
			done4x4 = 1'b0;
			done4x4B = 1'b0;
			enable_transpose_buffer8x8 = 1'b1;
			change_transpose_buffer_direction8x8 = 1'b1;
			reset_psatd8x8 = 1'b1;
			enable_psatd8x8 = 1'b0;
			reset_satd8x8 = 1'b0;
			enable_satd8x8 = 1'b1;
			reset_transpose_buffer8x8 = 1'b0;
			done8x8 = 1'b0;
		end
		PRE_CALCULATE_1:begin
			enable_inputs4x4 = 1'b1;
			enable_transpose_buffer4x4 = 1'b1;
			change_transpose_buffer_direction4x4 = 1'b0;
			reset_psatd4x4 = 1'b0;
			enable_psatd4x4 = 1'b1;
			reset_satd4x4 = 1'b0;
			enable_satd4x4 = 1'b0;
			reset_transpose_buffer4x4 = 1'b0;
			done4x4 = 1'b1;
			done4x4B = 1'b1;
			enable_transpose_buffer8x8 = 1'b1;
			change_transpose_buffer_direction8x8 = 1'b0;
			reset_psatd8x8 = 1'b0;
			enable_psatd8x8 = 1'b1;
			reset_satd8x8 = 1'b0;
			enable_satd8x8 = 1'b0;
			reset_transpose_buffer8x8 = 1'b0;
			done8x8 = 1'b1;
		end
		CALCULATE_0:begin
			enable_inputs4x4 = 1'b1;
			enable_transpose_buffer4x4 = 1'b1;
			change_transpose_buffer_direction4x4 = 1'b1;
			reset_psatd4x4 = 1'b1;
			enable_psatd4x4 = 1'b0;
			reset_satd4x4 = 1'b0;
			enable_satd4x4 = 1'b1;
			reset_transpose_buffer4x4 = 1'b0;
			done4x4 = 1'b0;
			done4x4B = 1'b0;
			enable_transpose_buffer8x8 = 1'b1;
			change_transpose_buffer_direction8x8 = 1'b1;
			reset_psatd8x8 = 1'b1;
			enable_psatd8x8 = 1'b0;
			reset_satd8x8 = 1'b0;
			enable_satd8x8 = 1'b1;
			reset_transpose_buffer8x8 = 1'b0;
			done8x8 = 1'b0;
		end
		CALCULATE_1:begin
			enable_inputs4x4 = 1'b1;
			enable_transpose_buffer4x4 = 1'b1;
			change_transpose_buffer_direction4x4 = 1'b0;
			reset_psatd4x4 = 1'b0;
			enable_psatd4x4 = 1'b1;
			reset_satd4x4 = 1'b0;
			enable_satd4x4 = 1'b0;
			reset_transpose_buffer4x4 = 1'b0;
			done4x4 = 1'b1;
			done4x4B = 1'b1;
			enable_transpose_buffer8x8 = 1'b1;
			change_transpose_buffer_direction8x8 = 1'b0;
			reset_psatd8x8 = 1'b0;
			enable_psatd8x8 = 1'b1;
			reset_satd8x8 = 1'b0;
			enable_satd8x8 = 1'b0;
			reset_transpose_buffer8x8 = 1'b0;
			done8x8 = 1'b1;
		end
		CALCULATE_2:begin
			enable_inputs4x4 = 1'b1;
			enable_transpose_buffer4x4 = 1'b1;
			change_transpose_buffer_direction4x4 = 1'b0;
			reset_psatd4x4 = 1'b0;
			enable_psatd4x4 = 1'b1;
			reset_satd4x4 = 1'b0;
			enable_satd4x4 = 1'b0;
			reset_transpose_buffer4x4 = 1'b0;
			done4x4 = 1'b0;
			done4x4B = 1'b0;
			enable_transpose_buffer8x8 = 1'b1;
			change_transpose_buffer_direction8x8 = 1'b0;
			reset_psatd8x8 = 1'b0;
			enable_psatd8x8 = 1'b1;
			reset_satd8x8 = 1'b0;
			enable_satd8x8 = 1'b0;
			reset_transpose_buffer8x8 = 1'b0;
			done8x8 = 1'b0;
		end
		CALCULATE_3:begin
			enable_inputs4x4 = 1'b1;
			enable_transpose_buffer4x4 = 1'b1;
			change_transpose_buffer_direction4x4 = 1'b0;
			reset_psatd4x4 = 1'b0;
			enable_psatd4x4 = 1'b1;
			reset_satd4x4 = 1'b0;
			enable_satd4x4 = 1'b0;
			reset_transpose_buffer4x4 = 1'b0;
			done4x4 = 1'b0;
			done4x4B = 1'b0;
			enable_transpose_buffer8x8 = 1'b1;
			change_transpose_buffer_direction8x8 = 1'b0;
			reset_psatd8x8 = 1'b0;
			enable_psatd8x8 = 1'b1;
			reset_satd8x8 = 1'b0;
			enable_satd8x8 = 1'b0;
			reset_transpose_buffer8x8 = 1'b0;
			done8x8 = 1'b0;
		end
		CALCULATE_4:begin
			enable_inputs4x4 = 1'b1;
			enable_transpose_buffer4x4 = 1'b1;
			change_transpose_buffer_direction4x4 = 1'b1;
			reset_psatd4x4 = 1'b1;
			enable_psatd4x4 = 1'b0;
			reset_satd4x4 = 1'b0;
			enable_satd4x4 = 1'b1;
			reset_transpose_buffer4x4 = 1'b0;
			done4x4 = 1'b0;
			done4x4B = 1'b0;
			enable_transpose_buffer8x8 = 1'b1;
			change_transpose_buffer_direction8x8 = 1'b0;
			reset_psatd8x8 = 1'b0;
			enable_psatd8x8 = 1'b1;
			reset_satd8x8 = 1'b0;
			enable_satd8x8 = 1'b0;
			reset_transpose_buffer8x8 = 1'b0;
			done8x8 = 1'b0;
		end
		CALCULATE_5:begin
			enable_inputs4x4 = 1'b1;
			enable_transpose_buffer4x4 = 1'b1;
			change_transpose_buffer_direction4x4 = 1'b0;
			reset_psatd4x4 = 1'b0;
			enable_psatd4x4 = 1'b1;
			reset_satd4x4 = 1'b0;
			enable_satd4x4 = 1'b0;
			reset_transpose_buffer4x4 = 1'b0;
			done4x4 = 1'b1;
			done4x4B = 1'b1;
			enable_transpose_buffer8x8 = 1'b1;
			change_transpose_buffer_direction8x8 = 1'b0;
			reset_psatd8x8 = 1'b0;
			enable_psatd8x8 = 1'b1;
			reset_satd8x8 = 1'b0;
			enable_satd8x8 = 1'b0;
			reset_transpose_buffer8x8 = 1'b0;
			done8x8 = 1'b0;
		end
		CALCULATE_6:begin
			enable_inputs4x4 = 1'b1;
			enable_transpose_buffer4x4 = 1'b1;
			change_transpose_buffer_direction4x4 = 1'b0;
			reset_psatd4x4 = 1'b0;
			enable_psatd4x4 = 1'b1;
			reset_satd4x4 = 1'b0;
			enable_satd4x4 = 1'b0;
			reset_transpose_buffer4x4 = 1'b0;
			done4x4 = 1'b0;
			done4x4B = 1'b0;
			enable_transpose_buffer8x8 = 1'b1;
			change_transpose_buffer_direction8x8 = 1'b0;
			reset_psatd8x8 = 1'b0;
			enable_psatd8x8 = 1'b1;
			reset_satd8x8 = 1'b0;
			enable_satd8x8 = 1'b0;
			reset_transpose_buffer8x8 = 1'b0;
			done8x8 = 1'b0;
		end
		CALCULATE_7:begin
			enable_inputs4x4 = 1'b1;
			enable_transpose_buffer4x4 = 1'b1;
			change_transpose_buffer_direction4x4 = 1'b0;
			reset_psatd4x4 = 1'b0;
			enable_psatd4x4 = 1'b1;
			reset_satd4x4 = 1'b0;
			enable_satd4x4 = 1'b0;
			reset_transpose_buffer4x4 = 1'b0;
			done4x4 = 1'b0;
			done4x4B = 1'b0;
			enable_transpose_buffer8x8 = 1'b1;
			change_transpose_buffer_direction8x8 = 1'b0;
			reset_psatd8x8 = 1'b0;
			enable_psatd8x8 = 1'b1;
			reset_satd8x8 = 1'b0;
			enable_satd8x8 = 1'b0;
			reset_transpose_buffer8x8 = 1'b0;
			done8x8 = 1'b0;
		end
		TERMINATE_0:begin
			enable_inputs4x4 = 1'b0;
			enable_transpose_buffer4x4 = 1'b1;
			change_transpose_buffer_direction4x4 = 1'b1;
			reset_psatd4x4 = 1'b1;
			enable_psatd4x4 = 1'b0;
			reset_satd4x4 = 1'b0;
			enable_satd4x4 = 1'b1;
			reset_transpose_buffer4x4 = 1'b0;
			done4x4 = 1'b0;
			done4x4B = 1'b0;
			enable_transpose_buffer8x8 = 1'b1;
			change_transpose_buffer_direction8x8 = 1'b1;
			reset_psatd8x8 = 1'b1;
			enable_psatd8x8 = 1'b0;
			reset_satd8x8 = 1'b0;
			enable_satd8x8 = 1'b1;
			reset_transpose_buffer8x8 = 1'b0;
			done8x8 = 1'b0;
		end
		TERMINATE_1:begin
			enable_inputs4x4 = 1'b0;
			enable_transpose_buffer4x4 = 1'b1;
			change_transpose_buffer_direction4x4 = 1'b0;
			reset_psatd4x4 = 1'b0;
			enable_psatd4x4 = 1'b1;
			reset_satd4x4 = 1'b0;
			enable_satd4x4 = 1'b0;
			reset_transpose_buffer4x4 = 1'b0;
			done4x4 = 1'b1;
			done4x4B = 1'b1;
			enable_transpose_buffer8x8 = 1'b1;
			change_transpose_buffer_direction8x8 = 1'b0;
			reset_psatd8x8 = 1'b0;
			enable_psatd8x8 = 1'b1;
			reset_satd8x8 = 1'b0;
			enable_satd8x8 = 1'b0;
			reset_transpose_buffer8x8 = 1'b0;
			done8x8 = 1'b1;
		end
		TERMINATE_2:begin
			enable_inputs4x4 = 1'b0;
			enable_transpose_buffer4x4 = 1'b1;
			change_transpose_buffer_direction4x4 = 1'b0;
			reset_psatd4x4 = 1'b0;
			enable_psatd4x4 = 1'b1;
			reset_satd4x4 = 1'b0;
			enable_satd4x4 = 1'b0;
			reset_transpose_buffer4x4 = 1'b0;
			done4x4 = 1'b0;
			done4x4B = 1'b0;
			enable_transpose_buffer8x8 = 1'b1;
			change_transpose_buffer_direction8x8 = 1'b0;
			reset_psatd8x8 = 1'b0;
			enable_psatd8x8 = 1'b1;
			reset_satd8x8 = 1'b0;
			enable_satd8x8 = 1'b0;
			reset_transpose_buffer8x8 = 1'b0;
			done8x8 = 1'b0;
		end
		TERMINATE_3:begin
			enable_inputs4x4 = 1'b0;
			enable_transpose_buffer4x4 = 1'b1;
			change_transpose_buffer_direction4x4 = 1'b0;
			reset_psatd4x4 = 1'b0;
			enable_psatd4x4 = 1'b1;
			reset_satd4x4 = 1'b0;
			enable_satd4x4 = 1'b0;
			reset_transpose_buffer4x4 = 1'b0;
			done4x4 = 1'b0;
			done4x4B = 1'b0;
			enable_transpose_buffer8x8 = 1'b1;
			change_transpose_buffer_direction8x8 = 1'b0;
			reset_psatd8x8 = 1'b0;
			enable_psatd8x8 = 1'b1;
			reset_satd8x8 = 1'b0;
			enable_satd8x8 = 1'b0;
			reset_transpose_buffer8x8 = 1'b0;
			done8x8 = 1'b0;
		end
		TERMINATE_4:begin
			enable_inputs4x4 = 1'b0;
			enable_transpose_buffer4x4 = 1'b1;
			change_transpose_buffer_direction4x4 = 1'b1;
			reset_psatd4x4 = 1'b0;
			enable_psatd4x4 = 1'b1;
			reset_satd4x4 = 1'b0;
			enable_satd4x4 = 1'b0;
			reset_transpose_buffer4x4 = 1'b0;
			done4x4 = 1'b0;
			done4x4B = 1'b0;
			enable_transpose_buffer8x8 = 1'b1;
			change_transpose_buffer_direction8x8 = 1'b0;
			reset_psatd8x8 = 1'b0;
			enable_psatd8x8 = 1'b1;
			reset_satd8x8 = 1'b0;
			enable_satd8x8 = 1'b0;
			reset_transpose_buffer8x8 = 1'b0;
			done8x8 = 1'b0;
		end
		TERMINATE_5:begin
			enable_inputs4x4 = 1'b0;
			enable_transpose_buffer4x4 = 1'b1;
			change_transpose_buffer_direction4x4 = 1'b0;
			reset_psatd4x4 = 1'b1;
			enable_psatd4x4 = 1'b0;
			reset_satd4x4 = 1'b0;
			enable_satd4x4 = 1'b0;
			reset_transpose_buffer4x4 = 1'b0;
			done4x4 = 1'b0;
			done4x4B = 1'b0;
			enable_transpose_buffer8x8 = 1'b1;
			change_transpose_buffer_direction8x8 = 1'b0;
			reset_psatd8x8 = 1'b0;
			enable_psatd8x8 = 1'b1;
			reset_satd8x8 = 1'b0;
			enable_satd8x8 = 1'b0;
			reset_transpose_buffer8x8 = 1'b0;
			done8x8 = 1'b0;
		end
		TERMINATE_6:begin
			enable_inputs4x4 = 1'b0;
			enable_transpose_buffer4x4 = 1'b1;
			change_transpose_buffer_direction4x4 = 1'b0;
			reset_psatd4x4 = 1'b0;
			enable_psatd4x4 = 1'b0;
			reset_satd4x4 = 1'b0;
			enable_satd4x4 = 1'b0;
			reset_transpose_buffer4x4 = 1'b0;
			done4x4 = 1'b0;
			done4x4B = 1'b0;
			enable_transpose_buffer8x8 = 1'b1;
			change_transpose_buffer_direction8x8 = 1'b0;
			reset_psatd8x8 = 1'b0;
			enable_psatd8x8 = 1'b1;
			reset_satd8x8 = 1'b0;
			enable_satd8x8 = 1'b0;
			reset_transpose_buffer8x8 = 1'b0;
			done8x8 = 1'b0;
		end
		TERMINATE_7:begin
			enable_inputs4x4 = 1'b0;
			enable_transpose_buffer4x4 = 1'b1;
			change_transpose_buffer_direction4x4 = 1'b0;
			reset_psatd4x4 = 1'b0;
			enable_psatd4x4 = 1'b0;
			reset_satd4x4 = 1'b0;
			enable_satd4x4 = 1'b0;
			reset_transpose_buffer4x4 = 1'b0;
			done4x4 = 1'b0;
			done4x4B = 1'b0;
			enable_transpose_buffer8x8 = 1'b1;
			change_transpose_buffer_direction8x8 = 1'b0;
			reset_psatd8x8 = 1'b0;
			enable_psatd8x8 = 1'b1;
			reset_satd8x8 = 1'b0;
			enable_satd8x8 = 1'b0;
			reset_transpose_buffer8x8 = 1'b0;
			done8x8 = 1'b0;
		end
		LAST_0:begin
			enable_inputs4x4 = 1'b0;
			enable_transpose_buffer4x4 = 1'b0;
			change_transpose_buffer_direction4x4 = 1'b0;
			reset_psatd4x4 = 1'b0;
			enable_psatd4x4 = 1'b0;
			reset_satd4x4 = 1'b0;
			enable_satd4x4 = 1'b0;
			reset_transpose_buffer4x4 = 1'b0;
			done4x4 = 1'b0;
			done4x4B = 1'b0;
			enable_transpose_buffer8x8 = 1'b1;
			change_transpose_buffer_direction8x8 = 1'b0;
			reset_psatd8x8 = 1'b1;
			enable_psatd8x8 = 1'b0;
			reset_satd8x8 = 1'b0;
			enable_satd8x8 = 1'b1;
			reset_transpose_buffer8x8 = 1'b0;
			done8x8 = 1'b0;
		end
		LAST_1:begin
			enable_inputs4x4 = 1'b0;
			enable_transpose_buffer4x4 = 1'b0;
			change_transpose_buffer_direction4x4 = 1'b0;
			reset_psatd4x4 = 1'b0;
			enable_psatd4x4 = 1'b0;
			reset_satd4x4 = 1'b0;
			enable_satd4x4 = 1'b0;
			reset_transpose_buffer4x4 = 1'b0;
			done4x4 = 1'b0;
			done4x4B = 1'b0;
			enable_transpose_buffer8x8 = 1'b0;
			change_transpose_buffer_direction8x8 = 1'b0;
			reset_psatd8x8 = 1'b0;
			enable_psatd8x8 = 1'b0;
			reset_satd8x8 = 1'b0;
			enable_satd8x8 = 1'b0;
			reset_transpose_buffer8x8 = 1'b0;
			done8x8 = 1'b1;
		end
		PRE_TERMINATE_0:begin
			enable_inputs4x4 = 1'b0;
			enable_transpose_buffer4x4 = 1'b0;
			change_transpose_buffer_direction4x4 = 1'b0;
			reset_psatd4x4 = 1'b0;
			enable_psatd4x4 = 1'b0;
			reset_satd4x4 = 1'b0;
			enable_satd4x4 = 1'b1;
			reset_transpose_buffer4x4 = 1'b0;
			done4x4 = 1'b0;
			done4x4B = 1'b0;
			enable_transpose_buffer8x8 = 1'b0;
			change_transpose_buffer_direction8x8 = 1'b0;
			reset_psatd8x8 = 1'b0;
			enable_psatd8x8 = 1'b0;
			reset_satd8x8 = 1'b0;
			enable_satd8x8 = 1'b1;
			reset_transpose_buffer8x8 = 1'b0;
			done8x8 = 1'b0;
		end
		PRE_TERMINATE_1:begin
			enable_inputs4x4 = 1'b0;
			enable_transpose_buffer4x4 = 1'b0;
			change_transpose_buffer_direction4x4 = 1'b0;
			reset_psatd4x4 = 1'b0;
			enable_psatd4x4 = 1'b0;
			reset_satd4x4 = 1'b0;
			enable_satd4x4 = 1'b0;
			reset_transpose_buffer4x4 = 1'b0;
			done4x4 = 1'b1;
			done4x4B = 1'b1;
			enable_transpose_buffer8x8 = 1'b0;
			change_transpose_buffer_direction8x8 = 1'b0;
			reset_psatd8x8 = 1'b0;
			enable_psatd8x8 = 1'b0;
			reset_satd8x8 = 1'b0;
			enable_satd8x8 = 1'b0;
			reset_transpose_buffer8x8 = 1'b0;
			done8x8 = 1'b1;
		end
		default:begin
			enable_inputs4x4 = 1'b0;
			enable_transpose_buffer4x4 = 1'b0;
			change_transpose_buffer_direction4x4 = 1'b0;
			reset_psatd4x4 = 1'b0;
			enable_psatd4x4 = 1'b0;
			reset_satd4x4 = 1'b0;
			enable_satd4x4 = 1'b0;
			reset_transpose_buffer4x4 = 1'b0;
			done4x4 = 1'b0;
			done4x4B = 1'b0;
			enable_transpose_buffer8x8 = 1'b0;
			change_transpose_buffer_direction8x8 = 1'b0;
			reset_psatd8x8 = 1'b0;
			enable_psatd8x8 = 1'b0;
			reset_satd8x8 = 1'b0;
			enable_satd8x8 = 1'b0;
			reset_transpose_buffer8x8 = 1'b0;
			done8x8 = 1'b0;
		end
	endcase
end

always @ (posedge clock or posedge reset) begin
	if (reset)
		state <= IDLE;
	else
		case (state)
			IDLE: if(enable)
				state<= INITIALIZE_0;
			INITIALIZE_0: state<=INITIALIZE_1;
			INITIALIZE_1: state<=INITIALIZE_2;
			INITIALIZE_2: state<=INITIALIZE_3;
			INITIALIZE_3: state<=INITIALIZE_4;
			INITIALIZE_4: state<=INITIALIZE_5;
			INITIALIZE_5: state<=INITIALIZE_6;
			INITIALIZE_6: state<=INITIALIZE_7;
			INITIALIZE_7: state<=INITIALIZE_8;
			INITIALIZE_8: state<=INITIALIZE_9;
			INITIALIZE_9: state<=INITIALIZE_10;
			INITIALIZE_10: state<=INITIALIZE_11;

			INITIALIZE_11: state<=INITIALIZE_12;
			INITIALIZE_12: state<=INITIALIZE_13;
			INITIALIZE_13: state<=INITIALIZE_14;
			INITIALIZE_14: state<=INITIALIZE_15;
			INITIALIZE_15: state<=INITIALIZE_16;
			INITIALIZE_16: state<=INITIALIZE_17;
			INITIALIZE_17: state<=INITIALIZE_18;
			INITIALIZE_18: state<=INITIALIZE_19;

			INITIALIZE_19: if(enable)
				state<=PRE_CALCULATE_0;
			else
				state<=PRE_TERMINATE_0;
			CALCULATE_0: state<=CALCULATE_1;
			CALCULATE_1: state<=CALCULATE_2;
			CALCULATE_2: state<=CALCULATE_3;
			CALCULATE_3: state<=CALCULATE_4;
			CALCULATE_4: state<=CALCULATE_5;
			CALCULATE_5: state<=CALCULATE_6;
			CALCULATE_6: state<=CALCULATE_7;
			CALCULATE_7: if(enable)
				state<=CALCULATE_0;
			else
				state<=TERMINATE_0;
			TERMINATE_0: state<= TERMINATE_1;
			TERMINATE_1: state<= TERMINATE_2;
			TERMINATE_2: state<= TERMINATE_3;
			TERMINATE_3: state<= TERMINATE_4;
			TERMINATE_4: state<= TERMINATE_5;
			TERMINATE_5: state<= TERMINATE_6;
			TERMINATE_6: state<= TERMINATE_7;
			TERMINATE_7: state<=LAST_0;
			PRE_CALCULATE_0: state<= PRE_CALCULATE_1;
			PRE_CALCULATE_1: state<= CALCULATE_2;
			LAST_0: state<= LAST_1;
			LAST_1: state<= IDLE;
			PRE_TERMINATE_0: state<=PRE_TERMINATE_1;
			PRE_TERMINATE_1: state<=TERMINATE_2;
		endcase
	end
endmodule

// //-------------------------------------------------------------------------
// // Design Name : mainControl
// // File Name   : mainControl.v
// // Function    : SATD of 4x4 blocks with 4 pixel pairs as input
// // Coder       : Ismael Seidel and Marcio Monteiro
// //-------------------------------------------------------------------------
// module mainControl(
// 	clock,
// 	reset,
// 	enable,
// 	enable_inputs4x4, // controls the inputs of 4x4
// 	reset_transpose_buffer4x4,
// 	enable_transpose_buffer4x4,
// 	change_transpose_buffer_direction4x4,
// 	reset_psatd4x4,
// 	enable_psatd4x4,
// 	reset_satd4x4,
// 	enable_satd4x4,
// 	done4x4,
// 	done4x4B,
// 	reset_transpose_buffer8x8,
// 	enable_transpose_buffer8x8,
// 	change_transpose_buffer_direction8x8,
// 	reset_psatd8x8,
// 	enable_psatd8x8,
// 	reset_satd8x8,
// 	enable_satd8x8,
// 	done8x8
// );

// parameter DATA_WIDTH = 8;

// //------------------------------- Input Ports -----------------------------------
// input clock, reset, enable;

// //------------------------------- Output Ports -----------------------------------
// output reg enable_inputs4x4, reset_transpose_buffer4x4, enable_transpose_buffer4x4, change_transpose_buffer_direction4x4, reset_psatd4x4, enable_psatd4x4, reset_satd4x4, enable_satd4x4, done4x4, done4x4B, reset_transpose_buffer8x8, enable_transpose_buffer8x8, change_transpose_buffer_direction8x8, reset_psatd8x8, enable_psatd8x8, reset_satd8x8, enable_satd8x8, done8x8;

// reg [5:0] state;
// parameter
// IDLE = 0,
// INITIALIZE_0 = 1,
// INITIALIZE_1 = 2,
// INITIALIZE_2 = 3,
// INITIALIZE_3 = 4,
// INITIALIZE_4 = 5,
// INITIALIZE_5 = 6,
// INITIALIZE_6 = 7,
// INITIALIZE_7 = 8,

// INITIALIZE_8 = 9,
// INITIALIZE_9 = 10,
// INITIALIZE_10 = 11,
// INITIALIZE_11 = 12,

// INITIALIZE_12 = 13,
// INITIALIZE_13 = 14,
// INITIALIZE_14 = 15,
// INITIALIZE_15 = 16,
// INITIALIZE_16 = 17,
// INITIALIZE_17 = 18,
// INITIALIZE_18 = 19,
// INITIALIZE_19 = 20,

// PRE_CALCULATE_0 = 21,
// PRE_CALCULATE_1 = 22,
// CALCULATE_0 = 23,
// CALCULATE_1 = 24,
// CALCULATE_2 = 25,
// CALCULATE_3 = 26,
// CALCULATE_4 = 27,
// CALCULATE_5 = 28,
// CALCULATE_6 = 29,
// CALCULATE_7 = 30,
// TERMINATE_0 = 31,
// TERMINATE_1 = 32,
// TERMINATE_2 = 33,
// TERMINATE_3 = 34,
// TERMINATE_4 = 35,
// TERMINATE_5 = 36,
// TERMINATE_6 = 37,
// TERMINATE_7 = 38,
// LAST_0 = 39,
// LAST_1 = 40,
// PRE_TERMINATE_0 = 41,
// PRE_TERMINATE_1 = 42;

// always @ (state) begin
// 	case (state)
// 		IDLE:begin
// 			enable_inputs4x4 = 1'b0;
// 			enable_transpose_buffer4x4 = 1'b0;
// 			change_transpose_buffer_direction4x4 = 1'b0;
// 			reset_psatd4x4 = 1'b1;
// 			enable_psatd4x4 = 1'b0;
// 			reset_satd4x4 = 1'b1;
// 			enable_satd4x4 = 1'b0;
// 			reset_transpose_buffer4x4 = 1'b1;
// 			done4x4 = 1'b0;
// 			done4x4B = 1'b0;
// 			enable_transpose_buffer8x8 = 1'b0;
// 			change_transpose_buffer_direction8x8 = 1'b0;
// 			reset_psatd8x8 = 1'b1;
// 			enable_psatd8x8 = 1'b0;
// 			reset_satd8x8 = 1'b1;
// 			enable_satd8x8 = 1'b0;
// 			reset_transpose_buffer8x8 = 1'b1;
// 			done8x8 = 1'b0;
// 		end
// 		INITIALIZE_0:begin
// 			enable_inputs4x4 = 1'b1;
// 			enable_transpose_buffer4x4 = 1'b1;
// 			change_transpose_buffer_direction4x4 = 1'b0;
// 			reset_psatd4x4 = 1'b0;
// 			enable_psatd4x4 = 1'b0;
// 			reset_satd4x4 = 1'b0;
// 			enable_satd4x4 = 1'b0;
// 			reset_transpose_buffer4x4 = 1'b0;
// 			done4x4 = 1'b0;
// 			done4x4B = 1'b0;
// 			enable_transpose_buffer8x8 = 1'b0;
// 			change_transpose_buffer_direction8x8 = 1'b0;
// 			reset_psatd8x8 = 1'b0;
// 			enable_psatd8x8 = 1'b0;
// 			reset_satd8x8 = 1'b0;
// 			enable_satd8x8 = 1'b0;
// 			reset_transpose_buffer8x8 = 1'b0;
// 			done8x8 = 1'b0;
// 		end
// 		INITIALIZE_1:begin
// 			enable_inputs4x4 = 1'b1;
// 			enable_transpose_buffer4x4 = 1'b1;
// 			change_transpose_buffer_direction4x4 = 1'b0;
// 			reset_psatd4x4 = 1'b0;
// 			enable_psatd4x4 = 1'b0;
// 			reset_satd4x4 = 1'b0;
// 			enable_satd4x4 = 1'b0;
// 			reset_transpose_buffer4x4 = 1'b0;
// 			done4x4 = 1'b0;
// 			done4x4B = 1'b0;
// 			enable_transpose_buffer8x8 = 1'b0;
// 			change_transpose_buffer_direction8x8 = 1'b0;
// 			reset_psatd8x8 = 1'b0;
// 			enable_psatd8x8 = 1'b0;
// 			reset_satd8x8 = 1'b0;
// 			enable_satd8x8 = 1'b0;
// 			reset_transpose_buffer8x8 = 1'b0;
// 			done8x8 = 1'b0;
// 		end
// 		INITIALIZE_2:begin
// 			enable_inputs4x4 = 1'b1;
// 			enable_transpose_buffer4x4 = 1'b1;
// 			change_transpose_buffer_direction4x4 = 1'b0;
// 			reset_psatd4x4 = 1'b0;
// 			enable_psatd4x4 = 1'b0;
// 			reset_satd4x4 = 1'b0;
// 			enable_satd4x4 = 1'b0;
// 			reset_transpose_buffer4x4 = 1'b0;
// 			done4x4 = 1'b0;
// 			done4x4B = 1'b0;
// 			enable_transpose_buffer8x8 = 1'b0;
// 			change_transpose_buffer_direction8x8 = 1'b0;
// 			reset_psatd8x8 = 1'b0;
// 			enable_psatd8x8 = 1'b0;
// 			reset_satd8x8 = 1'b0;
// 			enable_satd8x8 = 1'b0;
// 			reset_transpose_buffer8x8 = 1'b0;
// 			done8x8 = 1'b0;
// 		end
// 		INITIALIZE_3:begin
// 			enable_inputs4x4 = 1'b1;
// 			enable_transpose_buffer4x4 = 1'b1;
// 			change_transpose_buffer_direction4x4 = 1'b0;
// 			reset_psatd4x4 = 1'b0;
// 			enable_psatd4x4 = 1'b1;
// 			reset_satd4x4 = 1'b0;
// 			enable_satd4x4 = 1'b0;
// 			reset_transpose_buffer4x4 = 1'b0;
// 			done4x4 = 1'b0;
// 			done4x4B = 1'b0;
// 			enable_transpose_buffer8x8 = 1'b0;
// 			change_transpose_buffer_direction8x8 = 1'b0;
// 			reset_psatd8x8 = 1'b0;
// 			enable_psatd8x8 = 1'b0;
// 			reset_satd8x8 = 1'b0;
// 			enable_satd8x8 = 1'b0;
// 			reset_transpose_buffer8x8 = 1'b0;
// 			done8x8 = 1'b0;
// 		end
// 		INITIALIZE_4:begin
// 			enable_inputs4x4 = 1'b1;
// 			enable_transpose_buffer4x4 = 1'b1;
// 			change_transpose_buffer_direction4x4 = 1'b1;
// 			reset_psatd4x4 = 1'b0;
// 			enable_psatd4x4 = 1'b1;
// 			reset_satd4x4 = 1'b0;
// 			enable_satd4x4 = 1'b0;
// 			reset_transpose_buffer4x4 = 1'b0;
// 			done4x4 = 1'b0;
// 			done4x4B = 1'b0;
// 			enable_transpose_buffer8x8 = 1'b0;
// 			change_transpose_buffer_direction8x8 = 1'b0;
// 			reset_psatd8x8 = 1'b0;
// 			enable_psatd8x8 = 1'b0;
// 			reset_satd8x8 = 1'b0;
// 			enable_satd8x8 = 1'b0;
// 			reset_transpose_buffer8x8 = 1'b0;
// 			done8x8 = 1'b0;
// 		end
// 		INITIALIZE_5:begin
// 			enable_inputs4x4 = 1'b1;
// 			enable_transpose_buffer4x4 = 1'b1;
// 			change_transpose_buffer_direction4x4 = 1'b0;
// 			reset_psatd4x4 = 1'b0;
// 			enable_psatd4x4 = 1'b1;
// 			reset_satd4x4 = 1'b0;
// 			enable_satd4x4 = 1'b0;
// 			reset_transpose_buffer4x4 = 1'b0;
// 			done4x4 = 1'b0;
// 			done4x4B = 1'b0;
// 			enable_transpose_buffer8x8 = 1'b1;
// 			change_transpose_buffer_direction8x8 = 1'b0;
// 			reset_psatd8x8 = 1'b0;
// 			enable_psatd8x8 = 1'b0;
// 			reset_satd8x8 = 1'b0;
// 			enable_satd8x8 = 1'b0;
// 			reset_transpose_buffer8x8 = 1'b0;
// 			done8x8 = 1'b0;
// 		end
// 		INITIALIZE_6:begin
// 			enable_inputs4x4 = 1'b1;
// 			enable_transpose_buffer4x4 = 1'b1;
// 			change_transpose_buffer_direction4x4 = 1'b0;
// 			reset_psatd4x4 = 1'b0;
// 			enable_psatd4x4 = 1'b1;
// 			reset_satd4x4 = 1'b0;
// 			enable_satd4x4 = 1'b0;
// 			reset_transpose_buffer4x4 = 1'b0;
// 			done4x4 = 1'b0;
// 			done4x4B = 1'b0;
// 			enable_transpose_buffer8x8 = 1'b1;
// 			change_transpose_buffer_direction8x8 = 1'b0;
// 			reset_psatd8x8 = 1'b0;
// 			enable_psatd8x8 = 1'b0;
// 			reset_satd8x8 = 1'b0;
// 			enable_satd8x8 = 1'b0;
// 			reset_transpose_buffer8x8 = 1'b0;
// 			done8x8 = 1'b0;
// 		end
// 		INITIALIZE_7:begin
// 			enable_inputs4x4 = 1'b1;
// 			enable_transpose_buffer4x4 = 1'b1;
// 			change_transpose_buffer_direction4x4 = 1'b0;
// 			reset_psatd4x4 = 1'b0;
// 			enable_psatd4x4 = 1'b1;
// 			reset_satd4x4 = 1'b0;
// 			enable_satd4x4 = 1'b0;
// 			reset_transpose_buffer4x4 = 1'b0;
// 			done4x4 = 1'b0;
// 			done4x4B = 1'b0;
// 			enable_transpose_buffer8x8 = 1'b1;
// 			change_transpose_buffer_direction8x8 = 1'b0;
// 			reset_psatd8x8 = 1'b0;
// 			enable_psatd8x8 = 1'b0;
// 			reset_satd8x8 = 1'b0;
// 			enable_satd8x8 = 1'b0;
// 			reset_transpose_buffer8x8 = 1'b0;
// 			done8x8 = 1'b0;
// 		end
// 		INITIALIZE_8:begin
// 			enable_inputs4x4 = 1'b1;
// 			enable_transpose_buffer4x4 = 1'b1;
// 			change_transpose_buffer_direction4x4 = 1'b1;
// 			reset_psatd4x4 = 1'b1;
// 			enable_psatd4x4 = 1'b0;
// 			reset_satd4x4 = 1'b0;
// 			enable_satd4x4 = 1'b1;
// 			reset_transpose_buffer4x4 = 1'b0;
// 			done4x4 = 1'b0;
// 			done4x4B = 1'b0;
// 			enable_transpose_buffer8x8 = 1'b1;
// 			change_transpose_buffer_direction8x8 = 1'b0;
// 			reset_psatd8x8 = 1'b1;
// 			enable_psatd8x8 = 1'b0;
// 			reset_satd8x8 = 1'b0;
// 			enable_satd8x8 = 1'b0;
// 			reset_transpose_buffer8x8 = 1'b0;
// 			done8x8 = 1'b0;
// 		end
// 		INITIALIZE_9:begin
// 			enable_inputs4x4 = 1'b1;
// 			enable_transpose_buffer4x4 = 1'b1;
// 			change_transpose_buffer_direction4x4 = 1'b0;
// 			reset_psatd4x4 = 1'b0;
// 			enable_psatd4x4 = 1'b1;
// 			reset_satd4x4 = 1'b0;
// 			enable_satd4x4 = 1'b0;
// 			reset_transpose_buffer4x4 = 1'b0;
// 			done4x4 = 1'b1;
// 			done4x4B = 1'b1;
// 			enable_transpose_buffer8x8 = 1'b1;
// 			change_transpose_buffer_direction8x8 = 1'b0;
// 			reset_psatd8x8 = 1'b0;
// 			enable_psatd8x8 = 1'b1;
// 			reset_satd8x8 = 1'b0;
// 			enable_satd8x8 = 1'b0;
// 			reset_transpose_buffer8x8 = 1'b0;
// 			done8x8 = 1'b0;
// 		end
// 		INITIALIZE_10:begin
// 			enable_inputs4x4 = 1'b1;
// 			enable_transpose_buffer4x4 = 1'b1;
// 			change_transpose_buffer_direction4x4 = 1'b0;
// 			reset_psatd4x4 = 1'b0;
// 			enable_psatd4x4 = 1'b1;
// 			reset_satd4x4 = 1'b0;
// 			enable_satd4x4 = 1'b0;
// 			reset_transpose_buffer4x4 = 1'b0;
// 			done4x4 = 1'b0;
// 			done4x4B = 1'b0;
// 			enable_transpose_buffer8x8 = 1'b1;
// 			change_transpose_buffer_direction8x8 = 1'b0;
// 			reset_psatd8x8 = 1'b0;
// 			enable_psatd8x8 = 1'b1;
// 			reset_satd8x8 = 1'b0;
// 			enable_satd8x8 = 1'b0;
// 			reset_transpose_buffer8x8 = 1'b0;
// 			done8x8 = 1'b0;
// 		end
// 		INITIALIZE_11:begin
// 			enable_inputs4x4 = 1'b1;
// 			enable_transpose_buffer4x4 = 1'b1;
// 			change_transpose_buffer_direction4x4 = 1'b0;
// 			reset_psatd4x4 = 1'b0;
// 			enable_psatd4x4 = 1'b1;
// 			reset_satd4x4 = 1'b0;
// 			enable_satd4x4 = 1'b0;
// 			reset_transpose_buffer4x4 = 1'b0;
// 			done4x4 = 1'b0;
// 			done4x4B = 1'b0;
// 			enable_transpose_buffer8x8 = 1'b1;
// 			change_transpose_buffer_direction8x8 = 1'b0;
// 			reset_psatd8x8 = 1'b0;
// 			enable_psatd8x8 = 1'b1;
// 			reset_satd8x8 = 1'b0;
// 			enable_satd8x8 = 1'b0;
// 			reset_transpose_buffer8x8 = 1'b0;
// 			done8x8 = 1'b0;
// 		end
// 		INITIALIZE_12:begin
// 			enable_inputs4x4 = 1'b1;
// 			enable_transpose_buffer4x4 = 1'b1;
// 			change_transpose_buffer_direction4x4 = 1'b1;
// 			reset_psatd4x4 = 1'b1;
// 			enable_psatd4x4 = 1'b0;
// 			reset_satd4x4 = 1'b0;
// 			enable_satd4x4 = 1'b1;
// 			reset_transpose_buffer4x4 = 1'b0;
// 			done4x4 = 1'b0;
// 			done4x4B = 1'b0;
// 			enable_transpose_buffer8x8 = 1'b1;
// 			change_transpose_buffer_direction8x8 = 1'b1;
// 			reset_psatd8x8 = 1'b0;
// 			enable_psatd8x8 = 1'b1;
// 			reset_satd8x8 = 1'b0;
// 			enable_satd8x8 = 1'b0;
// 			reset_transpose_buffer8x8 = 1'b0;
// 			done8x8 = 1'b0;
// 		end
// 		INITIALIZE_13:begin
// 			enable_inputs4x4 = 1'b1;
// 			enable_transpose_buffer4x4 = 1'b1;
// 			change_transpose_buffer_direction4x4 = 1'b0;
// 			reset_psatd4x4 = 1'b0;
// 			enable_psatd4x4 = 1'b1;
// 			reset_satd4x4 = 1'b0;
// 			enable_satd4x4 = 1'b0;
// 			reset_transpose_buffer4x4 = 1'b0;
// 			done4x4 = 1'b1;
// 			done4x4B = 1'b1;
// 			enable_transpose_buffer8x8 = 1'b1;
// 			change_transpose_buffer_direction8x8 = 1'b0;
// 			reset_psatd8x8 = 1'b0;
// 			enable_psatd8x8 = 1'b1;
// 			reset_satd8x8 = 1'b0;
// 			enable_satd8x8 = 1'b0;
// 			reset_transpose_buffer8x8 = 1'b0;
// 			done8x8 = 1'b0;
// 		end
// 		INITIALIZE_14:begin
// 			enable_inputs4x4 = 1'b1;
// 			enable_transpose_buffer4x4 = 1'b1;
// 			change_transpose_buffer_direction4x4 = 1'b0;
// 			reset_psatd4x4 = 1'b0;
// 			enable_psatd4x4 = 1'b1;
// 			reset_satd4x4 = 1'b0;
// 			enable_satd4x4 = 1'b0;
// 			reset_transpose_buffer4x4 = 1'b0;
// 			done4x4 = 1'b0;
// 			done4x4B = 1'b0;
// 			enable_transpose_buffer8x8 = 1'b1;
// 			change_transpose_buffer_direction8x8 = 1'b0;
// 			reset_psatd8x8 = 1'b0;
// 			enable_psatd8x8 = 1'b1;
// 			reset_satd8x8 = 1'b0;
// 			enable_satd8x8 = 1'b0;
// 			reset_transpose_buffer8x8 = 1'b0;
// 			done8x8 = 1'b0;
// 		end
// 		INITIALIZE_15:begin
// 			enable_inputs4x4 = 1'b1;
// 			enable_transpose_buffer4x4 = 1'b1;
// 			change_transpose_buffer_direction4x4 = 1'b0;
// 			reset_psatd4x4 = 1'b0;
// 			enable_psatd4x4 = 1'b1;
// 			reset_satd4x4 = 1'b0;
// 			enable_satd4x4 = 1'b0;
// 			reset_transpose_buffer4x4 = 1'b0;
// 			done4x4 = 1'b0;
// 			done4x4B = 1'b0;
// 			enable_transpose_buffer8x8 = 1'b1;
// 			change_transpose_buffer_direction8x8 = 1'b0;
// 			reset_psatd8x8 = 1'b0;
// 			enable_psatd8x8 = 1'b1;
// 			reset_satd8x8 = 1'b0;
// 			enable_satd8x8 = 1'b0;
// 			reset_transpose_buffer8x8 = 1'b0;
// 			done8x8 = 1'b0;
// 		end
// 		INITIALIZE_16:begin
// 			enable_inputs4x4 = 1'b1;
// 			enable_transpose_buffer4x4 = 1'b1;
// 			change_transpose_buffer_direction4x4 = 1'b1;
// 			reset_psatd4x4 = 1'b1;
// 			enable_psatd4x4 = 1'b0;
// 			reset_satd4x4 = 1'b0;
// 			enable_satd4x4 = 1'b1;
// 			reset_transpose_buffer4x4 = 1'b0;
// 			done4x4 = 1'b0;
// 			done4x4B = 1'b0;
// 			enable_transpose_buffer8x8 = 1'b1;
// 			change_transpose_buffer_direction8x8 = 1'b0;
// 			reset_psatd8x8 = 1'b0;
// 			enable_psatd8x8 = 1'b1;
// 			reset_satd8x8 = 1'b0;
// 			enable_satd8x8 = 1'b0;
// 			reset_transpose_buffer8x8 = 1'b0;
// 			done8x8 = 1'b0;
// 		end
// 		INITIALIZE_17:begin
// 			enable_inputs4x4 = 1'b1;
// 			enable_transpose_buffer4x4 = 1'b1;
// 			change_transpose_buffer_direction4x4 = 1'b0;
// 			reset_psatd4x4 = 1'b0;
// 			enable_psatd4x4 = 1'b1;
// 			reset_satd4x4 = 1'b0;
// 			enable_satd4x4 = 1'b0;
// 			reset_transpose_buffer4x4 = 1'b0;
// 			done4x4 = 1'b1;
// 			done4x4B = 1'b1;
// 			enable_transpose_buffer8x8 = 1'b1;
// 			change_transpose_buffer_direction8x8 = 1'b0;
// 			reset_psatd8x8 = 1'b0;
// 			enable_psatd8x8 = 1'b1;
// 			reset_satd8x8 = 1'b0;
// 			enable_satd8x8 = 1'b0;
// 			reset_transpose_buffer8x8 = 1'b0;
// 			done8x8 = 1'b0;
// 		end
// 		INITIALIZE_18:begin
// 			enable_inputs4x4 = 1'b1;
// 			enable_transpose_buffer4x4 = 1'b1;
// 			change_transpose_buffer_direction4x4 = 1'b0;
// 			reset_psatd4x4 = 1'b0;
// 			enable_psatd4x4 = 1'b1;
// 			reset_satd4x4 = 1'b0;
// 			enable_satd4x4 = 1'b0;
// 			reset_transpose_buffer4x4 = 1'b0;
// 			done4x4 = 1'b0;
// 			done4x4B = 1'b0;
// 			enable_transpose_buffer8x8 = 1'b1;
// 			change_transpose_buffer_direction8x8 = 1'b0;
// 			reset_psatd8x8 = 1'b0;
// 			enable_psatd8x8 = 1'b1;
// 			reset_satd8x8 = 1'b0;
// 			enable_satd8x8 = 1'b0;
// 			reset_transpose_buffer8x8 = 1'b0;
// 			done8x8 = 1'b0;
// 		end
// 		INITIALIZE_19:begin
// 			enable_inputs4x4 = 1'b1;
// 			enable_transpose_buffer4x4 = 1'b1;
// 			change_transpose_buffer_direction4x4 = 1'b0;
// 			reset_psatd4x4 = 1'b0;
// 			enable_psatd4x4 = 1'b1;
// 			reset_satd4x4 = 1'b0;
// 			enable_satd4x4 = 1'b0;
// 			reset_transpose_buffer4x4 = 1'b0;
// 			done4x4 = 1'b0;
// 			done4x4B = 1'b0;
// 			enable_transpose_buffer8x8 = 1'b1;
// 			change_transpose_buffer_direction8x8 = 1'b0;
// 			reset_psatd8x8 = 1'b0;
// 			enable_psatd8x8 = 1'b1;
// 			reset_satd8x8 = 1'b0;
// 			enable_satd8x8 = 1'b0;
// 			reset_transpose_buffer8x8 = 1'b0;
// 			done8x8 = 1'b0;
// 		end
// 		PRE_CALCULATE_0:begin
// 			enable_inputs4x4 = 1'b1;
// 			enable_transpose_buffer4x4 = 1'b1;
// 			change_transpose_buffer_direction4x4 = 1'b1;
// 			reset_psatd4x4 = 1'b1;
// 			enable_psatd4x4 = 1'b0;
// 			reset_satd4x4 = 1'b0;
// 			enable_satd4x4 = 1'b1;
// 			reset_transpose_buffer4x4 = 1'b0;
// 			done4x4 = 1'b0;
// 			done4x4B = 1'b0;
// 			enable_transpose_buffer8x8 = 1'b1;
// 			change_transpose_buffer_direction8x8 = 1'b1;
// 			reset_psatd8x8 = 1'b1;
// 			enable_psatd8x8 = 1'b0;
// 			reset_satd8x8 = 1'b0;
// 			enable_satd8x8 = 1'b1;
// 			reset_transpose_buffer8x8 = 1'b0;
// 			done8x8 = 1'b0;
// 		end
// 		PRE_CALCULATE_1:begin
// 			enable_inputs4x4 = 1'b1;
// 			enable_transpose_buffer4x4 = 1'b1;
// 			change_transpose_buffer_direction4x4 = 1'b0;
// 			reset_psatd4x4 = 1'b0;
// 			enable_psatd4x4 = 1'b1;
// 			reset_satd4x4 = 1'b0;
// 			enable_satd4x4 = 1'b0;
// 			reset_transpose_buffer4x4 = 1'b0;
// 			done4x4 = 1'b1;
// 			done4x4B = 1'b1;
// 			enable_transpose_buffer8x8 = 1'b1;
// 			change_transpose_buffer_direction8x8 = 1'b0;
// 			reset_psatd8x8 = 1'b0;
// 			enable_psatd8x8 = 1'b1;
// 			reset_satd8x8 = 1'b0;
// 			enable_satd8x8 = 1'b0;
// 			reset_transpose_buffer8x8 = 1'b0;
// 			done8x8 = 1'b1;
// 		end
// 		CALCULATE_0:begin
// 			enable_inputs4x4 = 1'b1;
// 			enable_transpose_buffer4x4 = 1'b1;
// 			change_transpose_buffer_direction4x4 = 1'b1;
// 			reset_psatd4x4 = 1'b1;
// 			enable_psatd4x4 = 1'b0;
// 			reset_satd4x4 = 1'b0;
// 			enable_satd4x4 = 1'b1;
// 			reset_transpose_buffer4x4 = 1'b0;
// 			done4x4 = 1'b0;
// 			done4x4B = 1'b0;
// 			enable_transpose_buffer8x8 = 1'b1;
// 			change_transpose_buffer_direction8x8 = 1'b1;
// 			reset_psatd8x8 = 1'b1;
// 			enable_psatd8x8 = 1'b0;
// 			reset_satd8x8 = 1'b0;
// 			enable_satd8x8 = 1'b1;
// 			reset_transpose_buffer8x8 = 1'b0;
// 			done8x8 = 1'b0;
// 		end
// 		CALCULATE_1:begin
// 			enable_inputs4x4 = 1'b1;
// 			enable_transpose_buffer4x4 = 1'b1;
// 			change_transpose_buffer_direction4x4 = 1'b0;
// 			reset_psatd4x4 = 1'b0;
// 			enable_psatd4x4 = 1'b1;
// 			reset_satd4x4 = 1'b0;
// 			enable_satd4x4 = 1'b0;
// 			reset_transpose_buffer4x4 = 1'b0;
// 			done4x4 = 1'b1;
// 			done4x4B = 1'b1;
// 			enable_transpose_buffer8x8 = 1'b1;
// 			change_transpose_buffer_direction8x8 = 1'b0;
// 			reset_psatd8x8 = 1'b0;
// 			enable_psatd8x8 = 1'b1;
// 			reset_satd8x8 = 1'b0;
// 			enable_satd8x8 = 1'b0;
// 			reset_transpose_buffer8x8 = 1'b0;
// 			done8x8 = 1'b1;
// 		end
// 		CALCULATE_2:begin
// 			enable_inputs4x4 = 1'b1;
// 			enable_transpose_buffer4x4 = 1'b1;
// 			change_transpose_buffer_direction4x4 = 1'b0;
// 			reset_psatd4x4 = 1'b0;
// 			enable_psatd4x4 = 1'b1;
// 			reset_satd4x4 = 1'b0;
// 			enable_satd4x4 = 1'b0;
// 			reset_transpose_buffer4x4 = 1'b0;
// 			done4x4 = 1'b0;
// 			done4x4B = 1'b0;
// 			enable_transpose_buffer8x8 = 1'b1;
// 			change_transpose_buffer_direction8x8 = 1'b0;
// 			reset_psatd8x8 = 1'b0;
// 			enable_psatd8x8 = 1'b1;
// 			reset_satd8x8 = 1'b0;
// 			enable_satd8x8 = 1'b0;
// 			reset_transpose_buffer8x8 = 1'b0;
// 			done8x8 = 1'b0;
// 		end
// 		CALCULATE_3:begin
// 			enable_inputs4x4 = 1'b1;
// 			enable_transpose_buffer4x4 = 1'b1;
// 			change_transpose_buffer_direction4x4 = 1'b0;
// 			reset_psatd4x4 = 1'b0;
// 			enable_psatd4x4 = 1'b1;
// 			reset_satd4x4 = 1'b0;
// 			enable_satd4x4 = 1'b0;
// 			reset_transpose_buffer4x4 = 1'b0;
// 			done4x4 = 1'b0;
// 			done4x4B = 1'b0;
// 			enable_transpose_buffer8x8 = 1'b1;
// 			change_transpose_buffer_direction8x8 = 1'b0;
// 			reset_psatd8x8 = 1'b0;
// 			enable_psatd8x8 = 1'b1;
// 			reset_satd8x8 = 1'b0;
// 			enable_satd8x8 = 1'b0;
// 			reset_transpose_buffer8x8 = 1'b0;
// 			done8x8 = 1'b0;
// 		end
// 		CALCULATE_4:begin
// 			enable_inputs4x4 = 1'b1;
// 			enable_transpose_buffer4x4 = 1'b1;
// 			change_transpose_buffer_direction4x4 = 1'b1;
// 			reset_psatd4x4 = 1'b1;
// 			enable_psatd4x4 = 1'b0;
// 			reset_satd4x4 = 1'b0;
// 			enable_satd4x4 = 1'b1;
// 			reset_transpose_buffer4x4 = 1'b0;
// 			done4x4 = 1'b0;
// 			done4x4B = 1'b0;
// 			enable_transpose_buffer8x8 = 1'b1;
// 			change_transpose_buffer_direction8x8 = 1'b0;
// 			reset_psatd8x8 = 1'b0;
// 			enable_psatd8x8 = 1'b1;
// 			reset_satd8x8 = 1'b0;
// 			enable_satd8x8 = 1'b0;
// 			reset_transpose_buffer8x8 = 1'b0;
// 			done8x8 = 1'b0;
// 		end
// 		CALCULATE_5:begin
// 			enable_inputs4x4 = 1'b1;
// 			enable_transpose_buffer4x4 = 1'b1;
// 			change_transpose_buffer_direction4x4 = 1'b0;
// 			reset_psatd4x4 = 1'b0;
// 			enable_psatd4x4 = 1'b1;
// 			reset_satd4x4 = 1'b0;
// 			enable_satd4x4 = 1'b0;
// 			reset_transpose_buffer4x4 = 1'b0;
// 			done4x4 = 1'b1;
// 			done4x4B = 1'b1;
// 			enable_transpose_buffer8x8 = 1'b1;
// 			change_transpose_buffer_direction8x8 = 1'b0;
// 			reset_psatd8x8 = 1'b0;
// 			enable_psatd8x8 = 1'b1;
// 			reset_satd8x8 = 1'b0;
// 			enable_satd8x8 = 1'b0;
// 			reset_transpose_buffer8x8 = 1'b0;
// 			done8x8 = 1'b0;
// 		end
// 		CALCULATE_6:begin
// 			enable_inputs4x4 = 1'b1;
// 			enable_transpose_buffer4x4 = 1'b1;
// 			change_transpose_buffer_direction4x4 = 1'b0;
// 			reset_psatd4x4 = 1'b0;
// 			enable_psatd4x4 = 1'b1;
// 			reset_satd4x4 = 1'b0;
// 			enable_satd4x4 = 1'b0;
// 			reset_transpose_buffer4x4 = 1'b0;
// 			done4x4 = 1'b0;
// 			done4x4B = 1'b0;
// 			enable_transpose_buffer8x8 = 1'b1;
// 			change_transpose_buffer_direction8x8 = 1'b0;
// 			reset_psatd8x8 = 1'b0;
// 			enable_psatd8x8 = 1'b1;
// 			reset_satd8x8 = 1'b0;
// 			enable_satd8x8 = 1'b0;
// 			reset_transpose_buffer8x8 = 1'b0;
// 			done8x8 = 1'b0;
// 		end
// 		CALCULATE_7:begin
// 			enable_inputs4x4 = 1'b1;
// 			enable_transpose_buffer4x4 = 1'b1;
// 			change_transpose_buffer_direction4x4 = 1'b0;
// 			reset_psatd4x4 = 1'b0;
// 			enable_psatd4x4 = 1'b1;
// 			reset_satd4x4 = 1'b0;
// 			enable_satd4x4 = 1'b0;
// 			reset_transpose_buffer4x4 = 1'b0;
// 			done4x4 = 1'b0;
// 			done4x4B = 1'b0;
// 			enable_transpose_buffer8x8 = 1'b1;
// 			change_transpose_buffer_direction8x8 = 1'b0;
// 			reset_psatd8x8 = 1'b0;
// 			enable_psatd8x8 = 1'b1;
// 			reset_satd8x8 = 1'b0;
// 			enable_satd8x8 = 1'b0;
// 			reset_transpose_buffer8x8 = 1'b0;
// 			done8x8 = 1'b0;
// 		end
// 		TERMINATE_0:begin
// 			enable_inputs4x4 = 1'b0;
// 			enable_transpose_buffer4x4 = 1'b1;
// 			change_transpose_buffer_direction4x4 = 1'b0;
// 			reset_psatd4x4 = 1'b0;
// 			enable_psatd4x4 = 1'b0;
// 			reset_satd4x4 = 1'b0;
// 			enable_satd4x4 = 1'b1;
// 			reset_transpose_buffer4x4 = 1'b0;
// 			done4x4 = 1'b0;
// 			done4x4B = 1'b0;
// 			enable_transpose_buffer8x8 = 1'b0;
// 			change_transpose_buffer_direction8x8 = 1'b0;
// 			reset_psatd8x8 = 1'b0;
// 			enable_psatd8x8 = 1'b1;
// 			reset_satd8x8 = 1'b0;
// 			enable_satd8x8 = 1'b0;
// 			reset_transpose_buffer8x8 = 1'b0;
// 			done8x8 = 1'b0;
// 		end
// 		TERMINATE_1:begin
// 			enable_inputs4x4 = 1'b0;
// 			enable_transpose_buffer4x4 = 1'b0;
// 			change_transpose_buffer_direction4x4 = 1'b0;
// 			reset_psatd4x4 = 1'b0;
// 			enable_psatd4x4 = 1'b0;
// 			reset_satd4x4 = 1'b0;
// 			enable_satd4x4 = 1'b0;
// 			reset_transpose_buffer4x4 = 1'b1;
// 			done4x4 = 1'b1;
// 			done4x4B = 1'b1;
// 			enable_transpose_buffer8x8 = 1'b1;
// 			change_transpose_buffer_direction8x8 = 1'b0;
// 			reset_psatd8x8 = 1'b1;
// 			enable_psatd8x8 = 1'b0;
// 			reset_satd8x8 = 1'b0;
// 			enable_satd8x8 = 1'b1;
// 			reset_transpose_buffer8x8 = 1'b0;
// 			done8x8 = 1'b1;
// 		end
// 		TERMINATE_2:begin
// 			enable_inputs4x4 = 1'b0;
// 			enable_transpose_buffer4x4 = 1'b0;
// 			change_transpose_buffer_direction4x4 = 1'b0;
// 			reset_psatd4x4 = 1'b0;
// 			enable_psatd4x4 = 1'b0;
// 			reset_satd4x4 = 1'b0;
// 			enable_satd4x4 = 1'b0;
// 			reset_transpose_buffer4x4 = 1'b0;
// 			done4x4 = 1'b0;
// 			done4x4B = 1'b0;
// 			enable_transpose_buffer8x8 = 1'b1;
// 			change_transpose_buffer_direction8x8 = 1'b0;
// 			reset_psatd8x8 = 1'b0;
// 			enable_psatd8x8 = 1'b1;
// 			reset_satd8x8 = 1'b0;
// 			enable_satd8x8 = 1'b0;
// 			reset_transpose_buffer8x8 = 1'b0;
// 			done8x8 = 1'b0;
// 		end
// 		TERMINATE_3:begin
// 			enable_inputs4x4 = 1'b0;
// 			enable_transpose_buffer4x4 = 1'b0;
// 			change_transpose_buffer_direction4x4 = 1'b0;
// 			reset_psatd4x4 = 1'b0;
// 			enable_psatd4x4 = 1'b0;
// 			reset_satd4x4 = 1'b0;
// 			enable_satd4x4 = 1'b0;
// 			reset_transpose_buffer4x4 = 1'b0;
// 			done4x4 = 1'b0;
// 			done4x4B = 1'b0;
// 			enable_transpose_buffer8x8 = 1'b1;
// 			change_transpose_buffer_direction8x8 = 1'b0;
// 			reset_psatd8x8 = 1'b0;
// 			enable_psatd8x8 = 1'b0;
// 			reset_satd8x8 = 1'b1;
// 			enable_satd8x8 = 1'b0;
// 			reset_transpose_buffer8x8 = 1'b0;
// 			done8x8 = 1'b0;
// 		end
// 		TERMINATE_4:begin
// 			enable_inputs4x4 = 1'b0;
// 			enable_transpose_buffer4x4 = 1'b0;
// 			change_transpose_buffer_direction4x4 = 1'b0;
// 			reset_psatd4x4 = 1'b0;
// 			enable_psatd4x4 = 1'b0;
// 			reset_satd4x4 = 1'b0;
// 			enable_satd4x4 = 1'b0;
// 			reset_transpose_buffer4x4 = 1'b0;
// 			done4x4 = 1'b0;
// 			done4x4B = 1'b0;
// 			enable_transpose_buffer8x8 = 1'b1;
// 			change_transpose_buffer_direction8x8 = 1'b0;
// 			reset_psatd8x8 = 1'b0;
// 			enable_psatd8x8 = 1'b1;
// 			reset_satd8x8 = 1'b0;
// 			enable_satd8x8 = 1'b0;
// 			reset_transpose_buffer8x8 = 1'b0;
// 			done8x8 = 1'b0;
// 		end
// 		TERMINATE_5:begin
// 			enable_inputs4x4 = 1'b0;
// 			enable_transpose_buffer4x4 = 1'b0;
// 			change_transpose_buffer_direction4x4 = 1'b0;
// 			reset_psatd4x4 = 1'b0;
// 			enable_psatd4x4 = 1'b0;
// 			reset_satd4x4 = 1'b0;
// 			enable_satd4x4 = 1'b0;
// 			reset_transpose_buffer4x4 = 1'b0;
// 			done4x4 = 1'b0;
// 			done4x4B = 1'b0;
// 			enable_transpose_buffer8x8 = 1'b1;
// 			change_transpose_buffer_direction8x8 = 1'b0;
// 			reset_psatd8x8 = 1'b0;
// 			enable_psatd8x8 = 1'b1;
// 			reset_satd8x8 = 1'b0;
// 			enable_satd8x8 = 1'b0;
// 			reset_transpose_buffer8x8 = 1'b0;
// 			done8x8 = 1'b0;
// 		end
// 		TERMINATE_6:begin
// 			enable_inputs4x4 = 1'b0;
// 			enable_transpose_buffer4x4 = 1'b0;
// 			change_transpose_buffer_direction4x4 = 1'b0;
// 			reset_psatd4x4 = 1'b0;
// 			enable_psatd4x4 = 1'b0;
// 			reset_satd4x4 = 1'b0;
// 			enable_satd4x4 = 1'b0;
// 			reset_transpose_buffer4x4 = 1'b0;
// 			done4x4 = 1'b0;
// 			done4x4B = 1'b0;
// 			enable_transpose_buffer8x8 = 1'b1;
// 			change_transpose_buffer_direction8x8 = 1'b0;
// 			reset_psatd8x8 = 1'b0;
// 			enable_psatd8x8 = 1'b1;
// 			reset_satd8x8 = 1'b0;
// 			enable_satd8x8 = 1'b0;
// 			reset_transpose_buffer8x8 = 1'b0;
// 			done8x8 = 1'b0;
// 		end
// 		TERMINATE_7:begin
// 			enable_inputs4x4 = 1'b0;
// 			enable_transpose_buffer4x4 = 1'b0;
// 			change_transpose_buffer_direction4x4 = 1'b0;
// 			reset_psatd4x4 = 1'b0;
// 			enable_psatd4x4 = 1'b0;
// 			reset_satd4x4 = 1'b0;
// 			enable_satd4x4 = 1'b0;
// 			reset_transpose_buffer4x4 = 1'b0;
// 			done4x4 = 1'b0;
// 			done4x4B = 1'b0;
// 			enable_transpose_buffer8x8 = 1'b1;
// 			change_transpose_buffer_direction8x8 = 1'b1;
// 			reset_psatd8x8 = 1'b1;
// 			enable_psatd8x8 = 1'b0;
// 			reset_satd8x8 = 1'b0;
// 			enable_satd8x8 = 1'b0;
// 			reset_transpose_buffer8x8 = 1'b0;
// 			done8x8 = 1'b0;
// 		end
// 		LAST_0:begin
// 			enable_inputs4x4 = 1'b0;
// 			enable_transpose_buffer4x4 = 1'b0;
// 			change_transpose_buffer_direction4x4 = 1'b0;
// 			reset_psatd4x4 = 1'b0;
// 			enable_psatd4x4 = 1'b0;
// 			reset_satd4x4 = 1'b0;
// 			enable_satd4x4 = 1'b0;
// 			reset_transpose_buffer4x4 = 1'b0;
// 			done4x4 = 1'b0;
// 			done4x4B = 1'b0;
// 			enable_transpose_buffer8x8 = 1'b1;
// 			change_transpose_buffer_direction8x8 = 1'b0;
// 			reset_psatd8x8 = 1'b0;
// 			enable_psatd8x8 = 1'b1;
// 			reset_satd8x8 = 1'b0;
// 			enable_satd8x8 = 1'b0;
// 			reset_transpose_buffer8x8 = 1'b0;
// 			done8x8 = 1'b1;
// 		end
// 		LAST_1:begin
// 			enable_inputs4x4 = 1'b0;
// 			enable_transpose_buffer4x4 = 1'b0;
// 			change_transpose_buffer_direction4x4 = 1'b0;
// 			reset_psatd4x4 = 1'b0;
// 			enable_psatd4x4 = 1'b0;
// 			reset_satd4x4 = 1'b0;
// 			enable_satd4x4 = 1'b0;
// 			reset_transpose_buffer4x4 = 1'b0;
// 			done4x4 = 1'b0;
// 			done4x4B = 1'b0;
// 			enable_transpose_buffer8x8 = 1'b0;
// 			change_transpose_buffer_direction8x8 = 1'b0;
// 			reset_psatd8x8 = 1'b0;
// 			enable_psatd8x8 = 1'b0;
// 			reset_satd8x8 = 1'b0;
// 			enable_satd8x8 = 1'b1;
// 			reset_transpose_buffer8x8 = 1'b0;
// 			done8x8 = 1'b0;
// 		end
// 		PRE_TERMINATE_0:begin
// 			enable_inputs4x4 = 1'b0;
// 			enable_transpose_buffer4x4 = 1'b0;
// 			change_transpose_buffer_direction4x4 = 1'b0;
// 			reset_psatd4x4 = 1'b0;
// 			enable_psatd4x4 = 1'b0;
// 			reset_satd4x4 = 1'b0;
// 			enable_satd4x4 = 1'b0;
// 			reset_transpose_buffer4x4 = 1'b0;
// 			done4x4 = 1'b0;
// 			done4x4B = 1'b0;
// 			enable_transpose_buffer8x8 = 1'b0;
// 			change_transpose_buffer_direction8x8 = 1'b0;
// 			reset_psatd8x8 = 1'b0;
// 			enable_psatd8x8 = 1'b0;
// 			reset_satd8x8 = 1'b0;
// 			enable_satd8x8 = 1'b0;
// 			reset_transpose_buffer8x8 = 1'b0;
// 			done8x8 = 1'b0;
// 		end
// 		PRE_TERMINATE_1:begin
// 			enable_inputs4x4 = 1'b0;
// 			enable_transpose_buffer4x4 = 1'b0;
// 			change_transpose_buffer_direction4x4 = 1'b0;
// 			reset_psatd4x4 = 1'b0;
// 			enable_psatd4x4 = 1'b0;
// 			reset_satd4x4 = 1'b0;
// 			enable_satd4x4 = 1'b0;
// 			reset_transpose_buffer4x4 = 1'b0;
// 			done4x4 = 1'b0;
// 			done4x4B = 1'b0;
// 			enable_transpose_buffer8x8 = 1'b0;
// 			change_transpose_buffer_direction8x8 = 1'b0;
// 			reset_psatd8x8 = 1'b0;
// 			enable_psatd8x8 = 1'b0;
// 			reset_satd8x8 = 1'b0;
// 			enable_satd8x8 = 1'b0;
// 			reset_transpose_buffer8x8 = 1'b0;
// 			done8x8 = 1'b0;
// 		end
// 		default:begin
// 			enable_inputs4x4 = 1'b0;
// 			enable_transpose_buffer4x4 = 1'b0;
// 			change_transpose_buffer_direction4x4 = 1'b0;
// 			reset_psatd4x4 = 1'b0;
// 			enable_psatd4x4 = 1'b0;
// 			reset_satd4x4 = 1'b0;
// 			enable_satd4x4 = 1'b0;
// 			reset_transpose_buffer4x4 = 1'b0;
// 			done4x4 = 1'b0;
// 			done4x4B = 1'b0;
// 			enable_transpose_buffer8x8 = 1'b0;
// 			change_transpose_buffer_direction8x8 = 1'b0;
// 			reset_psatd8x8 = 1'b0;
// 			enable_psatd8x8 = 1'b0;
// 			reset_satd8x8 = 1'b0;
// 			enable_satd8x8 = 1'b0;
// 			reset_transpose_buffer8x8 = 1'b0;
// 			done8x8 = 1'b0;
// 		end
// 	endcase
// end

// always @ (posedge clock or posedge reset) begin
// 	if (reset)
// 		state <= IDLE;
// 	else
// 		case (state)
// 			IDLE: if(enable)
// 				state<= INITIALIZE_0;
// 			INITIALIZE_0: state<=INITIALIZE_1;
// 			INITIALIZE_1: state<=INITIALIZE_2;
// 			INITIALIZE_2: state<=INITIALIZE_3;
// 			INITIALIZE_3: state<=INITIALIZE_4;
// 			INITIALIZE_4: state<=INITIALIZE_5;
// 			INITIALIZE_5: state<=INITIALIZE_6;
// 			INITIALIZE_6: state<=INITIALIZE_7;
// 			INITIALIZE_7: state<=INITIALIZE_8;
// 			INITIALIZE_8: state<=INITIALIZE_9;
// 			INITIALIZE_9: state<=INITIALIZE_10;
// 			INITIALIZE_10: state<=INITIALIZE_11;

// 			INITIALIZE_11: state<=INITIALIZE_12;
// 			INITIALIZE_12: state<=INITIALIZE_13;
// 			INITIALIZE_13: state<=INITIALIZE_14;
// 			INITIALIZE_14: state<=INITIALIZE_15;
// 			INITIALIZE_15: state<=INITIALIZE_16;
// 			INITIALIZE_16: state<=INITIALIZE_17;
// 			INITIALIZE_17: state<=INITIALIZE_18;
// 			INITIALIZE_18: state<=INITIALIZE_19;

// 			INITIALIZE_19: if(enable)
// 				state<=PRE_CALCULATE_0;
// 			else
// 				state<=PRE_TERMINATE_0;
// 			CALCULATE_0: state<=CALCULATE_1;
// 			CALCULATE_1: state<=CALCULATE_2;
// 			CALCULATE_2: state<=CALCULATE_3;
// 			CALCULATE_3: state<=CALCULATE_4;
// 			CALCULATE_4: state<=CALCULATE_5;
// 			CALCULATE_5: state<=CALCULATE_6;
// 			CALCULATE_6: state<=CALCULATE_7;
// 			CALCULATE_7: if(enable)
// 				state<=CALCULATE_0;
// 			else
// 				state<=TERMINATE_0;
// 			TERMINATE_0: state<= TERMINATE_1;
// 			TERMINATE_1: state<= TERMINATE_2;
// 			TERMINATE_2: state<= TERMINATE_3;
// 			TERMINATE_3: state<= TERMINATE_4;
// 			TERMINATE_4: state<= TERMINATE_5;
// 			TERMINATE_5: state<= TERMINATE_6;
// 			TERMINATE_6: state<= TERMINATE_7;
// 			TERMINATE_7: state<=LAST_0;
// 			PRE_CALCULATE_0: state<= PRE_CALCULATE_1;
// 			PRE_CALCULATE_1: state<= CALCULATE_2;
// 			LAST_0: state<= LAST_1;
// 			LAST_1: state<= IDLE;
// 			PRE_TERMINATE_0: state<=PRE_TERMINATE_1;
// 			PRE_TERMINATE_1: state<=TERMINATE_2;
// 		endcase
// 	end
// endmodule