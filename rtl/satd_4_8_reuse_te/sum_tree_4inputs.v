//-------------------------------------------------------------------------
// Design Name : sum_tree_4inputs
// File Name   : sum_tree_4inputs.v
// Function    : Sums up 4 inputs in a combinational fashion
// Coder       : Ismael Seidel
//-------------------------------------------------------------------------
module sum_tree_4inputs (
	in_0,
	in_1,
	in_2,
	in_3,
	out
);

parameter DATA_WIDTH = 8;

//------------------------------- Input Ports -----------------------------------
input [DATA_WIDTH-1:0] in_0, in_1, in_2, in_3;

//------------------------------- Output Ports -----------------------------------
output [DATA_WIDTH+1:0] out;

//-------------------------------- Internal Wires ----------------------------
wire [DATA_WIDTH:0] sum_0, sum_1;
wire [DATA_WIDTH+1:0] sum_2;

//--------------------------------- Code Starts Here -----------------------------
adder #(DATA_WIDTH) adder_0(in_0, in_1, sum_0);
adder #(DATA_WIDTH) adder_1(in_2, in_3, sum_1);

adder #(DATA_WIDTH+1) adder_2(sum_0, sum_1, sum_2);

assign out = sum_2;

endmodule