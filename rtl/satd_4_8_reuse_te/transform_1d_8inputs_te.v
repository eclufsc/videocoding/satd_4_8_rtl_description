/* 
 *	\file transform_1d_8inputs_te.v
 *	\brief Description
 *	\author Ismael Seidel
 *	\Date:   2018-07-08 18:06:08
 *  \Last Modified by:   Ismael Seidel
 *  \Last Modified time: 2018-07-08 18:06:08
 *
 *  Longer description here
 *	
 *	Developed at Embedded Computing Lab (ECL), 2018.
 *	
 */
 

 module transform_1d_8inputs_te (
	in_0,
	in_1,
	in_2,
	in_3,
	in_4,
	in_5,
	in_6,
	in_7,
	out_0,
	out_1,
	out_2,
	out_3
);

parameter DATA_WIDTH = 8;

input signed [DATA_WIDTH-1:0] in_0, in_1, in_2, in_3, in_4, in_5, in_6, in_7;
output [DATA_WIDTH-2:0] out_0, out_1, out_2, out_3;

butterfly_te #(DATA_WIDTH) butterfly_te_0_0(in_0, in_7, out_0);
butterfly_te #(DATA_WIDTH) butterfly_te_0_2(in_1, in_6, out_1);
butterfly_te #(DATA_WIDTH) butterfly_te_0_4(in_2, in_5, out_2);
butterfly_te #(DATA_WIDTH) butterfly_te_0_6(in_3, in_4, out_3);


endmodule