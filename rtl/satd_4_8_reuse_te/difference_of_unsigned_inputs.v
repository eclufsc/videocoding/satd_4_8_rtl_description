/* 
 *	\file difference_of_unsigned_inputs.v
 *	\brief Implementation of an parameterized difference_of_unsigned_inputs.
 *	\author Ismael Seidel
 *	\Date:   2015-07-13 15:11:45
 *  \Last Modified by:   ismaelseidel
 *  \Last Modified time: 2015-07-14 11:35:54
 *
 *	This difference_of_unsigned_inputs implementation is parameterized so as to the number of bits in its inputs. 
 *	Thus, the value DATA_WIDTH is the number of bits of in_0 and also the number of bits of in_1.
 *	Notice that the output is one bit larger, so as to accomodate the carry out of this difference_of_unsigned_inputs.
 *	
 *	Developed at Embedded Computing Lab (ECL), 2015.
 *	
 */

//difference_of_unsigned_inputs module definition.
module difference_of_unsigned_inputs (
	in_0,
	in_1, 
	out
);

//parameter indicating the number of bits in the inputs. The output is one bit larger.
parameter DATA_WIDTH = 8;

//declaration of number of bits for inputs in_0 and in_1.
input [DATA_WIDTH-1:0] in_0, in_1;

//declaration of output.
output signed [DATA_WIDTH:0] out;

//the output is assigned to be the difference between in_0 and in_1. As the output is one bit longer than the inputs, it should accomodate the carry out (signal).
assign out = in_0-in_1;

endmodule