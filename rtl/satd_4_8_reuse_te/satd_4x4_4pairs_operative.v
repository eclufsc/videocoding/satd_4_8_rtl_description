//-------------------------------------------------------------------------
// Design Name : satd_4x4_4pairs_operative
// File Name   : satd_4x4_4pairs_operative.v
// Function    : SATD of 4x4 blocks with 4 pixel pairs as input
// Coder       : Ismael Seidel
//-------------------------------------------------------------------------
module satd_4x4_4pairs_operative(
	clock,
	reset,
	enable_inputs,
	reset_transpose_buffer,
	enable_transpose_buffer,
	change_transpose_buffer_direction,
	reset_psatd,
	enable_psatd,
	reset_satd,
	enable_satd,
	original_0,
	original_1,
	original_2,
	original_3,
	candidate_0,
	candidate_1,
	candidate_2,
	candidate_3,
	satd,
	out_HT_4x4_0,
	out_HT_4x4_1,
	out_HT_4x4_2,
	out_HT_4x4_3
);

parameter DATA_WIDTH = 8;

//------------------------------- Input Ports -----------------------------------
input clock, reset, enable_inputs, reset_transpose_buffer, enable_transpose_buffer, change_transpose_buffer_direction, reset_psatd, enable_psatd, reset_satd, enable_satd;
input [DATA_WIDTH-1:0] original_0, original_1, original_2, original_3, candidate_0, candidate_1, candidate_2, candidate_3;

//------------------------------- Output Ports -----------------------------------
output reg [DATA_WIDTH+7:0] satd;
output signed [DATA_WIDTH+4:0] out_HT_4x4_0, out_HT_4x4_1, out_HT_4x4_2, out_HT_4x4_3;

//-------------------------------- Internal Wires ----------------------------
wire transpose_buffer_direction;
wire [DATA_WIDTH-1:0] original_reg_0, original_reg_1, original_reg_2, original_reg_3, candidate_reg_0, candidate_reg_1, candidate_reg_2, candidate_reg_3;
wire signed [DATA_WIDTH:0] difference_0, difference_1, difference_2, difference_3;
wire signed [DATA_WIDTH+2:0] out_of_1st_1d_transform_0, out_of_1st_1d_transform_1, out_of_1st_1d_transform_2, out_of_1st_1d_transform_3;
wire signed [DATA_WIDTH+2:0] out_of_transpose_buffer_0, out_of_transpose_buffer_1, out_of_transpose_buffer_2, out_of_transpose_buffer_3;
wire signed [DATA_WIDTH+4:0] out_of_2nd_1d_transform_0, out_of_2nd_1d_transform_1, out_of_2nd_1d_transform_2, out_of_2nd_1d_transform_3;
wire [DATA_WIDTH+3:0] absolute_transformed_difference_0, absolute_transformed_difference_1, absolute_transformed_difference_2, absolute_transformed_difference_3;
wire [DATA_WIDTH+5:0] sum;
wire [DATA_WIDTH+7:0] psatd;

//--------------------------------- Code Starts Here -----------------------------
tff_async_reset transpose_buffer_direction_holder(change_transpose_buffer_direction, clock, reset, transpose_buffer_direction);

input_buffer_4pairs #(DATA_WIDTH) register_inputs(clock, reset, enable_inputs, original_0, original_1, original_2, original_3, candidate_0, candidate_1, candidate_2, candidate_3, original_reg_0, original_reg_1, original_reg_2, original_reg_3, candidate_reg_0, candidate_reg_1, candidate_reg_2, candidate_reg_3);

difference_layer_4pairs #(DATA_WIDTH) difference(original_reg_0, original_reg_1, original_reg_2, original_reg_3, candidate_reg_0, candidate_reg_1, candidate_reg_2, candidate_reg_3, difference_0, difference_1, difference_2, difference_3);

transform_1d_4inputs #(DATA_WIDTH+1) first_1d_transform(difference_0, difference_1, difference_2, difference_3, out_of_1st_1d_transform_0, out_of_1st_1d_transform_1, out_of_1st_1d_transform_2, out_of_1st_1d_transform_3);

transpose_buffer_4x4 #(DATA_WIDTH+3) transpose_buffer(clock, reset_transpose_buffer, enable_transpose_buffer, transpose_buffer_direction, out_of_1st_1d_transform_0, out_of_1st_1d_transform_1, out_of_1st_1d_transform_2, out_of_1st_1d_transform_3, out_of_transpose_buffer_0, out_of_transpose_buffer_1, out_of_transpose_buffer_2, out_of_transpose_buffer_3);

transform_1d_4inputs #(DATA_WIDTH+3) second_1d_transform(out_of_transpose_buffer_0, out_of_transpose_buffer_1, out_of_transpose_buffer_2, out_of_transpose_buffer_3, out_of_2nd_1d_transform_0, out_of_2nd_1d_transform_1, out_of_2nd_1d_transform_2, out_of_2nd_1d_transform_3); // pegar daqui os valores e jogar pro criador de tabela 8x8

abs_layer_4inputs #(DATA_WIDTH+5) absolute1(out_of_2nd_1d_transform_0, out_of_2nd_1d_transform_1, out_of_2nd_1d_transform_2, out_of_2nd_1d_transform_3, absolute_transformed_difference_0, absolute_transformed_difference_1, absolute_transformed_difference_2, absolute_transformed_difference_3);

sum_tree_4inputs #(DATA_WIDTH+4) sum_tree(absolute_transformed_difference_0, absolute_transformed_difference_1, absolute_transformed_difference_2, absolute_transformed_difference_3, sum);

accumulator #(DATA_WIDTH+6, DATA_WIDTH+8) acc(clock, reset_psatd, enable_psatd, sum, psatd);

// always @(*) begin
	assign out_HT_4x4_0 = out_of_2nd_1d_transform_0;
	assign out_HT_4x4_1 = out_of_2nd_1d_transform_1;
	assign out_HT_4x4_2 = out_of_2nd_1d_transform_2;
	assign out_HT_4x4_3 = out_of_2nd_1d_transform_3;
// end

always @(posedge clock) begin
	if (reset_satd) begin
		satd <= 0;
	end
	else if (enable_satd) begin
		satd <= (psatd+sum)>>1;
	end
end

endmodule