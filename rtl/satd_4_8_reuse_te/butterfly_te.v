module butterfly_te (
	in_0,
	in_1,
	out
);

parameter DATA_WIDTH = 8;

input signed [DATA_WIDTH-1:0] in_0, in_1;
output [DATA_WIDTH-2:0] out; //out is max(|in_0|, |in_1|)

wire [DATA_WIDTH-2:0] abs_in_0, abs_in_1;

absolute #(DATA_WIDTH) abs_0(in_0, abs_in_0);
absolute #(DATA_WIDTH) abs_1(in_1, abs_in_1);

unsigned_max #(DATA_WIDTH-1) max(abs_in_0, abs_in_1, out);

//assign out = max_abs << 1;

endmodule