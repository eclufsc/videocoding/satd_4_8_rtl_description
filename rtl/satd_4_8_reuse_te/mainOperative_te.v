//-------------------------------------------------------------------------
// Design Name : mainOperative_te
// File Name   : mainOperative_te.v
// Function    : SATD
// Coder       : Marcio Monteiro
//-------------------------------------------------------------------------
module mainOperative_te(
	clk,
	reset,
	enable_inputs4x4, // controls the inputs of 4x4
	reset_transpose_buffer4x4,
	enable_transpose_buffer4x4,
	change_transpose_buffer_direction4x4,
	reset_psatd4x4,
	enable_psatd4x4,
	reset_satd4x4,
	enable_satd4x4,
	reset_transpose_buffer8x8,
	enable_transpose_buffer8x8,
	change_transpose_buffer_direction8x8,
	reset_psatd8x8,
	enable_psatd8x8,
	reset_satd8x8,
	enable_satd8x8,
	original_0,
	original_1,
	original_2,
	original_3,
	original_4,
	original_5,
	original_6,
	original_7,
	candidate_0,
	candidate_1,
	candidate_2,
	candidate_3,
	candidate_4,
	candidate_5,
	candidate_6,
	candidate_7,
	satd4x4A,
	satd4x4B,
	satd8x8
);

parameter DATA_WIDTH = 8;

//------------------------------- Input Ports -----------------------------------
input clk, reset;
input [DATA_WIDTH-1:0] original_0, original_1, original_2, original_3, original_4, original_5, original_6, original_7, candidate_0, candidate_1, candidate_2, candidate_3, candidate_4, candidate_5, candidate_6, candidate_7;
input enable_inputs4x4, reset_transpose_buffer4x4, enable_transpose_buffer4x4, change_transpose_buffer_direction4x4, reset_psatd4x4, enable_psatd4x4, reset_satd4x4, enable_satd4x4, reset_transpose_buffer8x8, enable_transpose_buffer8x8, change_transpose_buffer_direction8x8, reset_psatd8x8, enable_psatd8x8, reset_satd8x8, enable_satd8x8;

//------------------------------- Output Ports -----------------------------------
output [DATA_WIDTH+7:0] satd4x4A, satd4x4B;
output [DATA_WIDTH+11:0] satd8x8;

//-------------------------------- Internal Wires ----------------------------
wire signed [DATA_WIDTH+4:0] out_HT_0, out_HT_1, out_HT_2, out_HT_3, out_HT_4, out_HT_5, out_HT_6, out_HT_7;

//--------------------------------- Code Starts Here -----------------------------
satd_4x4_4pairs_operative operativeA(clk, reset, enable_inputs4x4, reset_transpose_buffer4x4, enable_transpose_buffer4x4, change_transpose_buffer_direction4x4, reset_psatd4x4, enable_psatd4x4, reset_satd4x4, enable_satd4x4, original_0, original_1, original_2, original_3, candidate_0, candidate_1, candidate_2, candidate_3, satd4x4A, out_HT_0, out_HT_1, out_HT_2, out_HT_3);

satd_4x4_4pairs_operative operativeB(clk, reset, enable_inputs4x4, reset_transpose_buffer4x4, enable_transpose_buffer4x4, change_transpose_buffer_direction4x4, reset_psatd4x4, enable_psatd4x4, reset_satd4x4, enable_satd4x4, original_4, original_5, original_6, original_7, candidate_4, candidate_5, candidate_6, candidate_7, satd4x4B, out_HT_4, out_HT_5, out_HT_6, out_HT_7);

satd_8x8_8pairs_operative_te operative8x8(clk, reset, reset_transpose_buffer8x8, enable_transpose_buffer8x8, change_transpose_buffer_direction8x8, reset_psatd8x8, enable_psatd8x8, reset_satd8x8, enable_satd8x8, out_HT_0, out_HT_1, out_HT_2, out_HT_3, out_HT_4, out_HT_5, out_HT_6, out_HT_7, satd8x8);

endmodule