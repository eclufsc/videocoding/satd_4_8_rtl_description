# satd_4_8_rtl_description

Register Transfer Level (RTL) description using Verilog Hardware Description of the proposed architecture with calculation reuse.

This project is licensed under the MIT License - see the [LICENSE](https://gitlab.com/ecl-video-coding/testing-pipe-repo/-/blob/master/LICENSE) file for details.

If you used our library in an academic work, please cite us as:

M. Monteiro, I. Seidel and J. L. Güntzel, "On the calculation reuse in hadamard-based SATD," 2018 IEEE 9th Latin American Symposium on Circuits & Systems (LASCAS), Puerto Vallarta, 2018, pp. 1-4, doi: 10.1109/LASCAS.2018.8399925.
